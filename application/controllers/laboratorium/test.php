<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {
	
		
	function index()
	{
		$this->load->model('laboratorium/laboratoriummodel');
		$data['is_data']=$this->laboratoriummodel->getTest();		
		$this->load->view('laboratorium/testview',$data);
	}
	
	public function cetak(){
		$id_test_lab = $_GET['uid'];
		$this->load->model('laboratorium/laboratoriummodel');
    	$data['barcode'] = $this->laboratoriummodel->cetak_test_lab($id_test_lab);
    	$this->load->view('laboratorium/cetak_test_lab',$data);
    }
	
	function addtest()
	{
		$this->load->library('secure_library');
		$this->secure_library->filter_post('id_pasien','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('laboratorium/laboratoriummodel');
			$id_pasien=$this->input->post('id_pasien');

			if($this->laboratoriummodel->cari_pasien($id_pasien)==TRUE)
			{
				//$session_pasien=$this->session->set_userdata($id_pasien);
				$data['pasien']=$this->laboratoriummodel->cari_pasien($id_pasien);
				$data['laboratorium']=$this->laboratoriummodel->laboratorium($id_pasien);
				$data['jenis_lab']=$this->laboratoriummodel->jenis_lab();
				$data['id_pasien']=$id_pasien;
				$this->load->view('laboratorium/proses_test',$data);
			}else{
                echo "Error Tambah Data";
            }
		}else{			
			$this->load->view('laboratorium/addtest');
		}
	}

	function cart()
	{
		$this->load->model('laboratorium/laboratoriummodel');
		$id_pasien=$this->input->post('id_pasien');
		$kode_barang=$this->input->post('id_barang_lab');
		$jumlah_barang=$this->input->post('jumlah_barang');
		$laboran=getInfoUser('nama');
		$tanggal_output=date('Y-m-d');
		if($this->laboratoriummodel->input_cart($kode_barang,$jumlah_barang,$laboran,$tanggal_output)==TRUE)
			{
				//$session_pasien=$this->session->set_userdata($id_pasien);
				$data['pasien']=$this->laboratoriummodel->cari_pasien($id_pasien);
				$data['laboratorium']=$this->laboratoriummodel->laboratorium($id_pasien);
				$data['jenis_lab']=$this->laboratoriummodel->jenis_lab();
				$data['cart']=$this->laboratoriummodel->cart();
				$data['id_pasien']=$id_pasien;
				$this->load->view('laboratorium/proses_test',$data);
			}else{
                echo "Error Tambah Data";
            }
	}

	function simpantest()
	{
		$this->load->model('laboratorium/laboratoriummodel');
		$id_pasien=$this->input->post('id_pasien');
		$tanggal_test=date('Y-m-d');
		$jenis_test=$this->input->post('jenis_test');
		$hasil_test=$this->input->post('hasil_test');
		$laboran=getInfoUser('nama');
		$this->laboratoriummodel->simpan_test($id_pasien,$tanggal_test,$jenis_test,$hasil_test,$laboran);
//$this->proses();
		
		redirect('laboratorium/test');
	}

	function proses()
	{
		$this->load->model('laboratorium/laboratoriummodel');
		$this->laboratoriummodel->proses();
		//$this->load->view('laboratorium/testview');
		redirect('laboratorium/test');
	}

	

}