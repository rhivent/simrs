<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {
	
		
	function intanggal()
	{
		$this->load->library('secure_library');
        $this->secure_library->filter_post('tanggal_awal','required');
        $this->secure_library->filter_post('tanggal_akhir','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('laboratorium/laboratoriummodel');
            $tanggal_awal=$this->input->post('tanggal_awal');
            $tanggal_akhir=$this->input->post('tanggal_akhir');

			if($this->laboratoriummodel->laporan_gudang($tanggal_awal,$tanggal_akhir)==TRUE)
			{
                $data['is_data']=$this->laboratoriummodel->laporan_gudang($tanggal_awal,$tanggal_akhir);
                $data['tanggal_awal']=$tanggal_awal;
                $data['tanggal_akhir']=$tanggal_akhir;
				$this->load->view('laboratorium/laporan_gudang',$data);
			}else{
                echo "Error Tambah Data";
            }
		}else{			
			$this->load->view('laboratorium/gudang');
		}
    }

    function cetakintanggal()
    {
        $this->load->model('laboratorium/laboratoriummodel');
        $tanggal_awal=$_GET['tanggal_awal'];
        $tanggal_akhir=$_GET['tanggal_akhir'];
        $data['is_data']=$this->laboratoriummodel->cetak_laporan_gudang($tanggal_awal,$tanggal_akhir);
        $data['tanggal_awal']=$tanggal_awal;
        $data['tanggal_akhir']=$tanggal_akhir;
		$this->load->view('laboratorium/cetak_laporan_gudang',$data);
    }

    function inbarang()
    {
        $this->load->library('secure_library');
        $this->secure_library->filter_post('id_barang_lab','required');
        $this->secure_library->filter_post('tanggal_awal','required');
        $this->secure_library->filter_post('tanggal_akhir','required');
		if($this->secure_library->start_post()==TRUE)
		{
            $this->load->model('laboratorium/laboratoriummodel');
            $id_barang_lab=$this->input->post('id_barang_lab');
            $tanggal_awal=$this->input->post('tanggal_awal');
            $tanggal_akhir=$this->input->post('tanggal_akhir');

			if($this->laboratoriummodel->laporan_barang($tanggal_awal,$tanggal_akhir,$id_barang_lab)==TRUE)
			{
                $data['is_data']=$this->laboratoriummodel->laporan_barang($tanggal_awal,$tanggal_akhir,$id_barang_lab);
                $data['tanggal_awal']=$tanggal_awal;
                $data['tanggal_akhir']=$tanggal_akhir;
                $data['id_barang_lab']=$id_barang_lab;
				$this->load->view('laboratorium/laporan_barang',$data);
			}else{
                echo "Error";
            }
		}else{			
			$this->load->view('laboratorium/barang');
		}
    }

    function cetakinbarang()
    {
        $this->load->model('laboratorium/laboratoriummodel');
        $tanggal_awal=$_GET['tanggal_awal'];
        $tanggal_akhir=$_GET['tanggal_akhir'];
        $id_barang_lab=$_GET['id_barang_lab'];
        $data['is_data']=$this->laboratoriummodel->cetak_laporan_barang($tanggal_awal,$tanggal_akhir,$id_barang_lab);
        $data['tanggal_awal']=$tanggal_awal;
        $data['tanggal_akhir']=$tanggal_akhir;
        $data['id_barang_lab']=$id_barang_lab;
		$this->load->view('laboratorium/cetak_laporan_barang',$data);
    }

    function outtanggal()
    {
        $this->load->library('secure_library');
        $this->secure_library->filter_post('tanggal_awal','required');
        $this->secure_library->filter_post('tanggal_akhir','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('laboratorium/laboratoriummodel');
            $tanggal_awal=$this->input->post('tanggal_awal');
            $tanggal_akhir=$this->input->post('tanggal_akhir');

			if($this->laboratoriummodel->laporan_gudang_keluar($tanggal_awal,$tanggal_akhir)==TRUE)
			{
                $data['is_data']=$this->laboratoriummodel->laporan_gudang_keluar($tanggal_awal,$tanggal_akhir);
                $data['tanggal_awal']=$tanggal_awal;
                $data['tanggal_akhir']=$tanggal_akhir;
				$this->load->view('laboratorium/laporan_gudang_keluar',$data);
			}else{
                echo "Error Data";
            }
		}else{			
			$this->load->view('laboratorium/outtanggal');
		}
    }

    function cetakouttanggal()
    {
        $this->load->model('laboratorium/laboratoriummodel');
        $tanggal_awal=$_GET['tanggal_awal'];
        $tanggal_akhir=$_GET['tanggal_akhir'];
        $data['is_data']=$this->laboratoriummodel->cetak_laporan_keluar($tanggal_awal,$tanggal_akhir);
        $data['tanggal_awal']=$tanggal_awal;
        $data['tanggal_akhir']=$tanggal_akhir;
		$this->load->view('laboratorium/cetak_laporan_keluar',$data);
    }

    function outbarang()
    {

    }

    function cetakoutbarang()
    {

    }
}