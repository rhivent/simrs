<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang extends CI_Controller {
	
		
	function databarang()
	{
		$this->load->model('laboratorium/laboratoriummodel','um');
		$data['is_data']=$this->um->getBarang();		
		$this->load->view('laboratorium/barangview',$data);
    }
    
    function addbarang()
    {
        $this->load->view('laboratorium/addbarang');
    }

    function simpanbarang()
    {
        $this->load->library('secure_library');
		$this->secure_library->filter_post('kode_barang_lab','required');
		$this->secure_library->filter_post('nama_barang_lab','required');
        $this->secure_library->filter_post('kategori_barang_lab','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('laboratorium/laboratoriummodel');
			$kode_barang_lab=$this->input->post('kode_barang_lab');
			$nama_barang_lab=$this->input->post('nama_barang_lab');
			$kategori_barang_lab=$this->input->post('kategori_barang_lab');
			$satuan_barang_lab=$this->input->post('satuan_barang_lab');

			if($this->laboratoriummodel->create_barang($kode_barang_lab,$nama_barang_lab,$kategori_barang_lab,$satuan_barang_lab)==TRUE)
			{
				$data['isok']="Barang sukses ditambahkan";
				$this->load->view('laboratorium/addbarang',$data);
			}else{
                echo "Error Tambah Data";
            }
		}else{			
			$this->load->view('laboratorium/addbarang');
		}
    }

    function deletebarang(){
        $id_barang_lab=$_GET['uid'];
		$this->load->model('laboratorium/laboratoriummodel');
		if($this->laboratoriummodel->delete_barang($id_barang_lab)==TRUE)
		{
			redirect('laboratorium/barang/databarang');
		}else{
			redirect('laboratorium/barang/databarang');
		}
    }

    function updateview()
    {
        $id=$_GET['uid'];
		$this->load->model('laboratorium/laboratoriummodel');
        $data['update']=$this->laboratoriummodel->detail_barang($id);
		$this->load->view('laboratorium/editbarang',$data);
    }

    function updatebarang()
    {
        $this->load->library('secure_library');
		$this->secure_library->filter_post('kode_barang_lab','required');
		$this->secure_library->filter_post('nama_barang_lab','required');
        $this->secure_library->filter_post('kategori_barang_lab','required');
		if($this->secure_library->start_post()==TRUE)
		{
            $this->load->model('laboratorium/laboratoriummodel');
            $id_barang_lab=$this->input->post('id_barang_lab');
			$kode_barang_lab=$this->input->post('kode_barang_lab');
			$nama_barang_lab=$this->input->post('nama_barang_lab');
			$kategori_barang_lab=$this->input->post('kategori_barang_lab');

			if($this->laboratoriummodel->update_barang($id_barang_lab,$kode_barang_lab,$nama_barang_lab,$kategori_barang_lab))
			{
				redirect('laboratorium/barang/databarang');
			}else{
                echo "Error Tambah Data";
            }
		}else{			
			redirect('laboratorium/barang/databarang');
		}
	}
	
	function inputbarang()
	{
		$this->load->view('laboratorium/inputbarang');
	}

	function prosesinputbarang()
	{
		$this->load->library('secure_library');
		$this->secure_library->filter_post('kode_barang','required');
		$this->secure_library->filter_post('jumlah_barang','required');
		if($this->secure_library->start_post()==TRUE)
		{
            $this->load->model('laboratorium/laboratoriummodel');
			$kode_barang=$this->input->post('kode_barang');
			$jumlah_barang=$this->input->post('jumlah_barang');
			$laboran=getInfoUser('nama');
			$tanggal_input=date('Y-m-d');
			if($this->laboratoriummodel->update_stok($kode_barang,$jumlah_barang,$tanggal_input,$laboran)==TRUE)
			{
				redirect('laboratorium/barang/inputbarang');
			}else{
                echo "Error Tambah Data";
            }
		}else{			
			redirect('laboratorium/barang/inputbarang');
		}
	}
}