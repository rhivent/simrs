<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jenis_lab extends CI_Controller {
	
		
	function index()
	{
		$this->load->model('laboratorium/laboratoriummodel');
		$data['is_data']=$this->laboratoriummodel->getJenis();		
		$this->load->view('laboratorium/jenisview',$data);
    }
    
    function addjenis()
    {
        $this->load->library('secure_library');
		$this->secure_library->filter_post('kode_jenis','required');
		$this->secure_library->filter_post('nama_jenis','required');
        $this->secure_library->filter_post('standart_test','required');
        $this->secure_library->filter_post('keterangan','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('laboratorium/laboratoriummodel');
			$kode_jenis=$this->input->post('kode_jenis');
			$nama_jenis=$this->input->post('nama_jenis');
			$standart_test=$this->input->post('standart_test');
			$keterangan=$this->input->post('keterangan');

			if($this->laboratoriummodel->add_jenis($kode_jenis,$nama_jenis,$standart_test,$keterangan)==TRUE)
			{
				$data['isok']="Jenis test sukses ditambahkan";
				$this->load->view('laboratorium/addjenis',$data);
			}else{
                echo "Error Tambah Data";
            }
		}else{			
			$this->load->view('laboratorium/addjenis');
		}
    }

    function updatejenis()
    {
        $id=$_GET['uid'];
		$this->load->model('laboratorium/laboratoriummodel');
        $data['update']=$this->laboratoriummodel->detail_jenis($id);
		$this->load->view('laboratorium/editjenis',$data);
		//print_r( $data['update'] );
    }

    function proses_updatejenis()
    {
        $this->load->library('secure_library');
        $this->secure_library->filter_post('id_jenis_lab','required');
		$this->secure_library->filter_post('kode_jenis','required');
		$this->secure_library->filter_post('nama_jenis','required');
        $this->secure_library->filter_post('standart_test','required');
        $this->secure_library->filter_post('keterangan','required');
		if($this->secure_library->start_post()==TRUE)
		{
            $this->load->model('laboratorium/laboratoriummodel');
            $id_jenis_lab=$this->input->post('id_jenis_lab');
			$kode_jenis=$this->input->post('kode_jenis');
			$nama_jenis=$this->input->post('nama_jenis');
			$standart_test=$this->input->post('standart_test');
			$keterangan=$this->input->post('keterangan');

			if($this->laboratoriummodel->update_jenis($id_jenis_lab,$kode_jenis,$nama_jenis,$standart_test,$keterangan))
			{
				$this->load->model('laboratorium/laboratoriummodel');
				$data['isok']="Jenis test sukses diubah";
        		$data['update']=$this->laboratoriummodel->detail_jenis($id_jenis_lab);
				$this->load->view('laboratorium/editjenis',$data);
			}else{
                echo "Error Tambah Data";
            }
		}else{			
			$this->load->view('laboratorium/jenisview');
		}
    }

    function deletejenis()
    {
        $id_jenis_lab=$_GET['uid'];
		$this->load->model('laboratorium/laboratoriummodel');
		if($this->laboratoriummodel->delete_jenis($id_jenis_lab)==TRUE)
		{
			redirect('laboratorium/jenis_lab/jenisview');
		}else{
			redirect('laboratorium/jenis_lab/jenisview');
		}
    }
    
}