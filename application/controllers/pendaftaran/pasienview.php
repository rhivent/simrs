<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pasienview extends CI_Controller {
	
	function index()
	{
		$this->load->model('pendaftaran/pasienmodel','um');
		$data['is_data']=$this->um->getPasien();		
		$this->load->view('pendaftaran/pasienview',$data);
	}
	
	function delete()
	{
		$idPasien=$_GET['uid'];
		$this->load->model('pendaftaran/pasienmodel');
		if($this->pasienmodel->delete_pasien($idPasien)==TRUE)
		{
			redirect('pendaftaran/pasienview');
		}else{
			redirect('pendaftaran/pasienview');
		}
	}

	public function updateview()
	{
		$id=$_GET['uid'];
		$this->load->model('pendaftaran/pasienmodel');
		$data['pasien']=$this->pasienmodel->get_pasien_by_id($id);
		//$data['id']=$id;
		//if(!empty($data['pasien']))
		//{
			$this->load->view('pendaftaran/editpasienview',$data);
		//}else{
		//	redirect('pendaftaran/pasienview');
		//}
	}

	public function update(){
		$id=$_GET['uid'];
		$nama_pasien=$this->input->post('nama_pasien');
		$alamat_pasien=$this->input->post('alamat_pasien');
		$no_telp=$this->input->post('no_telp');
        $tempat_lahir=$this->input->post('tempat_lahir');
        $tanggal_lahir=$this->input->post('tanggal_lahir');
        $nomor=$this->input->post('nomor');
		$this->load->model('pendaftaran/pasienmodel');
		if($this->pasienmodel->update($id,$nama_pasien,$alamat_pasien,$no_telp,$tempat_lahir,$tanggal_lahir,$nomor)==TRUE)
		{
			redirect('pendaftaran/pasienview');
		}else{
			redirect('pendaftaran/pasienview');
		}
	}
	
	public function cetak(){
		$id_pasien = $_GET['uid'];
		$this->load->model('pendaftaran/pasienmodel');
    	$data['barcode'] = $this->pasienmodel->caripasien($id_pasien);
    	$this->load->view('pendaftaran/cetak',$data);
    }

	public function ca_barcode($primary_key , $row)
	{
	    return site_url('pasienview/get_barcode').'/'.$row->nomor;
	}

	public function get_barcode($code)
    {
        $this->set_barcode($code);
    }

    private function set_barcode($code)
    {
        $this->load->library('Zend');
        $this->zend->load('Zend/Barcode');
        //generate barcode
        Zend_Barcode::render('code128', 'image', array('text'=>$code), array());
    }
}