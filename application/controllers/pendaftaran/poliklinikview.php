<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Poliklinikview extends CI_Controller {
	
	function index()
	{
		$this->load->model('pendaftaran/poliklinikmodel','um');
		$data['is_data']=$this->um->getPasien();		
		$this->load->view('pendaftaran/poliklinikview',$data);
    }
    
    public function detail()
    {
        $id=$_GET['uid'];
		$this->load->model('pendaftaran/poliklinikmodel');
        $data['detail']=$this->poliklinikmodel->detail($id);
        $data['riwayat']=$this->poliklinikmodel->riwayat($id);
        $data['laboratorium']=$this->poliklinikmodel->laboratorium($id);
        //$data['riwayat']=$this->poliklinikmodel->riwayat($id);
		$this->load->view('pendaftaran/detailperiksa',$data);
    }

    function delete()
	{
		$id_reg_poli=$_GET['uid'];
		$this->load->model('pendaftaran/poliklinikmodel');
		if($this->poliklinikmodel->delete($id_reg_poli)==TRUE)
		{
			redirect('pendaftaran/poliklinikview');
		}else{
			redirect('pendaftaran/poliklinikview');
		}
	}
	
}