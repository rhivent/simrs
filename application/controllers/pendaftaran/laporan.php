<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {
    
    function pasien()
    {
        $this->load->library('secure_library');
        $this->secure_library->filter_post('tgl','required');
        $this->secure_library->filter_post('tgl2','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('pendaftaran/pasienmodel');
            $tanggal_awal=$this->input->post('tgl');
            $tanggal_akhir=$this->input->post('tgl2');

			if($this->pasienmodel->laporan_pasien($tanggal_awal,$tanggal_akhir)==TRUE)
			{
                $data['is_data']=$this->pasienmodel->laporan_pasien($tanggal_awal,$tanggal_akhir);
                $data['total']=$this->pasienmodel->total($tanggal_awal,$tanggal_akhir);
                $data['tanggal_awal']=$tanggal_awal;
                $data['tanggal_akhir']=$tanggal_akhir;
				$this->load->view('pendaftaran/laporan_pasien',$data);
			}else{
                echo "Error Data";
            }
		}else{			
			$this->load->view('pendaftaran/laporanpasien');
		}
    }
}