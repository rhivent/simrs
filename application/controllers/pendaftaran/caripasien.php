<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Caripasien extends CI_Controller {

    function datapasien(){
        $this->load->library('secure_library');
        $this->secure_library->filter_post('cari','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('pendaftaran/pencarianmodel');
            $cari=$this->input->post('cari');
			
			if($this->pencarianmodel->find($cari)==TRUE)
			{
				$data['detail']=$this->pencarianmodel->find($cari);
				$this->load->view('pendaftaran/hasilcaripasien',$data);
			}else{
                echo "Error Cari Data";
            }
		}else{			
			$this->load->view('pendaftaran/caridatapasien');
		}
    }

    function datapoli(){
        $this->load->library('secure_library');
        $this->secure_library->filter_post('cari','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('pendaftaran/pencarianmodel');
            $cari=$this->input->post('cari');
			
			if($this->pencarianmodel->findpasien($cari)==TRUE)
			{
                $this->load->model('pendaftaran/poliklinikmodel');
                $data['detail']=$this->pencarianmodel->findpasien($cari);
                $data['riwayat']=$this->pencarianmodel->riwayat($cari);
                $data['laboratorium']=$this->pencarianmodel->laboratorium($cari);
				$this->load->view('pendaftaran/hasilcaripoli',$data);
			}else{
                echo "Data Tidak Ditemukan";
            }
		}else{			
			$this->load->view('pendaftaran/caridatapoli');
		}
    }

    function datainap(){
        $this->load->library('secure_library');
        $this->secure_library->filter_post('cari','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('pendaftaran/pencarianmodel');
            $cari=$this->input->post('cari');
			
			if($this->pencarianmodel->findpasieninap($cari)==TRUE)
			{
                $data['detail']=$this->pencarianmodel->findpasieninap($cari);
                $data['inap']=$this->pencarianmodel->findinap($cari);
				$this->load->view('pendaftaran/hasilcariinap',$data);
			}else{
                $data['hasil']="Data Tidak Ditemukan";
                $this->load->view('pendaftaran/caridatainap',$data);
            }
		}else{			
			$this->load->view('pendaftaran/caridatainap');
		}
    }

}