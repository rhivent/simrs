<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rawatinapview extends CI_Controller {
	
	function index()
	{
		$this->load->model('pendaftaran/rawatinapmodel','um');
		$data['is_data']=$this->um->getdata();
		$this->load->view('pendaftaran/rawatinapview',$data);
    }
    
    public function detail()
    {
        $id=$_GET['uid'];
		$this->load->model('pendaftaran/poliklinikmodel');
        $data['detail']=$this->poliklinikmodel->detail($id);
        $data['riwayat']=$this->poliklinikmodel->riwayat($id);
        $data['laboratorium']=$this->poliklinikmodel->laboratorium($id);
        //$data['riwayat']=$this->poliklinikmodel->riwayat($id);
		$this->load->view('pendaftaran/detailperiksa',$data);
    }

    public function delete()
	{
		$id_reg_inap=$_GET['uid'];
		$this->load->model('pendaftaran/rawatinapmodel');
		if($this->rawatinapmodel->delete($id_reg_inap)==TRUE)
		{
			redirect('pendaftaran/rawatinapview');
		}else{
			redirect('pendaftaran/rawatinapview');
		}
	}

	function addreginap(){
		$this->load->model('pendaftaran/rawatinapmodel');
		$data['ruangan']=$this->rawatinapmodel->getruangan();
		$data['paket']=$this->rawatinapmodel->getpaket();
		$data['rujukan']=$this->rawatinapmodel->getrujukan();
		$this->load->view('pendaftaran/addreginap',$data);
	}

	function simpan(){
		
			$this->load->model('pendaftaran/rawatinapmodel');
			$nomor=$this->input->post('nomor');
			$tanggal_masuk=$this->input->post('tanggal_masuk');
			$id_paket=$this->input->post('id_paket');
            $id_ruangan=$this->input->post('id_ruang');
            $rujukan=$this->input->post('rujukan');
			$penanggung_jawab=$this->input->post('penanggung_jawab');
			$hub_dengan_pasien=$this->input->post('hub_dengan_pasien');
			
			if($this->rawatinapmodel->simpan($nomor,$tanggal_masuk,$id_ruangan,$id_paket,$rujukan,$penanggung_jawab,$hub_dengan_pasien)==TRUE)
			{
				$this->updatekamar($id_ruangan);
			}else{
                echo "Error Tambah Data";
            }
		
	}

	function updatekamar($id_ruangan)
	{
		$this->load->model('pendaftaran/rawatinapmodel');
		$this->rawatinapmodel->updatekamar($id_ruangan);
		$data['isok']="Data sukses ditambahkan";
		$this->load->view('pendaftaran/addreginap',$data);
	}

	
}