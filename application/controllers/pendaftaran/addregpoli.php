<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Addregpoli extends CI_Controller {
	
	public function index()
	{
		$this->load->library('secure_library');
        $this->secure_library->filter_post('nomor','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('pendaftaran/poliklinikmodel');
            $nomor=$this->input->post('nomor');
			
			if($this->poliklinikmodel->find($nomor)==TRUE)
			{
				$data['detail']=$this->poliklinikmodel->find($nomor);
				$data['poli']=$this->poliklinikmodel->poli();
				$this->load->view('pendaftaran/newregpoli',$data);
			}else{
                echo "Error Cari Data";
            }
		}else{			
			$this->load->view('pendaftaran/addregpoli');
		}
    }
	
	public function simpan(){
		$this->load->library('secure_library');
		$this->secure_library->filter_post('nomor','required');
		$this->secure_library->filter_post('no_registrasi','required');
        $this->secure_library->filter_post('id_poli','required');
        $this->secure_library->filter_post('bb','required');
        $this->secure_library->filter_post('tensi','required');
        $this->secure_library->filter_post('keluhan','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('pendaftaran/poliklinikmodel');
			$id_pasien=$this->input->post('nomor');
			$no_registrasi=$this->input->post('no_registrasi');
			$id_poli=$this->input->post('id_poli');
            $bb=$this->input->post('bb');
            $tensi=$this->input->post('tensi');
			$keluhan=$this->input->post('keluhan');
			$tanggal_periksa=date('Y-m-d');;
			
			if($this->poliklinikmodel->simpan($id_pasien,$tanggal_periksa,$id_poli,$no_registrasi,$keluhan,$bb,$tensi)==TRUE)
			{
				$data['isok']="Data sukses ditambahkan";
				$this->load->view('pendaftaran/addregpoli',$data);
				//$data['barcode']=$this->pasienmodel->cetak($nomor);
				//$this->load->view('pendaftaran/cetak2', $data,array('target' => '_blank'));
			}else{
                echo "Error Tambah Data";
            }
		}else{			
			$this->load->view('pendaftaran/addpasien');
		}
	}
    
	
}