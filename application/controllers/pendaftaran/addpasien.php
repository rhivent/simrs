<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Addpasien extends CI_Controller {
	
	public function index()
	{
		$this->load->library('secure_library');
		$this->secure_library->filter_post('nama_pasien','required');
		$this->secure_library->filter_post('alamat_pasien','required');
        $this->secure_library->filter_post('no_telp','required');
        $this->secure_library->filter_post('tempat_lahir','required');
        $this->secure_library->filter_post('tanggal_lahir','required');
        $this->secure_library->filter_post('nomor','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('pendaftaran/pasienmodel');
			$nama_pasien=$this->input->post('nama_pasien');
			$alamat_pasien=$this->input->post('alamat_pasien');
			$no_telp=$this->input->post('no_telp');
            $tempat_lahir=$this->input->post('tempat_lahir');
            $tanggal_lahir=$this->input->post('tanggal_lahir');
            $nomor=$this->input->post('nomor');
			
			if($this->pasienmodel->create_pasien($nama_pasien,$alamat_pasien,$no_telp,$tempat_lahir,$tanggal_lahir,$nomor)==TRUE)
			{
				//$data['isok']="Pasien sukses ditambahkan";
				//$this->load->view('pendaftaran/addpasien',$data);
				$data['barcode']=$this->pasienmodel->cetak($nomor);
				$this->load->view('pendaftaran/cetak2', $data,array('target' => '_blank'));
			}else{
                echo "Error Tambah Data";
            }
		}else{		
			$this->load->model('pendaftaran/pasienmodel');
			$data['nomor']	= $this->pasienmodel->nomorbaru();
			$this->load->view('pendaftaran/addpasien',$data);
		}
	}
	
}