<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
		
	function index()
	{
		if(getRole()!="kandungan")
		{
			redirect('kandungan');
		}
		$this->load->view('kandungan/index');
	}
}