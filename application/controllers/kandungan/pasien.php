<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pasien extends CI_Controller {
	
	function index()
	{
		$this->load->library('secure_library');
		$this->secure_library->filter_post('id_pasien','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('kandungan/kandunganmodel');
			$id_pasien=$this->input->post('id_pasien');

			if($this->kandunganmodel->cari_pasien($id_pasien)==TRUE)
			{
				//$session_pasien=$this->session->set_userdata($id_pasien);
				$data['pasien']=$this->kandunganmodel->cari_pasien($id_pasien);
				$data['laboratorium']=$this->kandunganmodel->laboratorium($id_pasien);
				$data['riwayat']=$this->kandunganmodel->riwayat($id_pasien);
				$data['id_pasien']=$id_pasien;
				$this->load->view('kandungan/proses_periksa',$data);
			}else{
                redirect(base_url('kandungan/pasien'));
            }
		}else{			
			$this->load->view('kandungan/cari_pasien');
		}
	}
	
	function mulaiperiksa()
	{
		$id_pasien=$_GET['uid'];
		$data['id_pasien']=$id_pasien;
		$this->load->view('kandungan/mulai_periksa',$data);
	}

	function simpanresep()
	{
		$id_pasien		= $_GET['uid'];
		$diagnosa		= $this->input->post('diagnosa');
		$resep			= $this->input->post('resep');

		//configurasi upload file
		$config['upload_path']		= './upload_image';
		$config['allowed_types']	= 'gif|jpg|png';
		$config['max_size']			= '1000';
		$config['max_width']		= '2024';
		$config['max_height']		= '1468';

		$this->load->library('upload',$config);
		$this->load->library('template');
		$this->load->model('kandungan/kandunganmodel');
		$this->load->helper(array('form','url'));
		$data['message']='';
		if(!$this->upload->do_upload())
		{
			$data['message']	= $this->upload->display_errors();
		}
		else
		{
			
		}
			if($this->laboratoriummodel->create_barang($kode_barang_lab,$nama_barang_lab,$kategori_barang_lab,$satuan_barang_lab)==TRUE)
		

		$filename = $id_pasien.".png"; // The filename - this could be specified by the user as another form field
		$filetype = "png"; // The file image type

		// Read the uploaded file from the form data and convert it to binary format
		$img = $_POST['save_remote_data'];
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);

	}

	function savediagnosa()
	{
		$data = $_POST['photo'];
		list($type, $data) = explode(';', $data);
		list(, $data)      = explode(',', $data);
		$data = base64_decode($data);

		mkdir($_SERVER['DOCUMENT_ROOT'] . "/photos");

		file_put_contents($_SERVER['DOCUMENT_ROOT'] . "/assets/image/".time().'.png', $data);
		die;
	}
}