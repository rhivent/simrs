<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pasien extends CI_Controller {
    function index()
    {
        $this->load->model('inap/inapmodel','um');
		$data['is_data']=$this->um->getPasien();		
        $this->load->view('inap/pasienview',$data);
    }

    function detailtindakan()
    {
        $id_reg_inap=$_GET['uid'];
        $nomor=$_GET['nomor'];
        $this->load->model('inap/inapmodel');
        $data['is_pasien']=$this->inapmodel->getpasienbyid($nomor);
        $data['is_data']=$this->inapmodel->detail_tindakan($id_reg_inap);
        $data['is_obat']=$this->inapmodel->detail_obat($id_reg_inap);		
        $this->load->view('inap/pasiendetail',$data);
    }

    function tambahtindakan()
    {
        $id_reg_inap=$_GET['uid'];
        $this->load->model('inap/inapmodel');
        $data['is_pasien']=$this->inapmodel->getpasienaddtindakan($id_reg_inap);
        $this->load->view('inap/adddetailtindakan',$data);
    }

    function adddetailtindakan()
    {
        $id_reg_inap=$_GET['uid'];
        $data['id_reg_inap']=$id_reg_inap;
        $this->load->model('inap/inapmodel');
        $data['is_data']=$this->inapmodel->getAlltindakan();
        $data['is_pasien']=$this->inapmodel->caripasien($id_reg_inap);
        $this->load->view('inap/adddetailtindakan',$data);
    }

    function adddetailobat()
    {
        $id_reg_inap=$_GET['uid'];
        $data['id_reg_inap']=$id_reg_inap;
        $this->load->model('inap/inapmodel');
        $data['is_data']=$this->inapmodel->getAllobat();
        $data['is_pasien']=$this->inapmodel->caripasien($id_reg_inap);
        $this->load->view('inap/adddetailobat',$data);
    }

    function simpantindakan()
    {
        $this->load->model('inap/inapmodel');
        $this->load->library('secure_library');
		$this->secure_library->filter_post('tindakan','required');
        $this->secure_library->filter_post('nomor','required');
        if($this->secure_library->start_post()==TRUE)
		{     
			$this->load->model('inap/inapmodel');
			$id_tindakan=$this->input->post('tindakan');
            $nomor=$this->input->post('nomor');
            $id_reg_inap=$this->input->post('id_reg_inap');
            $tanggal_tindakan=date('Y-m-d H:m:s');
            $petugas=getInfoUser('nama');

			if($this->inapmodel->simpantindakan($id_reg_inap,$id_tindakan,$tanggal_tindakan,$petugas)==TRUE)
			{
				$data['isok']="Tindakan sukses ditambahkan";
                $data['id_reg_inap']=$id_reg_inap;
                $data['is_data']=$this->inapmodel->getAlltindakan();
                $data['is_pasien']=$this->inapmodel->caripasien($id_reg_inap);
                $data['id_reg_inap']=$id_reg_inap;
                $this->load->view('inap/adddetailtindakan',$data);
			}else{
                $data['isok']="Tindakan gagal ditambahkan";
                $data['id_reg_inap']=$id_reg_inap;
                $data['is_pasien']=$this->inapmodel->caripasien($id_reg_inap);
				$data['is_data']=$this->inapmodel->getAlltindakan();
                $this->load->view('inap/adddetailtindakan',$data);
            }
		}else{	
                $data['id_reg_inap']=$id_reg_inap;	
				$data['is_data']=$this->inapmodel->getAlltindakan();
                $this->load->view('inap/adddetailtindakan',$data);
		}
    }

    function simpanobat()
    {
        $this->load->model('inap/inapmodel');
        $this->load->library('secure_library');
		$this->secure_library->filter_post('id_obat','required');
        $this->secure_library->filter_post('jumlah','required');
        if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('inap/inapmodel');
			$id_obat=$this->input->post('id_obat');
            $jumlah=$this->input->post('jumlah');
            $id_reg_inap=$this->input->post('id_reg_inap');
            $tanggal_tindakan=date('Y-m-d H:m:s');
            $petugas=getInfoUser('nama');

			if($this->inapmodel->simpanobat($id_reg_inap,$id_obat,$jumlah,$tanggal_tindakan,$petugas)==TRUE)
			{
				$data['isok']="Obar sukses ditambahkan";
                $data['is_data']=$this->inapmodel->getAllobat();
                $data['id_reg_inap']=$id_reg_inap;
                $data['is_pasien']=$this->inapmodel->caripasien($id_reg_inap);
                $data['id_reg_inap']=$id_reg_inap;
                $this->load->view('inap/adddetailobat',$data);
			// }else{
   //              $data['isok']="Obar gagal ditambahkan";
   //              $this->load->view('inap/adddetailobat',$data);
			}else{
                $data['isok']="Tindakan gagal ditambahkan";
                $data['id_reg_inap']=$id_reg_inap;
                $data['is_pasien']=$this->inapmodel->caripasien($id_reg_inap);
				$data['is_data']=$this->inapmodel->getAllobat();
                $this->load->view('inap/adddetailobat',$data);
            }
		}else{			
                $data['id_reg_inap']=$id_reg_inap;
				$data['is_data']=$this->inapmodel->getAllobat();
                $this->load->view('inap/adddetailobat',$data);
		}
    }

    function laporanpasien()
    {
        $this->load->library('secure_library');
        $this->secure_library->filter_post('tanggal_awal','required');
        $this->secure_library->filter_post('tanggal_akhir','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('inap/inapmodel');
            $tanggal_awal=$this->input->post('tanggal_awal');
            $tanggal_akhir=$this->input->post('tanggal_akhir');

			if($this->inapmodel->laporan_pasien($tanggal_awal,$tanggal_akhir)==TRUE)
			{
                $data['is_data']=$this->inapmodel->laporan_pasien($tanggal_awal,$tanggal_akhir);
                $data['tanggal_awal']=$tanggal_awal;
                $data['tanggal_akhir']=$tanggal_akhir;
				$this->load->view('inap/laporan_pasien',$data);
			}else{
                echo "Error Data";
            }
		}else{			
			$this->load->view('inap/laporanpasien');
		}
    }

    function laporanbidanshift()
    {
        $this->load->library('secure_library');
        $this->secure_library->filter_post('tanggal_awal','required');
        $this->secure_library->filter_post('jam_awal','required');
        $this->secure_library->filter_post('tanggal_akhir','required');
        $this->secure_library->filter_post('jam_akhir','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('inap/inapmodel');
            $tanggal_awal=$this->input->post('tanggal_awal');
            $jam_awal=$this->input->post('jam_awal');
            $tanggal_akhir=$this->input->post('tanggal_akhir');
            $jam_akhir=$this->input->post('jam_akhir');

			if($this->inapmodel->laporan_bidan($tanggal_awal,$tanggal_akhir,$jam_awal,$jam_akhir)==TRUE)
			{
                $data['is_data']=$this->inapmodel->laporan_bidan($tanggal_awal,$tanggal_akhir,$jam_awal,$jam_akhir);
                $data['tanggal_awal']=$tanggal_awal;
                $data['jam_awal']=$tanggal_awal;
                $data['tanggal_akhir']=$tanggal_akhir;
                $data['jam_akhir']=$tanggal_akhir;
				$this->load->view('inap/laporan_bidan',$data);
			}else{
                echo "Error Data";
            }
		}else{			
			$this->load->view('inap/laporanbidan');
		}
    }
}