<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
		
	function index()
	{
		if(getRole()!="inap")
		{
			redirect('inap');
		}
		$this->load->view('inap/index');
	}
}