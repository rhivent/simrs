<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master extends CI_Controller {

	//transaksi tindakan
    function tindakan()
    {
        $this->load->model('inap/inapmodel','um');
		$data['is_data']=$this->um->getTindakan();		
		$this->load->view('inap/tindakanview',$data);
    }

    function addtindakan()
    {
        $this->load->library('secure_library');
		$this->secure_library->filter_post('nama_tindakan','required');
		$this->secure_library->filter_post('harga','required');
        $this->secure_library->filter_post('keterangan','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('inap/inapmodel');
			$nama_tindakan=$this->input->post('nama_tindakan');
			$harga=$this->input->post('harga');
			$keterangan=$this->input->post('keterangan');

			if($this->inapmodel->create_tindakan($nama_tindakan,$harga,$keterangan)==TRUE)
			{
				$data['isok']="Tindakan sukses ditambahkan";
				$this->load->view('inap/addtindakan',$data);
			}else{
                echo "Error Tambah Data";
            }
		}else{			
			$this->load->view('inap/addtindakan');
		}
    }

    function gettindakanbyid()
    {
        $id_tindakan=$_GET['uid'];
        $this->load->model('inap/inapmodel');
        $data['update'] = $this->inapmodel->getTindakanByID($id_tindakan);
        $this->load->view('inap/updatetindakan',$data);

    }

    function updatetindakan()
    {
        $this->load->library('secure_library');
        $this->secure_library->filter_post('id_tindakan','required');
		$this->secure_library->filter_post('nama_tindakan','required');
		$this->secure_library->filter_post('harga','required');
        $this->secure_library->filter_post('keterangan','required');
		if($this->secure_library->start_post()==TRUE)
		{
            $this->load->model('inap/inapmodel');
            $id_tindakan=$this->input->post('id_tindakan');
			$nama_tindakan=$this->input->post('nama_tindakan');
			$harga=$this->input->post('harga');
			$keterangan=$this->input->post('keterangan');

			if($this->inapmodel->update_tindakan($id_tindakan,$nama_tindakan,$harga,$keterangan)==TRUE)
			{
                $data['isok']="Tindakan sukses diupdate";
                $id_tindakan=$this->input->post('id_tindakan');	
                $data['update'] = $this->inapmodel->getTindakanByID($id_tindakan);
				$this->load->view('inap/updatetindakan',$data);
			}else{
                $data['isok']="Tindakan gagal diupdate";
                $id_tindakan=$this->input->post('id_tindakan');	
                $data['update'] = $this->inapmodel->getTindakanByID($id_tindakan);
				$this->load->view('inap/updatetindakan',$data);
            }
		}else{		
            $id_tindakan=$this->input->post('id_tindakan');	
            $data['update'] = $this->inapmodel->getTindakanByID($id_tindakan);
			$this->load->view('inap/updatetindakan',$data);
		}
    }
    
    function deletetindakan(){
        $id_tindakan=$_GET['uid'];
		$this->load->model('inap/inapmodel');
		if($this->inapmodel->delete_tindakan($id_tindakan)==TRUE)
		{
			redirect('inap/master/tindakan');
		}else{
			redirect('inap/master/tindakan');
		}
	}


	//pulang pasien
	function pulang(){
		$id_reg_pas=$_GET['uid'];
		$nomor=$_GET['nomor'];
		echo '<p>Belum ada halaman</p>';
		return redirect('inap/pasien');
	}
	
	//transaksi ruangan
	function ruangan()
    {
        $this->load->model('inap/inapmodel','um');
		$data['is_data']=$this->um->getRuangan();		
		$this->load->view('inap/ruanganview',$data);
	}

	function inforuangan()
    {
        $this->load->model('inap/inapmodel','um');
		$data['is_data']=$this->um->get_Ruangan();		
		$this->load->view('inap/ruangan_view',$data);
	}
	
	function addruangan()
    {
        $this->load->library('secure_library');
		$this->secure_library->filter_post('nama_ruangan','required');
		$this->secure_library->filter_post('kelas','required');
		$this->secure_library->filter_post('harga','required');
        $this->secure_library->filter_post('status','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('inap/inapmodel');
			$nama_ruangan=$this->input->post('nama_ruangan');
			$status=$this->input->post('status');
			$harga=$this->input->post('harga');
			$kelas=$this->input->post('kelas');

			if($this->inapmodel->create_ruangan($nama_ruangan,$kelas,$harga,$status)==TRUE)
			{
				$data['isok']="Ruangan sukses ditambahkan";
				$this->load->view('inap/addruangan',$data);
			}else{
                $data['isok']="Ruangan gagal ditambahkan";
				$this->load->view('inap/addruangan',$data);
            }
		}else{			
			$this->load->view('inap/addruangan');
		}
	}
	
	function deleteruangan(){
        $id_ruangan=$_GET['uid'];
		$this->load->model('inap/inapmodel');
		if($this->inapmodel->delete_ruangan($id_ruangan)==TRUE)
		{
			redirect('inap/master/ruangan');
		}else{
			redirect('inap/master/ruangan');
		}
	}

	function getruanganbyid()
    {
        $id_ruangan=$_GET['uid'];
        $this->load->model('inap/inapmodel');
        $data['update'] = $this->inapmodel->getRuanganByID($id_ruangan);
        $this->load->view('inap/updateruangan',$data);

	}
	
	function updateruangan()
    {
        $this->load->library('secure_library');
        $this->secure_library->filter_post('id_ruangan','required');
		$this->secure_library->filter_post('nama_ruangan','required');
		$this->secure_library->filter_post('harga','required');
		$this->secure_library->filter_post('kelas','required');
		$this->secure_library->filter_post('status','required');
		if($this->secure_library->start_post()==TRUE)
		{
            $this->load->model('inap/inapmodel');
            $id_ruangan=$this->input->post('id_ruangan');
			$nama_ruangan=$this->input->post('nama_ruangan');
			$harga=$this->input->post('harga');
			$kelas=$this->input->post('kelas');
			$status=$this->input->post('status');
			

			if($this->inapmodel->update_ruangan($id_ruangan,$nama_ruangan,$kelas,$harga,$status)==TRUE)
			{
                $data['isok']="Ruangan sukses diupdate";
                $id_ruangan=$this->input->post('id_ruangan');	
                $data['update'] = $this->inapmodel->getRuanganByID($id_ruangan);
				$this->load->view('inap/updateruangan',$data);
			}else{
                $data['isok']="Ruangan gagal diupdate";
                $id_ruangan=$this->input->post('id_ruangan');	
                $data['update'] = $this->inapmodel->getRuanganByID($id_ruangan);
				$this->load->view('inap/updatetruangan',$data);
            }
		}else{		
            $id_ruangan=$this->input->post('id_ruangan');	
            $data['update'] = $this->inapmodel->getRuanganByID($id_ruangan);
			$this->load->view('inap/updateruangan',$data);
		}
    }
}