<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transaksi extends CI_Controller {
	
		
	function index()
	{
		if(getRole()!="kasir")
		{
			redirect('kasir');
		}
		$this->load->view('kasir/index');
    }
    
  function inkaso()
  {
		$this->load->model('kasir/kasirmodel');
		if($this->kasirmodel->inkaso()==TRUE)
		{
			$data['is_data']=$this->kasirmodel->inkaso();
			$this->load->view('kasir/inkaso',$data);
		}else{
			echo "Error Data";
		}
		// echo "<pre><code>";
		// print_r($this->kasirmodel->inkaso());	
		// echo "</code></pre>";
	}	

	function detailinkaso()
	{
		$id_transaksi=$_GET['uid'];
		$this->load->model('kasir/kasirmodel');
		if($this->kasirmodel->detailinkaso($id_transaksi)==TRUE)
		{
			$data['is_data']=$this->kasirmodel->detailinkaso($id_transaksi);
			$data['is_total']=$this->kasirmodel->totalinkaso($id_transaksi);
			$this->load->view('kasir/detailinkaso',$data);
		}else{
			echo "Error Data";
		}			
		
	}

	function bayar()
	{
		$id_transaksi=$_GET['uid'];
		$this->load->model('kasir/kasirmodel');
		if($this->kasirmodel->bayar($id_transaksi)==TRUE)
		{
			$data['is_data']=$this->kasirmodel->inkaso();
			$this->load->view('kasir/inkaso',$data);
		}else{
			echo "Error Data";
		}
	}

	function rawatinap()
	{
		$this->load->library('secure_library');
        $this->secure_library->filter_post('kode','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('kasir/kasirmodel');
            $cari=$this->input->post('kode');
			
			if($this->kasirmodel->find($cari)==TRUE)
			{
				$data['detail']=$this->pencarianmodel->find($cari);
				$this->load->view('kasir/hasilcaripasien',$data);
			}else{
            	redirect(site_url('kasir/transaksi/rawatinap'));
            }
		}else{			
			$this->load->view('kasir/cariinap');
		}
	}

	function tampilrawatinap()
	{
		$kode=$_GET['kode'];
		$this->load->library('database_library');
		$this->database_library->pake_table('reg_inap');
		$arraysearch="";
	
			$arraysearch=array(
				'reg_inap.nomor'=>$kode,
				);
			//"penjualan_apotik_rs.NoMR='123'";	
		
		$datp=$this->database_library->ambil_data_array($arraysearch,"tanggal","desc");
		if(!empty($datp))
		{
		echo '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
			<thead>
			<th>Tanggal</th>
			<th>No MR</th>
			<th>Nama</th>
			<th>Jenis Rawat</th>
			<th>Aksi</th>
			</thead>';
			
		foreach($datp as $row)
		{
			echo '<tr>';
			echo '<td>'.$row->tanggal.'</td>';
			echo '<td>'.$row->NoMR.'</td>';
			echo '<td>'.$row->Nama.'</td>';
			echo '<td>'.$row->jenisrawat.'</td>';
			echo '<td><a class="btn" target="_blank" href="'.base_url('apotik/laporan/repMRprint?nomr='.$row->NoMR.'&nama='.$row->Nama.'').'"><i class="icon-print"></i>CETAK</a></td>';
			echo '</tr>';
		}
		
			echo '</table>';
		}else{
			echo "DATA KOSONG";
		}
	}

	function ujilab()
	{
		$this->load->library('secure_library');
        $this->secure_library->filter_post('kode','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('kasir/kasirmodel');
            $cari=$this->input->post('kode');
			
			if($this->kasirmodel->find($cari)==TRUE)
			{
				$data['detail']=$this->pencarianmodel->find($cari);
				$this->load->view('kasir/hasilcaripasien',$data);
			}else{
                redirect(site_url('kasir/transaksi/ujilab'));
            }
		}else{			
			$this->load->view('kasir/carilab');
		}
	}
}