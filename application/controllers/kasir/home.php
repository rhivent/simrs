<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	
		
	function index()
	{
		if(getRole()!="kasir")
		{
			redirect('kasir');
		}
		$this->load->view('kasir/index');
	}
	
}