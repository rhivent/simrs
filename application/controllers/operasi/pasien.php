<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pasien extends CI_Controller {
	
	function index()
	{
		$this->load->model('operasi/Operasimodel');
		$data['is_data']=$this->Operasimodel->getPasien();
			$this->load->view('operasi/pasienview',$data);
	}
	
	function detailtindakan()
    {
        $id_reg_inap=$_GET['uid'];
        $nomor=$_GET['nomor'];
        $this->load->model('operasi/Operasimodel');
        $data['is_pasien']=$this->Operasimodel->getpasienbyid($nomor);
        $data['is_data']=$this->Operasimodel->detail_tindakan($id_reg_inap);
        $data['is_obat']=$this->Operasimodel->detail_obat($id_reg_inap);		
		$this->load->view('operasi/pasiendetail',$data);
	}
	
	
}