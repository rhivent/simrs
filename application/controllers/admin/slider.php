<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('database_library');
	}

	public function index()
	{
		$this->database_library->pake_table('options_slider');
		$data['is_data']=$this->database_library->ambil_data();
		// echo "<pre>";
		// var_dump($data['isdata']);
		// echo "</pre>";

		// foreach ($data['isdata'] as $data_he) {
		// 	echo $data_he->id_slider . "<br/>";
		// }

		// die;
		$this->load->view('admin/sliderview',$data);
    }

    public function create(){
    	// 
    }
    
    public function detail()
    {
        $id=$_GET['uid'];
		$this->load->model('pendaftaran/poliklinikmodel');
        $data['detail']=$this->poliklinikmodel->detail($id);
        $data['riwayat']=$this->poliklinikmodel->riwayat($id);
        $data['laboratorium']=$this->poliklinikmodel->laboratorium($id);
        //$data['riwayat']=$this->poliklinikmodel->riwayat($id);
		$this->load->view('pendaftaran/detailperiksa',$data);
    }

    function delete()
	{
		$id_reg_poli=$_GET['uid'];
		$this->load->model('pendaftaran/poliklinikmodel');
		if($this->poliklinikmodel->delete($id_reg_poli)==TRUE)
		{
			redirect('pendaftaran/poliklinikview');
		}else{
			redirect('pendaftaran/poliklinikview');
		}
	}
	
}