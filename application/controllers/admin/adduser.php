<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Adduser extends CI_Controller {
	
	function index()
	{
		$this->load->library('secure_library');
		$this->secure_library->filter_post('username','required');
		$this->secure_library->filter_post('password','required');
		$this->secure_library->filter_post('nama','required');
		if($this->secure_library->start_post()==TRUE)
		{
			$this->load->model('admin/usermodel');
			$username=$this->input->post('username');
			$password=$this->input->post('password');
			$role=$this->input->post('akses');
			$nama=$this->input->post('nama');

			// UPLOAD FOTO
	 		$foto ="";
            if(!empty($_FILES['foto']['name'])){
	            $foto .= time()."_".$_FILES["foto"]['name'];

		        if(move_uploaded_file($_FILES["foto"]["tmp_name"], './assets/dokter/'.$foto));
            }

			if($this->usermodel->create_user($username,$password,$role,$foto,$nama)==TRUE)
			{
				$data['isok']="Data sukses ditambahkan";
				$this->load->view('admin/adduserview',$data);
			}
		}else{			
			$this->load->view('admin/adduserview');
		}
	}
	
}