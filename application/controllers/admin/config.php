<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Config extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('secure_library');
	}

	function index()
	{
		$this->load->model('admin/configmodel');
		$this->secure_library->filter_post('site_name','required');
		$this->secure_library->filter_post('site_app','required');
		$this->secure_library->filter_post('site_alamat','required');
		$this->secure_library->filter_post('site_email','required');
		$this->secure_library->filter_post('site_telp','required');
		if($this->secure_library->start_post()==TRUE)
		{
	        
			// UPLOAD ICON
	 		$new_name_icon ="";
            if(!empty($_FILES['site_icon']['name'])){
	            $new_name_icon .= time()."_".$_FILES["site_icon"]['name'];
	        	$lokasi_icon = './assets/img/'.get_option('site_icon') ;
	        	if(file_exists($lokasi_icon)) 
	        		unlink($lokasi_icon);

		        if(move_uploaded_file($_FILES["site_icon"]["tmp_name"], './assets/img/'.$new_name_icon));

            }else{
            	$new_name_icon .= get_option('site_icon');
            }

			// UPLOAD ICON
	 		$new_name_logo ="";
	 		if(!empty($_FILES['site_logo']['name'])){
	            $new_name_logo .= time()."_".$_FILES["site_logo"]['name'];
	        	$lokasi_logo = './assets/img/'.get_option('site_logo') ;
	        	if(file_exists($lokasi_logo)) 
	        		unlink($lokasi_logo);

		        move_uploaded_file($_FILES["site_logo"]["tmp_name"], './assets/img/'.$new_name_logo);
            }else{
            	$new_name_logo .= get_option('site_logo');
            }
            
            // Simpan Data2 ke Database
			$this->simpan_db('site_name',$this->input->post('site_name'));
			$this->simpan_db('site_app',$this->input->post('site_app'));
			$this->simpan_db('site_icon',$new_name_icon);
			$this->simpan_db('site_logo',$new_name_logo);
			$this->simpan_db('site_alamat',$this->input->post('site_alamat'));
			$this->simpan_db('site_email',$this->input->post('site_email'));
			$this->simpan_db('site_telp',$this->input->post('site_telp'));
			$this->simpan_db('site_linkfb',$this->input->post('site_linkfb'));
			$this->simpan_db('site_linktw',$this->input->post('site_linktw'));
			$this->simpan_db('site_linkig',$this->input->post('site_linkig'));
			$this->simpan_db('site_linkline',$this->input->post('site_linkline'));
			$this->simpan_db('site_linklain',$this->input->post('site_linklain'));

			$data['isok']="Sukses mengubah konfigurasi";
			$data['isdata']=$this->configmodel->get_data_config();
			$this->load->view('admin/configview',$data);
		}else{
			get_option('site_name');
			$data['isdata']=$this->configmodel->get_data_config();
			$this->load->view('admin/configview',$data);
		}
	}

	function simpan_db($where,$datadb){
		$this->load->model('admin/configmodel');
		$arraysearch =array(
						'option_key'=>$where,
					);
		$data_update=array(
						'option_value'=>$datadb,
					);
		$this->configmodel->update_data($arraysearch,$data_update);
	}


}