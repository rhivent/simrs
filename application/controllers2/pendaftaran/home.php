<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendaftaran extends CI_Controller {
	
		
	function index()
	{
		if(getRole()!="pendaftaran")
		{
			redirect('login');
		}
		$this->load->view('pendaftaran/index');
	}
}