<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class kasirmodel extends CI_Model
{
    function inkaso()
    {
        $this->db->select('*');
		$this->db->from('pembelian');
        $this->db->join('supplier', 'supplier.id_supplier = pembelian.id_supplier');
        $this->db->where('status','BELUM');
        $this->db->order_by('tanggal');
		$query = $this->db->get();
		return $query->result(); 
    }

    function detailinkaso($id_transaksi)
    {
        //$this->db->select ( '*' ); 
        //$this->db->from ('pembelian');
        //$this->db->join ( 'pembelian_detail', 'pembelian_detail.id_transaksi = pembelian.id_transaksi','left');
        //$this->db->join ( 'supplier', 'supplier.id_supplier = pembelian.id_supplier','left');
        //$this->db->join ( 'obat', 'obat.id_obat = pembelian_detail.id_obat','left');
        //$this->db->where('pmbelian.id_transaksi',$id_transaksi);
        //$query = $this->db->get ();
        //return $query->result();

        $query = $this->db->query("SELECT * FROM (`pembelian`) JOIN `pembelian_detail` ON `pembelian_detail`.`id_transaksi` = `pembelian`.`id_transaksi` JOIN `supplier` ON `supplier`.`id_supplier` = `pembelian`.`id_supplier` JOIN `obat` ON `obat`.`id_obat` = `pembelian_detail`.`id_obat` WHERE `pembelian`.`id_transaksi` = '$id_transaksi'");  
        return $query->result();
    }

    function totalinkaso($id_transaksi)
    {
        $this->db->select('SUM(harga_beli) as total');
        $this->db->from('pembelian_detail');
        $this->db->where('id_transaksi',$id_transaksi);
        $query = $this->db->get();
        return $query->result();
    }

    function bayar($id_transaksi)
    {
        $this->load->library('database_library');
		$this->database_library->pake_table('pembelian');
		$arraysearch=array(
				'id_transaksi'=>$id_transaksi,
				);
		$data=array(
				'status'=>'LUNAS',
				);
		if($this->database_library->ubah_data($arraysearch,$data)==TRUE)
		{
			return true;
		}else{
			return false;
		}
    }

    function find()
    {
        
    }
}