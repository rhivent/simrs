<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operasimodel extends CI_Model
{

    function getObat()
		{
			
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('operasi/obatview').'?paging=true&';		
			$page = isset($_GET['page']) ? mysql_real_escape_string($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT * FROM barang_laboratorium limit $offset,$limit";
			$sql2="SELECT * FROM barang_laboratorium";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
        }

        function getPasien()
		{
			
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('operasi/pasienview').'?paging=true&';		
			$page = isset($_GET['page']) ? mysql_real_escape_string($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT reg_inap.*,data.*,ruangan.* FROM reg_inap,data,ruangan WHERE data.nomor=reg_inap.nomor AND reg_inap.id_ruang=ruangan.id_ruangan limit $offset,$limit";
			$sql2="SELECT reg_inap.*,data.*,ruangan.* FROM reg_inap,data,ruangan WHERE data.nomor=reg_inap.nomor AND reg_inap.id_ruang=ruangan.id_ruangan";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
  }
	
	function getTindakanByID($id_tindakan){
      $this->db->select('*');
			$this->db->from('tindakan_inap');
			$this->db->where('id_tindakan',$id_tindakan);
			$query = $this->db->get();  
			return $query->result(); 
  }
}