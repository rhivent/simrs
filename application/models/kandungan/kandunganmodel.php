<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kandunganmodel extends CI_Model
{

    function cari_pasien($id_pasien)
    {
        $this->db->select('*');
        $this->db->from('data');
        $this->db->where('nomor',$id_pasien);
        $query = $this->db->get();  
        return $query->result(); 
    }
        
    function laboratorium($id_pasien)
    {
        $this->db->select('*');
        $this->db->from('test_laboratorium'); 
        $this->db->join('jenis_lab', 'jenis_lab.id_jenis_lab=test_laboratorium.jenis_test', 'left');
        $this->db->where('test_laboratorium.id_pasien',$id_pasien);
        $query = $this->db->get(); 
        return $query->result();
    }
    
    function riwayat($id_pasien)
    {
        $this->db->select('*');
        $this->db->from('reg_poli'); 
        $this->db->where('id_pasien',$id_pasien);     
        $query = $this->db->get(); 
        return $query->result();
    }

}