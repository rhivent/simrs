<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rawatinapmodel extends CI_Model
{

    function getdata()
	{
		
		$this->load->library('database_library');
		
		$search="";
		$url='';
		
		$url=base_url('pendaftaran/rawatinapview').'?paging=true&';		
		$page = isset($_GET['page']) ? htmlentities($_GET['page']) : '1';
		
		$limit=20;
		$offset = ($page - 1) * $limit;
		$sql="SELECT data.*,reg_inap.*,ruangan.*,paket.*,rujukan.* FROM data,reg_inap,ruangan,paket,rujukan WHERE data.nomor=reg_inap.nomor AND reg_inap.id_paket=paket.id_paket AND reg_inap.id_ruang=ruangan.id_ruangan AND rujukan.id_rujukan=reg_inap.rujukan AND reg_inap.status='RAWAT' ORDER BY reg_inap.id_reg_inap DESC limit $offset,$limit";
		$sql2="SELECT data.*,reg_inap.*,ruangan.*,paket.*,rujukan.* FROM data,reg_inap,ruangan,paket,rujukan WHERE data.nomor=reg_inap.nomor AND reg_inap.id_paket=paket.id_paket AND reg_inap.id_ruang=ruangan.id_ruangan AND rujukan.id_rujukan=reg_inap.rujukan AND reg_inap.status='RAWAT' ORDER BY reg_inap.id_reg_inap DESC";
		$datas=$this->database_library->QueryData($sql);;
		$TR=$this->database_library->QueryNumRow($sql2);
		
		
		$tpage=ceil($TR/$limit);
		$this->load->library('pagination_library');
		$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
		$data['results']=$datas;
		
		return $data;
    }

    function detail($id)
    {
        $this->db->select('*');
        $this->db->from('data'); 
        $this->db->where('nomor',$id);     
        $query = $this->db->get(); 
        return $query->result();
    }

    function riwayat($id)
    {
        $this->db->select('*');
        $this->db->from('reg_poli'); 
        $this->db->where('id_pasien',$id);     
        $query = $this->db->get(); 
        return $query->result();
    }

    function laboratorium($id)
    {
        $this->db->select('*');
        $this->db->from('laboratorium'); 
        $this->db->join('jenis_lab', 'jenis_lab.id_jenis_lab=laboratorium.jenis_cek', 'left');
        $this->db->where('laboratorium.id_pasien',$id);
        $query = $this->db->get(); 
        return $query->result();
    }

    function find($nomor)
    {
        $this->db->select('*');
        $this->db->from('data'); 
        $this->db->where('nomor',$nomor);     
        $query = $this->db->get(); 
        return $query->result();
    }

    function poli()
    {
        $this->db->select('*');
        $this->db->from('poli');   
        $query = $this->db->get(); 
        return $query->result();
    }

    function simpan($nomor,$tanggal_masuk,$id_ruangan,$id_paket,$rujukan,$penanggung_jawab,$hub_dengan_pasien)
    {
        $this->load->library('database_library');
		
			$this->database_library->pake_table('reg_inap');
			$data=array(
				'nomor'=>$nomor,
				'tanggal_masuk'=>$tanggal_masuk,
				'id_ruang'=>$id_ruangan,
				'id_paket'=>$id_paket,
				'rujukan'=>$rujukan,
                'penanggung_jawab'=>$penanggung_jawab,
                'hub_dengan_pasien'=>$hub_dengan_pasien,
				);
				if($this->database_library->tambah_data($data)==TRUE)
				{
					return true;
				}else{
					return false;
				}
    }

    function updatekamar($id_ruangan)
    {
        $this->db->set('STATUS','ISI');
        $this->db->where('id_ruangan',$id_ruangan);     
        $query = $this->db->update('ruangan'); 
        return $query->result();
    }

    function delete($id_reg_inap)
	{
		$this->load->library('database_library');
		$this->database_library->pake_table('reg_inap');
		$arraysearch=array(
				'id_reg_inap'=>$id_reg_inap,
				);
		if($this->database_library->hapus_data($arraysearch)==TRUE)
		{
			return true;
		}else{
			return false;
		}
    }
    
    function getruangan()
    {
        $this->db->select('*');
        $this->db->from('ruangan'); 
        $this->db->where('status','KOSONG');  
        $query = $this->db->get(); 
        return $query->result();
    }

    function getpaket()
    {
        $this->db->select('*');
        $this->db->from('paket');   
        $query = $this->db->get(); 
        return $query->result();
    }

    function getrujukan()
    {
        $this->db->select('*');
        $this->db->from('rujukan');   
        $query = $this->db->get(); 
        return $query->result();
    }
}