<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pasienmodel extends CI_Model
{

    function getPasien()
	{
		
		$this->load->library('database_library');
		
		$search="";
		$url='';
		
		$url=base_url('pendaftaran/pasienview').'?paging=true&';		
		$page = isset($_GET['page']) ? htmlentities($_GET['page']) : '1';
		
		$limit=20;
		$offset = ($page - 1) * $limit;
		$sql="SELECT * FROM data limit $offset,$limit";
		$sql2="SELECT * FROM data";
		$datas=$this->database_library->QueryData($sql);;
		$TR=$this->database_library->QueryNumRow($sql2);
		
		
		$tpage=ceil($TR/$limit);
		$this->load->library('pagination_library');
		$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
		$data['results']=$datas;
		
		return $data;
    }

    function create_pasien($nama_pasien,$alamat_pasien,$no_telp,$tempat_lahir,$tanggal_lahir,$nomor){
			$this->load->library('database_library');
			$this->database_library->pake_table('data');
			$data=array(
				'nomor'=>$nomor,
				'nama_pasien'=>$nama_pasien,
				'alamat_pasien'=>$alamat_pasien,
				'no_telp'=>$no_telp,
				'tempat_lahir'=>$tempat_lahir,
				'tanggal_lahir'=>$tanggal_lahir,
				);
				if($this->database_library->tambah_data($data)==TRUE)
				{
					return true;
				}else{
					return false;
				}
	}

	function delete_pasien($idPasien)
	{
		$this->load->library('database_library');
		$this->database_library->pake_table('data');
		$arraysearch=array(
				'id'=>$idPasien,
				);
		if($this->database_library->hapus_data($arraysearch)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}

	function caripasien($id_pasien)
	{
		$this->db->select('*');
		$this->db->from('data');
		$this->db->where('id',$id_pasien);
		$query = $this->db->get();  
		return $query->result(); 
	}

	function cetak($nomor)
	{
		$this->db->select('*');
		$this->db->from('data');
		$this->db->where('nomor',$nomor);
		$query = $this->db->get();  
		return $query->result(); 
	}

	function get_pasien_by_id($id)
	{
		$this->db->select('*');
		$this->db->from('data');
		$this->db->where('id',$id);
		$query = $this->db->get();  
		return $query->result(); 		
	}

	function update($id,$nama_pasien,$alamat_pasien,$no_telp,$tempat_lahir,$tanggal_lahir,$nomor){
		$this->load->library('database_library');
		
			$this->database_library->pake_table('data');		
			$arr=array(
				'id'=>$id,
				);
			$arrupdate=array(
				'nomor'=>$nomor,
				'nama_pasien'=>$nama_pasien,
				'alamat_pasien'=>$alamat_pasien,
				'no_telp'=>$no_telp,
				'tempat_lahir'=>$tempat_lahir,
				'tanggal_lahir'=>$tanggal_lahir,
				);
			if($this->database_library->ubah_data($arr,$arrupdate)==TRUE)
			{
				return true;
			}else{
				return false;
			}
	}

	function nomorbaru()
	{
		$this->db->select('*');
		$this->db->from('data');
		$this->db->order_by('id','DESC');
		$this->db->limit(1);
		$query=$this->db->get();
		$nomor_lama = $query->row()->nomor;
		$formatnum = (int)substr($nomor_lama, 0,5);
		$nomorBaru = (string)($formatnum + 1) . date('m') . substr(date('Y'),2);
		return $nomorBaru; 
		// return $query->result(); 
	}

	function laporan_pasien($tanggal_awal,$tanggal_akhir)
	{
		$this->load->library('database_library');
		
		$search="";
		$url='';
		
		$url=base_url('pendaftaran/laporan/pasien').'?paging=true&';		
		$page = isset($_GET['page']) ? htmlentities($_GET['page']) : '1';
		
		$limit=100;
		$offset = ($page - 1) * $limit;
		$sql="SELECT reg_poli.*,data.*,poli.* FROM reg_poli,data,poli WHERE poli.id_poliklinik=reg_poli.id_poli AND reg_poli.id_pasien=data.nomor AND (tanggal_periksa BETWEEN '$tanggal_awal' AND '$tanggal_akhir') limit $offset,$limit";
		$sql2="SELECT reg_poli.*,data.*,poli.* FROM reg_poli,data,poli WHERE poli.id_poliklinik=reg_poli.id_poli AND reg_poli.id_pasien=data.nomor AND (tanggal_periksa BETWEEN '$tanggal_awal' AND '$tanggal_akhir')";
		$datas=$this->database_library->QueryData($sql);;
		$TR=$this->database_library->QueryNumRow($sql2);
		
		
		$tpage=ceil($TR/$limit);
		$this->load->library('pagination_library');
		$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
		$data['results']=$datas;
		
		return $data;
	}

	function total($tanggal_awal,$tanggal_akhir)
	{
		$this->load->library('database_library');
		
		$search="";
		$url='';
		
		$url=base_url('pendaftaran/laporan/pasien').'?paging=true&';		
		$page = isset($_GET['page']) ? htmlentities($_GET['page']) : '1';
		
		$limit=100;
		$offset = ($page - 1) * $limit;
		$sql="SELECT SUM(biaya) AS total FROM reg_poli WHERE tanggal_periksa BETWEEN '$tanggal_awal' AND '$tanggal_akhir' limit $offset,$limit";
		$sql2="SELECT SUM(biaya) AS total FROM reg_poli WHERE tanggal_periksa BETWEEN '$tanggal_awal' AND '$tanggal_akhir'";
		$datas=$this->database_library->QueryData($sql);;
		$TR=$this->database_library->QueryNumRow($sql2);
		
		
		$tpage=ceil($TR/$limit);
		$this->load->library('pagination_library');
		$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
		$data['results']=$datas;
		
		return $data;
	}
    
}