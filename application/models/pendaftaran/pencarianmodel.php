<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pencarianmodel extends CI_Model
{
    function find($cari)
    {   
        $this->db->select('*');
        $this->db->from('data'); 
        $this->db->like('nama_pasien',$cari);
        $query = $this->db->get(); 
        return $query->result();
    }

    function findpasien($cari)
    {
        $query = $this->db->query("SELECT * FROM data WHERE nomor LIKE '%$cari%' OR nama_pasien LIKE '%$cari%'");
        return $query->result();
    }

    function riwayat($cari)
    {
        $query = $this->db->query("SELECT data.*,reg_poli.* FROM data,reg_poli WHERE data.nomor=reg_poli.id_pasien AND (data.nomor LIKE '%$cari%' OR data.nama_pasien LIKE '%$cari%')");  
        return $query->result();
    }

    function laboratorium($cari)
    {
        $this->db->select('*');
        $this->db->from('test_laboratorium'); 
        $this->db->join('jenis_lab', 'jenis_lab.id_jenis_lab=test_laboratorium.jenis_test', 'left');
        $this->db->where('test_laboratorium.id_pasien',$cari);
        $query = $this->db->get(); 
        return $query->result();
    }

    function findpasieninap($cari)
    {
        $query = $this->db->query("SELECT data.*,reg_inap.* FROM data,reg_inap WHERE data.nomor=reg_inap.nomor AND (data.nomor LIKE '%$cari%' OR data.nama_pasien LIKE '%$cari%')");  
        return $query->result();
    }

    function findinap($cari)
    {
        $query = $this->db->query("SELECT data.*,reg_inap.*,ruangan.*,paket.*,rujukan.* FROM data,reg_inap,ruangan,paket,rujukan WHERE data.nomor=reg_inap.nomor AND reg_inap.id_paket=paket.id_paket AND reg_inap.id_ruang=ruangan.id_ruangan AND rujukan.id_rujukan=reg_inap.rujukan AND (data.nomor LIKE '%$cari%' OR data.nama_pasien LIKE '%$cari%')");  
        return $query->result();
    }
}