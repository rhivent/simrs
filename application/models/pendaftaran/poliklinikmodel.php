<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Poliklinikmodel extends CI_Model
{

    function getPasien()
	{
		
		$this->load->library('database_library');
		
		$search="";
		$url='';
		
		$url=base_url('pendaftaran/poliklinikview').'?paging=true&';		
		$page = isset($_GET['page']) ? mysql_real_escape_string($_GET['page']) : '1';
		
		$limit=20;
		$offset = ($page - 1) * $limit;
		$sql="SELECT data.*,reg_poli.*,poli.* FROM data,reg_poli,poli WHERE data.nomor=reg_poli.id_pasien AND poli.id_poliklinik=reg_poli.id_poli ORDER BY reg_poli.tanggal_periksa DESC limit $offset,$limit";
		$sql2="SELECT data.*,reg_poli.*,poli.* FROM data,reg_poli,poli WHERE data.nomor=reg_poli.id_pasien AND poli.id_poliklinik=reg_poli.id_poli ORDER BY reg_poli.tanggal_periksa DESC";
		$datas=$this->database_library->QueryData($sql);;
		$TR=$this->database_library->QueryNumRow($sql2);
		
		
		$tpage=ceil($TR/$limit);
		$this->load->library('pagination_library');
		$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
		$data['results']=$datas;
		
		return $data;
    }

    function detail($id)
    {
        $this->db->select('*');
        $this->db->from('data'); 
        $this->db->where('nomor',$id);     
        $query = $this->db->get(); 
        return $query->result();
    }

    function riwayat($id)
    {
        $this->db->select('*');
        $this->db->from('reg_poli'); 
        $this->db->where('id_pasien',$id);     
        $query = $this->db->get(); 
        return $query->result();
    }

    function laboratorium($id)
    {
        $this->db->select('*');
        $this->db->from('test_laboratorium'); 
        $this->db->join('jenis_lab', 'jenis_lab.id_jenis_lab=test_laboratorium.jenis_test', 'left');
        $this->db->where('test_laboratorium.id_pasien',$id);
        $query = $this->db->get(); 
        return $query->result();
    }

    function find($nomor)
    {
        $this->db->select('*');
        $this->db->from('data'); 
        $this->db->where('nomor',$nomor);     
        $query = $this->db->get(); 
        return $query->result();
    }

    function poli()
    {
        $this->db->select('*');
        $this->db->from('poli');   
        $query = $this->db->get(); 
        return $query->result();
    }

    function simpan($id_pasien,$tanggal_periksa,$id_poli,$no_registrasi,$keluhan,$bb,$tensi)
    {
        $this->load->library('database_library');
		
			$this->database_library->pake_table('reg_poli');
			$data=array(
				'id_pasien'=>$id_pasien,
				'tanggal_periksa'=>$tanggal_periksa,
				'id_poli'=>$id_poli,
				'no_registrasi'=>$no_registrasi,
				'keluhan'=>$keluhan,
                'bb'=>$bb,
                'tensi'=>$tensi,
				);
				if($this->database_library->tambah_data($data)==TRUE)
				{
					return true;
				}else{
					return false;
				}
    }

    function delete($id_reg_poli)
	{
		$this->load->library('database_library');
		$this->database_library->pake_table('reg_poli');
		$arraysearch=array(
				'id_reg_poli'=>$id_reg_poli,
				);
		if($this->database_library->hapus_data($arraysearch)==TRUE)
		{
			return true;
		}else{
			return false;
		}
	}
    
}