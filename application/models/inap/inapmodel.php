<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inapmodel extends CI_Model
{
		//transaksi tindakan
        function getTindakan()
		{
			
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('inap/master/tindakan').'?paging=true&';		
			$page = isset($_GET['page']) ? htmlentities($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT * FROM tindakan_inap limit $offset,$limit";
			$sql2="SELECT * FROM tindakan_inap";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
        }

        function getTindakanByID($id_tindakan)
        {
            $this->db->select('*');
			$this->db->from('tindakan_inap');
			$this->db->where('id_tindakan',$id_tindakan);
			$query = $this->db->get();  
			return $query->result(); 
        }

        function create_tindakan($nama_tindakan,$harga,$keterangan){
			$this->load->library('database_library');
			$this->database_library->pake_table('tindakan_inap');
			$data=array(
				'nama_tindakan'=>$nama_tindakan,
				'harga'=>$harga,
				'keterangan'=>$keterangan,
				);
				if($this->database_library->tambah_data($data)==TRUE)
				{
					return true;
				}else{
					return false;
				}
        }
        
        function update_tindakan($id_tindakan,$nama_tindakan,$harga,$keterangan)
        {
            $this->load->library('database_library');
		
			$this->database_library->pake_table('tindakan_inap');		
			$arr=array(
				'id_tindakan'=>$id_tindakan,
				);
			$arrupdate=array(
				'nama_tindakan'=>$nama_tindakan,
				'harga'=>$harga,
				'keterangan'=>$keterangan,
				);
			if($this->database_library->ubah_data($arr,$arrupdate)==TRUE)
			{
				return true;
			}else{
				return false;
			}
        }

        function delete_tindakan($id_tindakan)
		{
			$this->load->library('database_library');
			$this->database_library->pake_table('tindakan_inap');
			$arraysearch=array(
					'id_tindakan'=>$id_tindakan,
					);
			if($this->database_library->hapus_data($arraysearch)==TRUE)
			{
				return true;
			}else{
				return false;
			}
		}

		//transaksi ruangan
		function getRuangan()
		{
			
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('inap/master/ruangan').'?paging=true&';		
			$page = isset($_GET['page']) ? htmlentities($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT * FROM ruangan limit $offset,$limit";
			$sql2="SELECT * FROM ruangan";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}

		function get_Ruangan()
		{
			
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('inap/master/inforuangan').'?paging=true&';		
			$page = isset($_GET['page']) ? htmlentities($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT * FROM ruangan limit $offset,$limit";
			$sql2="SELECT * FROM ruangan";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}
		
		function create_ruangan($nama_ruangan,$kelas,$harga,$status){
			$this->load->library('database_library');
			$this->database_library->pake_table('ruangan');
			$data=array(
				'nama_ruangan'=>$nama_ruangan,
				'kelas'=>$kelas,
				'harga'=>$harga,
				'status'=>$status,
				);
				if($this->database_library->tambah_data($data)==TRUE)
				{
					return true;
				}else{
					return false;
				}
		}
		
		function delete_ruangan($id_ruangan)
		{
			$this->load->library('database_library');
			$this->database_library->pake_table('ruangan');
			$arraysearch=array(
					'id_ruangan'=>$id_ruangan,
					);
			if($this->database_library->hapus_data($arraysearch)==TRUE)
			{
				return true;
			}else{
				return false;
			}
		}

		function getRuanganByID($id_ruangan)
        {
            $this->db->select('*');
			$this->db->from('ruangan');
			$this->db->where('id_ruangan',$id_ruangan);
			$query = $this->db->get();  
			return $query->result(); 
		}
		
		function update_ruangan($id_ruangan,$nama_ruangan,$kelas,$harga,$status)
        {
            $this->load->library('database_library');
		
			$this->database_library->pake_table('ruangan');		
			$arr=array(
				'id_ruangan'=>$id_ruangan,
				);
			$arrupdate=array(
				'nama_ruangan'=>$nama_ruangan,
				'kelas'=>$kelas,
				'harga'=>$harga,
				'status'=>$status,
				);
			if($this->database_library->ubah_data($arr,$arrupdate)==TRUE)
			{
				return true;
			}else{
				return false;
			}
		}
		
		//model untuk data pasien rawat inap
		function getPasien()
		{
			
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('inap/pasienview').'?paging=true&';		
			$page = isset($_GET['page']) ? htmlentities($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT data.*,reg_inap.*,ruangan.* FROM data,reg_inap,ruangan WHERE data.nomor=reg_inap.nomor AND reg_inap.id_ruang=ruangan.id_ruangan limit $offset,$limit";
			$sql2="SELECT data.*,reg_inap.*,ruangan.* FROM data,reg_inap,ruangan WHERE data.nomor=reg_inap.nomor AND reg_inap.id_ruang=ruangan.id_ruangan ";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}
		
		function detail_tindakan($id_reg_inap)
        {
            $this->db->select('*');
			$this->db->from('detail_tindakan'); 
			$this->db->join('reg_inap', 'reg_inap.id_reg_inap=detail_tindakan.id_reg_inap');
			$this->db->join('tindakan_inap', 'tindakan_inap.id_tindakan=detail_tindakan.id_tindakan');
			$this->db->where('reg_inap.id_reg_inap',$id_reg_inap);
			$this->db->order_by('tanggal_tindakan');
			$query = $this->db->get(); 
			return $query->result();
		}

		function getpasienbyid($nomor)
        {
            $this->db->select('*');
			$this->db->from('data'); 
			$this->db->join('reg_inap', 'data.nomor=reg_inap.nomor','left');
			$this->db->where('data.nomor',$nomor);
			$query = $this->db->get(); 
			return $query->result();
		}

		function getpasienaddtindakan($id_reg_inap)
        {
            $this->db->select('*');
			$this->db->from('data'); 
			$this->db->join('reg_inap', 'data.nomor=reg_inap.nomor','left');
			$this->db->where('reg_inap.id_reg_inap',$id_reg_inap);
			$query = $this->db->get(); 
			return $query->result();
		}
		
		function getAlltindakan()
        {
            $this->db->select('*');
			$this->db->from('tindakan_inap'); 
			$query = $this->db->get(); 
			return $query->result();
		}

		function simpantindakan($id_reg_inap,$id_tindakan,$tanggal_tindakan,$petugas)
		{
			$this->load->library('database_library');
			$this->database_library->pake_table('detail_tindakan');
			$data=array(
				'id_reg_inap'=>$id_reg_inap,
				'id_tindakan'=>$id_tindakan,
				'tanggal_tindakan'=>$tanggal_tindakan,
				'petugas'=>$petugas,
				);
				if($this->database_library->tambah_data($data)==TRUE)
				{
					return true;
				}else{
					return false;
				}
		}

		function caripasien($id_reg_inap)
        {
            $this->db->select('*');
			$this->db->from('data'); 
			$this->db->join('reg_inap', 'data.nomor=reg_inap.nomor','left');
			$this->db->where('reg_inap.id_reg_inap',$id_reg_inap);
			$query = $this->db->get(); 
			return $query->result();
		}

		//tindakan obat
		function detail_obat($id_reg_inap)
        {
            $this->db->select('*');
			$this->db->from('detail_obat'); 
			$this->db->join('reg_inap', 'reg_inap.id_reg_inap=detail_obat.id_reg_inap');
			$this->db->join('obat', 'obat.id_obat=detail_obat.id_obat');
			$this->db->join('satuan_obat', 'obat.id_satuan_obat=satuan_obat.id_satuan_obat');
			$this->db->where('reg_inap.id_reg_inap',$id_reg_inap);
			$this->db->order_by('tanggal_tindakan');
			$query = $this->db->get(); 
			return $query->result();
		}

		function getAllobat()
        {
            $this->db->select('*');
			$this->db->from('obat'); 
			$this->db->join('satuan_obat', 'satuan_obat.id_satuan_obat=obat.id_satuan_obat','left');
			$query = $this->db->get(); 
			return $query->result();
		}

		function simpanobat($id_reg_inap,$id_obat,$jumlah,$tanggal_tindakan,$petugas)
		{
			$this->load->library('database_library');
			$this->database_library->pake_table('detail_obat');
			$data=array(
				'id_reg_inap'=>$id_reg_inap,
				'id_obat'=>$id_obat,
				'jumlah'=>$jumlah,
				'tanggal_tindakan'=>$tanggal_tindakan,
				'petugas'=>$petugas,
				);
				if($this->database_library->tambah_data($data)==TRUE)
				{
					return true;
				}else{
					return false;
				}
		}

		function laporan_pasien($tanggal_awal,$tanggal_akhir)
		{
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('inap/laporan_pasien').'?paging=true&';		
			$page = isset($_GET['page']) ? htmlentities($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT data.*,reg_inap.* FROM data,reg_inap WHERE data.nomor=reg_inap.nomor AND reg_inap.tanggal_masuk BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ORDER BY reg_inap.id_reg_inap";
			$sql2="SELECT data.*,reg_inap.* FROM data,reg_inap WHERE data.nomor=reg_inap.nomor AND reg_inap.tanggal_masuk BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ORDER BY reg_inap.id_reg_inap";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}

		function laporan_bidan($tanggal_awal,$tanggal_akhir,$jam_awal,$jam_akhir)
		{
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('inap/laporan_pasien').'?paging=true&';		
			$page = isset($_GET['page']) ? htmlentities($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT data.*,reg_inap.* FROM data,reg_inap WHERE data.nomor=reg_inap.nomor AND reg_inap.tanggal_masuk BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ORDER BY reg_inap.id_reg_inap";
			$sql2="SELECT data.*,reg_inap.* FROM data,reg_inap WHERE data.nomor=reg_inap.nomor AND reg_inap.tanggal_masuk BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ORDER BY reg_inap.id_reg_inap";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}

}
