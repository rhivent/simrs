<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laboratoriummodel extends CI_Model
{

    function getBarang()
		{
			
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('laboratorium/barangview').'?paging=true&';		
			$page = isset($_GET['page']) ? htmlentities($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT * FROM barang_laboratorium limit $offset,$limit";
			$sql2="SELECT * FROM barang_laboratorium";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}

		function create_barang($kode_barang_lab,$nama_barang_lab,$kategori_barang_lab,$satuan_barang_lab){
			$this->load->library('database_library');
			$this->database_library->pake_table('barang_laboratorium');
			$data=array(
				'kode_barang_lab'=>$kode_barang_lab,
				'nama_barang_lab'=>$nama_barang_lab,
				'kategori_barang_lab'=>$kategori_barang_lab,
				'satuan'=>$satuan_barang_lab,
				);
				if($this->database_library->tambah_data($data)==TRUE)
				{
					return true;
				}else{
					return false;
				}
		}

		function delete_barang($id_barang_lab)
		{
			$this->load->library('database_library');
			$this->database_library->pake_table('barang_laboratorium');
			$arraysearch=array(
					'id_barang_lab'=>$id_barang_lab,
					);
			if($this->database_library->hapus_data($arraysearch)==TRUE)
			{
				return true;
			}else{
				return false;
			}
		}

		function detail_barang($id)
		{
			$this->db->select('*');
			$this->db->from('barang_laboratorium');
			$this->db->where('id_barang_lab',$id);
			$query = $this->db->get();  
			return $query->result(); 
		}

		function update_barang($id_barang_lab,$kode_barang_lab,$nama_barang_lab,$kategori_barang_lab)
		{
			$this->load->library('database_library');
		
			$this->database_library->pake_table('barang_laboratorium');		
			$arr=array(
				'id_barang_lab'=>$id_barang_lab,
				);
			$arrupdate=array(
				'kode_barang_lab'=>$kode_barang_lab,
				'nama_barang_lab'=>$nama_barang_lab,
				'kategori_barang_lab'=>$kategori_barang_lab,
				);
			if($this->database_library->ubah_data($arr,$arrupdate)==TRUE)
			{
				return true;
			}else{
				return false;
			}
		}

		function get_barang_by_id($id)
		{
			$query=$this->db->get_where('barang_laboratorium',array('id_barang_lab'=>$id));
			return $query->row();
		}

		function update_stok($kode_barang,$jumlah_barang,$tanggal_input,$laboran)
		{
			$this->load->library('database_library');
			$this->database_library->pake_table('input_barang_laboratorium');
			$data=array(
				'kode_barang_lab'=>$kode_barang,
				'tanggal_input'=>$tanggal_input,
				'laboran'=>$laboran,
				'jumlah'=>$jumlah_barang,
				);
				if($this->database_library->tambah_data($data)==TRUE)
				{
					return true;
				}else{
					return false;
				}
		}

		function getTest()
		{
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('laboratorium/testview').'?paging=true&';		
			$page = isset($_GET['page']) ? mysql_real_escape_string($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT jenis_lab.*,test_laboratorium.*,data.* FROM jenis_lab,test_laboratorium,data WHERE jenis_lab.id_jenis_lab=test_laboratorium.jenis_test AND test_laboratorium.id_pasien=data.nomor limit $offset,$limit";
			$sql2="SELECT jenis_lab.*,test_laboratorium.*,data.* FROM jenis_lab,test_laboratorium,data WHERE jenis_lab.id_jenis_lab=test_laboratorium.jenis_test AND test_laboratorium.id_pasien=data.nomor";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}

		// cetak test lab
		function cetak_test_lab($id_test_lab){
			$this->db->select('data.*,test_laboratorium.*');
			$this->db->from('test_laboratorium');
			$this->db->join('data','data.nomor=test_laboratorium.id_pasien');
			$this->db->where('id_test_lab',$id_test_lab);
			$query = $this->db->get();
			return $query->result();
		}

		function cari_pasien($id_pasien)
		{
			$this->db->select('*');
			$this->db->from('data');
			$this->db->where('nomor',$id_pasien);
			$query = $this->db->get();  
			return $query->result(); 
		}

		function jenis_lab()
		{
			$this->db->select('*');
			$this->db->from('jenis_lab');
			$query = $this->db->get();  
			return $query->result(); 
		}

		function laboratorium($id_pasien)
    {
        $this->db->select('*');
        $this->db->from('test_laboratorium'); 
        $this->db->join('jenis_lab', 'jenis_lab.id_jenis_lab=test_laboratorium.jenis_test', 'left');
        $this->db->where('test_laboratorium.id_pasien',$id_pasien);
        $query = $this->db->get(); 
        return $query->result();
		}
		
		function laporan_gudang($tanggal_awal,$tanggal_akhir)
		{
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('laboratorium/laporan_gudang').'?paging=true&';		
			$page = isset($_GET['page']) ? htmlentities($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT barang_laboratorium.*,input_barang_laboratorium.* FROM barang_laboratorium,input_barang_laboratorium WHERE barang_laboratorium.kode_barang_lab=input_barang_laboratorium.kode_barang_lab AND input_barang_laboratorium.tanggal_input BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ORDER BY input_barang_laboratorium.tanggal_input limit $offset,$limit";
			$sql2="SELECT barang_laboratorium.*,input_barang_laboratorium.* FROM barang_laboratorium,input_barang_laboratorium WHERE barang_laboratorium.kode_barang_lab=input_barang_laboratorium.kode_barang_lab AND input_barang_laboratorium.tanggal_input BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ORDER BY input_barang_laboratorium.tanggal_input";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}

		function cetak_laporan_gudang($tanggal_awal,$tanggal_akhir)
		{
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('laboratorium/laporan_gudang').'?paging=true&';		
			$page = isset($_GET['page']) ? mysql_real_escape_string($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT barang_laboratorium.*,input_barang_laboratorium.* FROM barang_laboratorium,input_barang_laboratorium WHERE barang_laboratorium.kode_barang_lab=input_barang_laboratorium.kode_barang_lab AND input_barang_laboratorium.tanggal_input BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ORDER BY input_barang_laboratorium.tanggal_input ORDER BY tanggal_input";
			$sql2="SELECT barang_laboratorium.*,input_barang_laboratorium.* FROM barang_laboratorium,input_barang_laboratorium WHERE barang_laboratorium.kode_barang_lab=input_barang_laboratorium.kode_barang_lab AND input_barang_laboratorium.tanggal_input BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ORDER BY input_barang_laboratorium.tanggal_input";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}

		function laporan_barang($tanggal_awal,$tanggal_akhir,$id_barang_lab)
		{
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('laboratorium/laporan_barang').'?paging=true&';		
			$page = isset($_GET['page']) ? mysql_real_escape_string($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT barang_laboratorium.*,input_barang_laboratorium.* FROM barang_laboratorium,input_barang_laboratorium WHERE barang_laboratorium.kode_barang_lab=input_barang_laboratorium.kode_barang_lab AND input_barang_laboratorium.tanggal_input BETWEEN '$tanggal_awal' AND '$tanggal_akhir' AND barang_laboratorium.kode_barang_lab='$id_barang_lab' ORDER BY input_barang_laboratorium.tanggal_input limit $offset,$limit";
			$sql2="SELECT barang_laboratorium.*,input_barang_laboratorium.* FROM barang_laboratorium,input_barang_laboratorium WHERE barang_laboratorium.kode_barang_lab=input_barang_laboratorium.kode_barang_lab AND input_barang_laboratorium.tanggal_input BETWEEN '$tanggal_awal' AND '$tanggal_akhir' AND barang_laboratorium.kode_barang_lab='$id_barang_lab' ORDER BY input_barang_laboratorium.tanggal_input";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}

		function cetak_laporan_barang($tanggal_awal,$tanggal_akhir,$id_barang_lab)
		{
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('laboratorium/cetak_laporan_barang').'?paging=true&';		
			$page = isset($_GET['page']) ? mysql_real_escape_string($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT barang_laboratorium.*,input_barang_laboratorium.* FROM barang_laboratorium,input_barang_laboratorium WHERE barang_laboratorium.kode_barang_lab=input_barang_laboratorium.kode_barang_lab AND input_barang_laboratorium.tanggal_input BETWEEN '$tanggal_awal' AND '$tanggal_akhir' AND barang_laboratorium.kode_barang_lab='$id_barang_lab' ORDER BY input_barang_laboratorium.tanggal_input";
			$sql2="SELECT barang_laboratorium.*,input_barang_laboratorium.* FROM barang_laboratorium,input_barang_laboratorium WHERE barang_laboratorium.kode_barang_lab=input_barang_laboratorium.kode_barang_lab AND input_barang_laboratorium.tanggal_input BETWEEN '$tanggal_awal' AND '$tanggal_akhir' AND barang_laboratorium.kode_barang_lab='$id_barang_lab' ORDER BY input_barang_laboratorium.tanggal_input";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}

		function input_cart($kode_barang,$jumlah_barang,$laboran,$tanggal_output){
			$this->load->library('database_library');
			$this->database_library->pake_table('output_barang_laboratorium');
			$data=array(
				'kode_barang_lab'=>$kode_barang,
				'tanggal_output'=>$tanggal_output,
				'laboran'=>$laboran,
				'jumlah'=>$jumlah_barang,
				);
				if($this->database_library->tambah_data($data)==TRUE)
				{
					return true;
				}else{
					return false;
				}
		}

		function getJenis()
		{
			
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('laboratorium/jenisview').'?paging=true&';		
			$page = isset($_GET['page']) ? mysql_real_escape_string($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT * FROM jenis_lab limit $offset,$limit";
			$sql2="SELECT * FROM jenis_lab";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}

	function add_jenis($kode_jenis,$nama_jenis,$standart_test,$keterangan){
			$this->load->library('database_library');
			$this->database_library->pake_table('jenis_lab');
			$data=array(
				'kode_jenis'=>$kode_jenis,
				'nama_jenis'=>$nama_jenis,
				'standart_test'=>$standart_test,
				'keterangan'=>$keterangan,
				);
				if($this->database_library->tambah_data($data)==TRUE)
				{
					return true;
				}else{
					return false;
				}
		}

		function detail_jenis($id)
		{
			$this->db->select('*');
			$this->db->from('jenis_lab');
			$this->db->where('id_jenis_lab',$id);
			$query = $this->db->get();  
			return $query->result(); 
		}

		function update_jenis($id_jenis_lab,$kode_jenis,$nama_jenis,$standart_test,$keterangan)
		{
			$this->load->library('database_library');
		
			$this->database_library->pake_table('jenis_lab');		
			$arr=array(
				'id_jenis_lab'=>$id_jenis_lab,
				);
			$arrupdate=array(
				'kode_jenis'=>$kode_jenis,
				'nama_jenis'=>$nama_jenis,
				'standart_test'=>$standart_test,
				'keterangan'=>$keterangan,
				);
			if($this->database_library->ubah_data($arr,$arrupdate)==TRUE)
			{
				return true;
			}else{
				return false;
			}
		}

		function delete_jenis($id_jenis_lab)
		{
			$this->load->library('database_library');
			$this->database_library->pake_table('jenis_lab');
			$arraysearch=array(
					'id_jenis_lab'=>$id_jenis_lab,
					);
			if($this->database_library->hapus_data($arraysearch)==TRUE)
			{
				return true;
			}else{
				return false;
			}
		}

		function cart()
		{
				$this->db->select('*');
        $this->db->from('output_barang_laboratorium'); 
        $this->db->join('barang_laboratorium', 'barang_laboratorium.kode_barang_lab=output_barang_laboratorium.kode_barang_lab', 'left');
        $this->db->where('output_barang_laboratorium.status','N');
        $query = $this->db->get(); 
        return $query->result();
		}

		function simpan_test($id_pasien,$tanggal_test,$jenis_test,$hasil_test,$laboran)
		{
			$this->load->library('database_library');
			$this->database_library->pake_table('test_laboratorium');
			$data=array(
				'id_pasien'=>$id_pasien,
				'tanggal_test'=>$tanggal_test,
				'jenis_test'=>$jenis_test,
				'hasil_test'=>$hasil_test,
				'laboran'=>$laboran,
				);
				if($this->database_library->tambah_data($data)==TRUE)
				{
					return true;
				}else{
					return false;
				}
		}

		function proses()
		{
			$this->db->set('STATUS','Y');   
			$query = $this->db->update('output_barang_laboratorium'); 
			return $query->result();
		}

		//laporan barang keluar berdasarkan tanggal
		function laporan_gudang_keluar($tanggal_awal,$tanggal_akhir)
		{
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('laboratorium/outtanggal').'?paging=true&';		
			$page = isset($_GET['page']) ? mysql_real_escape_string($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT barang_laboratorium.*,output_barang_laboratorium.* FROM barang_laboratorium,output_barang_laboratorium WHERE barang_laboratorium.kode_barang_lab=output_barang_laboratorium.kode_barang_lab AND output_barang_laboratorium.tanggal_output BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ORDER BY output_barang_laboratorium.tanggal_output limit $offset,$limit";
			$sql2="SELECT barang_laboratorium.*,output_barang_laboratorium.* FROM barang_laboratorium,output_barang_laboratorium WHERE barang_laboratorium.kode_barang_lab=output_barang_laboratorium.kode_barang_lab AND output_barang_laboratorium.tanggal_output BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ORDER BY output_barang_laboratorium.tanggal_output";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}

		function cetak_laporan_keluar($tanggal_awal,$tanggal_akhir)
		{
			$this->load->library('database_library');
			
			$search="";
			$url='';
			
			$url=base_url('laboratorium/cetak_laporan_keluar').'?paging=true&';		
			$page = isset($_GET['page']) ? mysql_real_escape_string($_GET['page']) : '1';
			
			$limit=20;
			$offset = ($page - 1) * $limit;
			$sql="SELECT barang_laboratorium.*,output_barang_laboratorium.* FROM barang_laboratorium,output_barang_laboratorium WHERE barang_laboratorium.kode_barang_lab=output_barang_laboratorium.kode_barang_lab AND output_barang_laboratorium.tanggal_output BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ORDER BY output_barang_laboratorium.tanggal_output";
			$sql2="SELECT barang_laboratorium.*,output_barang_laboratorium.* FROM barang_laboratorium,output_barang_laboratorium WHERE barang_laboratorium.kode_barang_lab=output_barang_laboratorium.kode_barang_lab AND output_barang_laboratorium.tanggal_output BETWEEN '$tanggal_awal' AND '$tanggal_akhir' ORDER BY output_barang_laboratorium.tanggal_output";
			$datas=$this->database_library->QueryData($sql);;
			$TR=$this->database_library->QueryNumRow($sql2);
			
			
			$tpage=ceil($TR/$limit);
			$this->load->library('pagination_library');
			$data['links']=$this->pagination_library->paginate_anchor($url,$page,$TR,$limit);
			$data['results']=$datas;
			
			return $data;
		}
}