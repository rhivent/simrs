<?php
get_header();
?>
<h1>DETAIL INKASO</h1>
<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>#</td>
<td>ID TRANSAKSI</td>
<td>NAMA SUPPLIER</td>
<td>NAMA OBAT</td>
<td>JUMLAH</td>
<td>HARGA</td>
<td>SUB TOTAL</td>
</tr>
</thead>
<tbody>
<?php
	foreach($is_data as $row)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no;?></td>
<td><?= $row->id_transaksi;?></td>
<td><?= $row->nama_supplier;?></td>
<td><?= $row->nama_obat;?></td>
<td><?= $row->qty;?></td>
<td><?= number_format($row->harga_beli,2,",",".");?></td>
<td align="right"><?= number_format($row->harga_beli*$row->qty,2,",",".");?></td>
</tr>
<?php 
}
?>
<tr>
<td colspan="6">Total</td>
<?=
foreach($is_total as $row2)
{
?>
    <td>Rp. <?=$row2->total; ?></td>
<?=
}
?>

</tr>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error">DATA KOSONG</div>
<?php } ?>

<?= $is_data['links']; ?>
<?php
get_footer();
?>