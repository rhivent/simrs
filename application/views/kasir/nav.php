<div class="leftmenu">
  <ul class="nav nav-tabs nav-stacked">    
    <li class="dropdown"><a href="#"><span class="icon-shopping-cart"></span>Transaksi</a>
      <ul>
        <li><a href="<?=base_url('kasir/transaksi/inkaso');?>">Transaksi Pembelian/Inkaso</a></li>
        <li><a href="<?=base_url('apotik/laporan/repMR');?>">Transaksi Penjualan Apotek</a></li>
        <li><a href="<?=base_url('apotik/penjualan');?>">Transaksi Penjualan Apotek Umum</a></li>
        <li><a href="<?=base_url('kasir/transaksi/ujilab');?>">Transaksi Pembayaran Uji Lab</a></li>
        <li><a href="<?=base_url('kasir/transaksi/rawatinap');?>">Transaksi Pembayaran Rawat Inap</a></li>   
      </ul>
    </li>        
    <li class="dropdown"><a href="#"><span class="icon-file"></span> Laporan</a>
      <ul>
        <li><a href="<?=base_url('gudang/laporan/repbeli');?>">Laporan Inkaso</a></li>
        <li><a href="<?=base_url('apotik/laporan/repjualperiode');?>">Laporan Penjualan Apotek</a></li>
        <!--<li><a href="<?=base_url('kasir/laporan/lab');?>">Laporan Biaya Laboratorium</a></li> 
        <li><a href="<?=base_url('kasir/laporan/rawatinap');?>">Laporan Pembayaran Rawat Inap</a></li>-->
      </ul>
    </li>   
    </ul>
</div>
<!--leftmenu-->

</div>