<?php
get_header();
?>
<h1>INKASO BELUM TERBAYAR</h1>
<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover datatables">
<thead>
<tr>
<td>#</td>	
<td>ID TRANSAKSI</td>
<td>SUPPLIER</td>
<td>TANGGAL INKASO</td>
<td>TANGGAL JATUH TEMPO</td>
<td>NOMOR PO</td>
<td>STATUS</td>
<td>AKSI</td>
</tr>
</thead>
<tbody>
<?php
	foreach($is_data as $row)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no;?></td>
<td><?= $row->id_transaksi;?></td>
<td><?= $row->nama_supplier;?></td>
<td><?= $row->tanggal;?></td>
<td><?= $row->tanggal_jatuh_tempo;?></td>
<td><?= $row->nomorPO;?></td>
<td><?= $row->status;?></td>
<td>
<!--<a class="btn btn-small" href="<?=base_url();?>kasir/transaksi/detailinkaso?uid=<?=$row->id_transaksi;?>"><i class="icon-search"></i> DETAIL</a>-->
<a class="btn btn-small" href="<?=base_url();?>kasir/transaksi/bayar?uid=<?=$row->id_transaksi;?>"><i class="icon-edit"></i> BAYAR</a>
</td>
</tr>
<?php }
?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error">DATA KOSONG</div>
<?php } ?>

<?php // (!empty($is_data['links'])) ? $is_data['links'] : null; ?>
<?php
get_footer();
?>