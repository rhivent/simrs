<?php
get_header();
?>
<hr>
<a target="_blank" class="btn btn-success" href="<?=base_url();?>kandungan/pasien/mulaiperiksa?uid=<?=$id_pasien;?>"><i class="icon-plus"></i> Mulai Periksa</a>
<hr>

<div class="alert alert-success">Biodata Pasien</div>
<table class="table table-hover">
    <thead>
        <tr>
            <td><strong>No Rekam Medik</strong></td>
            <td><strong>Nama Lengkap</strong></td>
            <td><strong>Alamat</strong></td>
            <td><strong>No Telepon</strong></td>
            <td><strong>Tempat Lahir</strong></td>
            <td><strong>Tanggal Lahir</strong></td>
        </tr>
    </thead>
    <tbody>
    <?php
        foreach($pasien as $pasien)
        {	
    ?>
        <tr>
            <td><?php echo $pasien->nomor;?></td>
            <td><?php echo $pasien->nama_pasien;?></td>
            <td><?php echo $pasien->alamat_pasien;?></td>
            <td><?php echo $pasien->no_telp;?></td>
            <td><?php echo $pasien->tempat_lahir;?></td>
            <td><?php echo $pasien->tanggal_lahir;?></td>
        </tr>
    <?php   
        }
    ?>
    </tbody>
</table>
<div class="alert alert-success">Periksa Laboratorium</div>
<table class="table table-hover">
                        <thead>
                            <tr>
                                <td><strong>Tanggal Periksa</strong></td>
                                <td><strong>Jenis Pemeriksaan</strong></td>
                                <td><strong>Hasil</strong></td>
                                <td><strong>Laboran</strong></td>
                            </tr>
                        <thead>
                        <tbody>
                            <?php
                                foreach($laboratorium as $laboratorium)
                                {
                            ?>
                                <tr>
                                    <td><?php echo $laboratorium->tanggal_test ?></td>
                                    <td><?php echo $laboratorium->kode_jenis?></td>
                                    <td><?php echo $laboratorium->hasil_test?></td>
                                    <td><?php echo $laboratorium->laboran?></td>
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
<div class="alert alert-success">Riwayat Periksa</div>

<table class="table table-hover">
                        <thead>
                            <tr>
                                <td><strong>Tanggal Periksa</strong></td>
                                <td><strong>No Registrasi</strong></td>
                                <td><strong>Keluhan</strong></td>
                                <td><strong>Berat Badan</strong></td>
                                <td><strong>Tensi Darah</strong></td>
                                <td><strong>Diagnosa</strong></td>
                            </tr>
                        <thead>
                        <tbody>
                            <?php
                                foreach($riwayat as $riwayat)
                                {
                            ?>
                                <tr>
                                    <td><?php echo $riwayat->tanggal_periksa ?></td>
                                    <td><?php echo $riwayat->no_registrasi ?></td>
                                    <td><?php echo $riwayat->keluhan ?></td>
                                    <td><?php echo $riwayat->bb ?> Kg</td>
                                    <td><?php echo $riwayat->tensi ?></td>
                                    <td><?php echo $riwayat->diagnosa ?></td>
                                    
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>


<hr>

<?php
get_footer();
?>