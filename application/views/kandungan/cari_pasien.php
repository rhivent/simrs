<?php
get_header();
?>
<h1>Masukkan / Scan Barcode No Rekam Medis</h1><br>

<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('',$att);
?>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Nomor Rekam Medik</label>
        <div class="controls">
            <input type="number" id="inputEmail" name="id_pasien" placeholder="Masukkan Nomor Rekam Medik">
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-success">Proses</button>
        </div>
    </div>
</form>


<?php
get_footer();
?>