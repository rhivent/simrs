<?php
get_header_frontend();
?>
  	
	<!-- loader start -->

	<div class="loader">
		<div id="awsload-pageloading">
			<div class="awsload-wrap">
				<ul class="awsload-divi">
					<li></li>
					<li></li>
					<li></li>
					<li></li>
				</ul>
			</div>
		</div>
	</div>

	<!-- loader end -->
   
    <!-- Nav Section -->
    <nav class="navbar navbar-default navbar-fixed navbar-transparent dark navbar-scrollspy awesomenav" data-minus-value-desktop="88" data-minus-value-mobile="55" data-speed="1000">
	   
		<!-- End Top Search -->
        <div class="container">               
            <!-- Start Header Navigation -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#brand">                
					<img src="<?= base_url('/assets/img') ."/". get_option('site_logo');?>" class="logo logo-display" alt="">
					<img src="<?= base_url('/assets/img') ."/". get_option('site_logo');?>" class="logo logo-scrolled" alt="">
				</a>
            </div>
            <!-- End Header Navigation -->
    
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-menu">
                
                <ul class="nav navbar-nav navbar-right">
                    <li class="active scroll"><a href="#home">Beranda</a></li>
                    <li class="scroll"><a href="#services">Layanan</a></li>
                    <li class="scroll"><a href="#doctors">Dokter</a></li>
                    <li class="scroll"><a href="#procedures">Prosedur</a></li>
                    <li class="scroll"><a href="#babycare">Baby Care</a></li>
                    <li class="scroll"><a href="#facilities">Fasilitas</a></li>
                    <li class="scroll"><a href="#appointment">Pendaftaran</a></li>
                    <li class="scroll"><a href="#blog">Blog</a></li>
    				<li class="scroll"><a href="#contact">Kontak</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div>  
	</nav>

    
    <!--Slider Here-->
    <section class="dart-no-padding-tb"> 
        
        <div class="ms-fullscreen-template" id="slider1-wrapper">
            <!-- masterslider -->
            <div class="master-slider ms-skin-default" id="masterslider">
            	<?php foreach ($sliders as $key => $slider) {
	            	if($key%2 == 0){ 
	            ?>
			            <div class="ms-slide slide-1 slide1">
		                    <img src="<?= base_url('/assets/slider')."/".$slider->img_slider;?>" data-src="<?= base_url('/assets/slider') ."/".$slider->img_slider;?>" alt="lorem ipsum dolor sit"> 
		                    <h3 class="ms-layer bold-text-white bigtext" data-type="text" data-effect="back(1300)" data-duration="1800" data-delay="0" data-hide-effect="left(short,false)" >
		                        <?= $slider->text_slider ?>
		                    </h3>
		                    <h5 class="ms-layer btn" data-type="text" data-effect="rotate3dbottom(-70,0,0,180)" data-duration="2000" data-ease="easeInOutQuint"><a href="#appointment"><?= $slider->title_slider ?></a></h5>
		                </div>
            		<?php }else{ ?>
			            <div class="ms-slide slide-1 slide2">
		                    <img src="<?= base_url('/assets/slider')."/".$slider->img_slider;?>" data-src="<?= base_url('/assets/slider')."/".$slider->img_slider;?>" alt="lorem ipsum dolor sit"> 
		                    <h3 class="ms-layer bold-text-white bigtext" data-type="text" data-effect="back(1300)" data-duration="1800" data-delay="0" data-hide-effect="right(short,false)" style="right: 0;text-align: right;" >
		                        <?= $slider->text_slider ?>
		                    </h3>
		                    <h4 class="ms-layer captiontext" data-type="text" data-effect="rotate3dbottom(-70,0,0,180)" data-duration="2000" data-ease="easeInOutQuint" style="left: initial; right: 0;">
		                        <?= $slider->title_slider ?>
		                    </h4>
		                </div>
            		<?php } ?>
            	<?php } ?>
            </div>
            <!-- end of masterslider -->
        </div>
        
    </section>  
    
    <div class="clearfix"></div>    
    
    
    <!--welcome text-->
	<section class="wel-text dart-pb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<h1><span class="dart-fw-400">Welcome to the</span> <span class="pink">Healthy Pregnancy Center!</span></h1>
					<p>Sight Improvement Center provides optimum care in an efficient, cheerful and caring environment. At EyeDoctor, we conduct comprehensive eye exams using the most advanced technology available and provide high quality evaluation and treatment. Sight Improvement Center provides optimum care in an efficient, cheerful and caring environment. At EyeDoctor, we conduct comprehensive eye exams using the most advanced technology available and provide high quality evaluation and treatment.</p>
				</div>
			</div>
		</div>
	</section>  
	
	<!--services-->
	<section id="services" class="dart-pb-0">
    	<div class="container">        	
            
            <div class="row">
            
                <div class="col-md-8 col-sm-8">
                    <div class="row">
                    	<div class="col-md-12 col-sm-12">
                            <div class="dart-heading"> 
                                <h1 class="h2 dart-mt-0">Our Services</h1>
                                <hr>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="icon_box_hexa">
                                <div class="ibox_hexagon">
                                	<i class="fa fa-briefcase"></i>
                                </div>
                                <a href="#" target="_blank">
                                	<h3>Family planning</h3>
                                </a>
                            	<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some humour.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="icon_box_hexa">
                                <div class="ibox_hexagon">
                                	<i class="fa fa-briefcase"></i>
                                </div>
                                <a href="#" target="_blank">
                                	<h3>Fertility medication</h3>
                                </a>
                            	<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some humour.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="icon_box_hexa">
                                <div class="ibox_hexagon">
                                	<i class="fa fa-briefcase"></i>
                                </div>
                                <a href="#" target="_blank">
                                	<h3>Gynecology</h3>
                                </a>
                            	<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some humour.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="icon_box_hexa">
                                <div class="ibox_hexagon">
                                	<i class="fa fa-briefcase"></i>
                                </div>
                                <a href="#" target="_blank">
                                	<h3>Prenatal ultrasound</h3>
                                </a>
                            	<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some humour.</p>
                            </div>
                        </div>
                    </div>
                </div> 
                           	
                <div class="col-md-4 col-sm-4">
                	<div class="single_image">
                    	<img src="<?= base_url('/assets/frontend');?>/images/services-img.jpg" class="img-responsive" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </section>
 
	<!--team-->  
	<section id="doctors" class="team">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="heading">
						<h1>Our Specialised Doctor</h1>
						<img src="<?= base_url('/assets/frontend');?>/images/sep-1.png" alt="sep"/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="owl-carousel owl-theme">
					<?php 
						foreach($dokters as $dokter):
					?>
					<div class="team-block">
						<div class="team-wapper">
							<div class="team-avtar">
								<img src="<?= base_url('/assets/dokter')."/".$dokter->foto;?>" class="img-responsive" alt="doctor"/>
								<div class="ImageOverlayH"></div>							
							</div>
							<div class="conect">
								<h3><?= $dokter->nama; ?></h3>
								<!-- <ul class="list-inline">
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#"><i class="fa fa-instagram"></i></a></li>
								</ul> -->
							</div>
						</div>					
						<div class="team-info">
							<h4><?= $dokter->nama; ?></h4>
							<p><?= $dokter->role_special; ?></p>
						</div>
					</div>
					<?php endforeach; ?>

				</div>
			</div>
		</div>
	</section>
	  
	<!--procedures-->  
	<section id="procedures" class="procedures">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="heading">
						<h1>Procedures</h1>
						<img src="<?= base_url('/assets/frontend');?>/images/sep-1.png" alt="sep"/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 col-sm-4">
					<div class="content-block">
						<figure>
							<img src="<?= base_url('/assets/frontend');?>/images/image01.jpg" class="img-responsive" alt="img01">
							<figcaption>
								<h4>Prenatal care</h4>
								<span>Our comprehensive eye examinations check your eyes inside and out for glaucoma, cataracts and all other eye diseases as well as high blood pressure, diabetes and other systemic diseases.</span>
							</figcaption>
						</figure>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="content-block">
						<figure>
							<img src="<?= base_url('/assets/frontend');?>/images/image02.jpg" class="img-responsive" alt="img01">
							<figcaption>
								<h4>Morning Sickness</h4>
								<span>Our comprehensive eye examinations check your eyes inside and out for glaucoma, cataracts and all other eye diseases as well as high blood pressure, diabetes and other systemic diseases.</span>
							</figcaption>
						</figure>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="content-block">
						<figure>
							<img src="<?= base_url('/assets/frontend');?>/images/image03.jpg" class="img-responsive" alt="img01">
							<figcaption>
								<h4>Baby Vaccination</h4>
								<span>Our comprehensive eye examinations check your eyes inside and out for glaucoma, cataracts and all other eye diseases as well as high blood pressure, diabetes and other systemic diseases.</span>
							</figcaption>
						</figure>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="content-block">
						<figure>
							<img src="<?= base_url('/assets/frontend');?>/images/image04.jpg" class="img-responsive" alt="img01">
							<figcaption>
								<h4>Surgery Practice</h4>
								<span>Our comprehensive eye examinations check your eyes inside and out for glaucoma, cataracts and all other eye diseases as well as high blood pressure, diabetes and other systemic diseases.</span>
							</figcaption>
						</figure>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="content-block">
						<figure>
							<img src="<?= base_url('/assets/frontend');?>/images/image05.jpg" class="img-responsive" alt="img01">
							<figcaption>
								<h4>Diet Schedule</h4>
								<span>Our comprehensive eye examinations check your eyes inside and out for glaucoma, cataracts and all other eye diseases as well as high blood pressure, diabetes and other systemic diseases.</span>
							</figcaption>
						</figure>
					</div>
				</div>
				<div class="col-md-4 col-sm-4">
					<div class="content-block">
						<figure>
							<img src="<?= base_url('/assets/frontend');?>/images/image06.jpg" class="img-responsive" alt="img01">
							<figcaption>
								<h4>Expert’s Advice</h4>
								<span>Our comprehensive eye examinations check your eyes inside and out for glaucoma, cataracts and all other eye diseases as well as high blood pressure, diabetes and other systemic diseases.</span>
							</figcaption>
						</figure>
					</div>
				</div>
			</div>
		</div>
	</section>
	  
	<!--call to action-->  
	<section class="cta">
		<div class="container">
			<div class="row">
				<div class="cta-block clearfix">
					<div class="col-md-7 col-sm-7">
						<h1><span class="dart-fw-300">If you need a doctor?</span> <br>Make an appointment now !</h1>
					</div>
					<div class="col-md-5 col-sm-5">
						<div class="cta-btn">
							<a href="#" class="btn">get an appointment</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	  
	<!--baby care-->  
	<section id="babycare" class="babycare">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-sm-6">
					<img src="<?= base_url('/assets/frontend');?>/images/baby-care.jpg" class="img-responsive" alt="baby care" />
				</div>
				<div class="col-md-6 col-sm-6">
					<div class="canternt-wapper">
						<div class="dart-heading"> 
							<h1 class="h2 dart-mt-0">Baby Care</h1>
							<hr>
						</div>
						<p class="dart-fs-18">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
						<ul class="list-unstyled">
							<li><i class="fa fa-angle-double-right"></i>Couples Who Cannot Conceive Naturally</li>
							<li><i class="fa fa-angle-double-right"></i>Single Women Who Want to Get Pregnant</li>
							<li><i class="fa fa-angle-double-right"></i>Men With Fertility Issues</li>
							<li><i class="fa fa-angle-double-right"></i>Couples With Recurrent Pregnancy Losses</li>
							<li><i class="fa fa-angle-double-right"></i>Couples Carrying Genetically Inherited Disorders</li>
							<li><i class="fa fa-angle-double-right"></i>Cancer Patients Who Want to Preserve Their Eggs</li>
							<li><i class="fa fa-angle-double-right"></i>Women Who Want to Freeze Their Eggs</li>
							<li><i class="fa fa-angle-double-right"></i>Couples Who Cannot Conceive Naturally</li>
							<li><i class="fa fa-angle-double-right"></i>Single Women Who Want to Get Pregnant</li>
							<li><i class="fa fa-angle-double-right"></i>Men With Fertility Issues</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</section> 
	  
	<!--facilities-->  
	<section id="facilities" class="facilities">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="heading">
						<h1>Our New Facilities</h1>
						<img src="<?= base_url('/assets/frontend');?>/images/sep-1.png" alt="sep"/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="tabs tabs-style-iconbox">
					<nav>
						<ul>
							<li><a href="#section-iconbox-1" class="icon flaticon-medical"><span>Diagnostics</span></a></li>
							<li><a href="#section-iconbox-2" class="icon flaticon-dna"><span>Neurology</span></a></li>
							<li><a href="#section-iconbox-3" class="icon flaticon-cardiogram"><span>Cardiology</span></a></li>
							<li><a href="#section-iconbox-4" class="icon flaticon-blood"><span>Blood Test</span></a></li>
							<li><a href="#section-iconbox-5" class="icon flaticon-tooth"><span>Dental Care</span></a></li>
						</ul>
					</nav>
					<div class="content-wrap">
						<section id="section-iconbox-1">
							<div class="row">
								<div class="col-sm-6">
									<div class="fac-content">
										<div class="dart-heading"> 
											<h2 class="h2 dart-mt-0">Diagnostics</h2>
											<hr>
										</div>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid  unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.</p>
										<ul class="list-unstyled">
											<li><i class="fa fa-arrow-circle-right"></i>Curabitur vehicula nulla id eros maximus aliquam.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Vestibulum pharetra quam vel sapien viverra iaculis.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Cras vulputate nibh sed odio rhoncus luctus.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Donec dictum lectus sit amet fringilla volutpat.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Etiam in mi ut dui iaculis ultricies.</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="fac-img">
										<img src="<?= base_url('/assets/frontend');?>/images/tab-1.jpg" class="img-responsive" alt="baby care"/>
									</div>
								</div>
							</div>
						</section>
						<section id="section-iconbox-2">
							<div class="row">
								<div class="col-sm-6">
									<div class="fac-content">
										<div class="dart-heading"> 
											<h2 class="h2 dart-mt-0">Neurology</h2>
											<hr>
										</div>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid  unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.</p>
										<ul class="list-unstyled">
											<li><i class="fa fa-arrow-circle-right"></i>Curabitur vehicula nulla id eros maximus aliquam.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Vestibulum pharetra quam vel sapien viverra iaculis.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Cras vulputate nibh sed odio rhoncus luctus.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Donec dictum lectus sit amet fringilla volutpat.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Etiam in mi ut dui iaculis ultricies.</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="fac-img">
										<img src="<?= base_url('/assets/frontend');?>/images/tab-2.jpg" class="img-responsive" alt="baby care"/>
									</div>
								</div>
							</div>
						</section>
						<section id="section-iconbox-3">
							<div class="row">
								<div class="col-sm-6">
									<div class="fac-content">
										<div class="dart-heading"> 
											<h2 class="h2 dart-mt-0">Cardiology</h2>
											<hr>
										</div>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid  unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.</p>
										<ul class="list-unstyled">
											<li><i class="fa fa-arrow-circle-right"></i>Curabitur vehicula nulla id eros maximus aliquam.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Vestibulum pharetra quam vel sapien viverra iaculis.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Cras vulputate nibh sed odio rhoncus luctus.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Donec dictum lectus sit amet fringilla volutpat.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Etiam in mi ut dui iaculis ultricies.</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="fac-img">
										<img src="<?= base_url('/assets/frontend');?>/images/tab-3.jpg" class="img-responsive" alt="baby care"/>
									</div>
								</div>
							</div>
						</section>
						<section id="section-iconbox-4">
							<div class="row">
								<div class="col-sm-6">
									<div class="fac-content">
										<div class="dart-heading"> 
											<h2 class="h2 dart-mt-0">Blood Test</h2>
											<hr>
										</div>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid  unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.</p>
										<ul class="list-unstyled">
											<li><i class="fa fa-arrow-circle-right"></i>Curabitur vehicula nulla id eros maximus aliquam.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Vestibulum pharetra quam vel sapien viverra iaculis.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Cras vulputate nibh sed odio rhoncus luctus.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Donec dictum lectus sit amet fringilla volutpat.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Etiam in mi ut dui iaculis ultricies.</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="fac-img">
										<img src="<?= base_url('/assets/frontend');?>/images/tab-4.jpg" class="img-responsive" alt="baby care"/>
									</div>
								</div>
							</div>
						</section>
						<section id="section-iconbox-5">
							<div class="row">
								<div class="col-sm-6">
									<div class="fac-content">
										<div class="dart-heading"> 
											<h2 class="h2 dart-mt-0">Dental Care</h2>
											<hr>
										</div>
										<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincid  unt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci.</p>
										<ul class="list-unstyled">
											<li><i class="fa fa-arrow-circle-right"></i>Curabitur vehicula nulla id eros maximus aliquam.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Vestibulum pharetra quam vel sapien viverra iaculis.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Cras vulputate nibh sed odio rhoncus luctus.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Donec dictum lectus sit amet fringilla volutpat.</li>
											<li><i class="fa fa-arrow-circle-right"></i>Etiam in mi ut dui iaculis ultricies.</li>
										</ul>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="fac-img">
										<img src="<?= base_url('/assets/frontend');?>/images/tab-5.jpg" class="img-responsive" alt="baby care"/>
									</div>
								</div>
							</div>
						</section>
					</div><!-- /content -->
				</div><!-- /tabs -->
			</div>
		</div>
	</section>
	  
	<!--appointment--> 
	<section id="appointment" class="appointment">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-8">
					<div class="appo-form">
						<div class="dart-heading"> 
							<h1 class="dart-mt-0">Pendaftaran</h1>
							<hr>
							<p class="white">Silahkan daftar melalui form ini. Untuk mendapatkan kode pedaftaran pasien.</p>
						</div>
						<div class="reservation-form">
						<?php
							$att=array(
								'class'=>'form-inline',
								'role'=>'form',
								'id' => 'bookappointment',
								'name' => 'bookappointment',
								'action' => base_url('pendaftaran/addpasien'),
								'method' => 'post'
								);
							echo form_open('',$att);
						?>

                            <!-- <form class="form-inline" action="<?php // echo base_url('pendaftaran/addpasien');?>" id="bookappointment" name="bookappointment" method="post" > -->
                                <div class="form-inline clearfix">
									  <div class="form-group col-sm-12">
									  		<label><strong>Nomor REKAM MEDIS</strong> (*otomatis)</label>
											<input type="text" class="form-control" readonly name="nomor" value="<?= $nomor;?>" placeholder="Nomor Rekam Medis" required>
									  </div>
                                </div>
                                <div class="form-inline clearfix">
									  <div class="form-group col-sm-6">
											<input type="text" class="form-control" name="nama_pasien" placeholder="Nama Lengkap Anda *" required>
									  </div>
									  <div class="form-group col-sm-6">
											<input type="text" class="form-control" name="no_telp" placeholder="Nomor Telephone Anda *" required>
									  </div>
                                </div>
                               <!--  <div class="form-inline clearfix">
									<div class="form-group col-sm-6">
										<input type="tel" class="form-control" name="userMobileNumber" placeholder="Nomor Telephone *" required>
									</div>                                  
									<div class="form-group col-sm-6">
										<select class="form-control" name="layanan">
										  <option>Pilih Layanan</option>
										  <option>Periksa Rutin</option>
										  <option>Konsultasi Dokter</option>
										  <option>Urgent</option>
										  <option>Lainnya</option>
										</select>
									</div>                                  
                                </div> -->
                                <div class="form-inline clearfix">
									<div class="form-group col-sm-6">
										<input type="text" class="form-control" name="tempat_lahir" placeholder="Tempat Lahir">
									</div>
									<div class="form-group col-sm-6">
										<!-- <div class="input-group date " id="start"> -->
										<div class="input-group">
											<input type="date" class="form-control" name="tanggal_lahir" placeholder="Tgl Lahir" required />
												<span class="input-group-addon">
												<i class="glyphicon glyphicon-calendar"></i>
											</span>
										</div>
									</div>
								</div>
                               <div class="form-inline clearfix">
									<div class="form-group col-sm-12">
										<textarea class="form-control" name="alamat_pasien" id="InputMessage" rows="4" placeholder="Alamat Anda" required></textarea>
									</div>
								</div>
                                <div class="clearfix">
									 <div class="col-sm-12">
										<button type="submit" class="btn btn-default">DAFTAR ANTRIAN</button>
									 </div>
                                 </div>
                            </form>
                        </div>
					</div>
				</div>
			</div>
		</div>
	</section>
	  
	<!--latest blog-->  
	<section id="blog" class="blog">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="heading">
						<h1>Latest Blog</h1>
						<img src="<?= base_url('/assets/frontend');?>/images/sep-1.png" alt="sep"/>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6">
    				<div class="news-wapper clearfix">
    					<div class="event-img">
							<img src="<?= base_url('/assets/frontend');?>/images/blog-1.jpg" class="pull-left" alt="event">
							<div class="date-block pull-left"><p><span class="dart-fs-18">08</span><br>JULY</p></div>
    					</div>
    					<div class="event-content clearfix">
    						<h4>Sed ut perspiciatis unde omnis </h4>
    						<p>Voluptatem accusantium doloremque totam rem aperiam, eaque ipsa quae ab illo invento veritatis et quasi architecto beatae. </p>
    						<a href="#"><i class="fa fa-user"></i>Nghi Pinky</a>
    					</div>
    				</div>
    			</div>
    			<div class="col-md-6 col-sm-6">
    				<div class="news-wapper clearfix">
    					<div class="event-img">
							<img src="<?= base_url('/assets/frontend');?>/images/blog-3.jpg" class="pull-left" alt="event">
							<div class="date-block pull-left"><p><span class="dart-fs-18">08</span><br>JULY</p></div>
    					</div>
    					<div class="event-content clearfix">
    						<h4>Sed ut perspiciatis unde omnis </h4>
    						<p>Voluptatem accusantium doloremque totam rem aperiam, eaque ipsa quae ab illo invento veritatis et quasi architecto beatae. </p>
    						<a href="#"><i class="fa fa-user"></i>Nghi Pinky</a>
    					</div>
    				</div>
    			</div>
    			<div class="col-md-6 col-sm-6">
    				<div class="news-wapper clearfix">
    					<div class="event-img">
							<img src="<?= base_url('/assets/frontend');?>/images/blog-2.jpg" class="pull-left" alt="event">
							<div class="date-block pull-left"><p><span class="dart-fs-18">08</span><br>JULY</p></div>
    					</div>
    					<div class="event-content clearfix">
    						<h4>Sed ut perspiciatis unde omnis </h4>
    						<p>Voluptatem accusantium doloremque totam rem aperiam, eaque ipsa quae ab illo invento veritatis et quasi architecto beatae. </p>
    						<a href="#"><i class="fa fa-user"></i>Nghi Pinky</a>
    					</div>
    				</div>
    			</div>
    			<div class="col-md-6 col-sm-6">
    				<div class="news-wapper clearfix">
    					<div class="event-img">
							<img src="<?= base_url('/assets/frontend');?>/images/blog-4.jpg" class="pull-left" alt="event">
							<div class="date-block pull-left"><p><span class="dart-fs-18">08</span><br>JULY</p></div>
    					</div>
    					<div class="event-content clearfix">
    						<h4>Sed ut perspiciatis unde omnis </h4>
    						<p>Voluptatem accusantium doloremque totam rem aperiam, eaque ipsa quae ab illo invento veritatis et quasi architecto beatae. </p>
    						<a href="#"><i class="fa fa-user"></i>Nghi Pinky</a>
    					</div>
    				</div>
    			</div>
			</div>
		</div>
	</section>

<?php 
	get_footer_frontend();
?>	  
