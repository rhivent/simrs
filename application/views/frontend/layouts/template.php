<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?= base_url('/assets/frontend');?>/images/favicon.ico">

    <title>Pregnancy | Home</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('/assets/frontend');?>/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
       
    <!-- Bootstrap Datepicker CSS -->
    <link href="<?= base_url('/assets/frontend');?>/vendor/bootstrap-datepicker/css/datepicker.min.css" rel="stylesheet">
           
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?= base_url('/assets/frontend');?>/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
    <!-- Custom Fonts -->
    <link href="<?= base_url('/assets/frontend');?>/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> 
    
    <!-- template CSS -->
    <link href="<?= base_url('/assets/frontend');?>/css/style.css" rel="stylesheet">
      
    <!-- Custom CSS -->
    <link href="<?= base_url('/assets/frontend');?>/css/custom.css" rel="stylesheet">

   	<!-- Base MasterSlider style sheet -->
    <link rel="stylesheet" href="<?= base_url('/assets/frontend');?>/vendor/masterslider/style/masterslider.css" />
     
    <!-- Master Slider Skin -->
    <link href="<?= base_url('/assets/frontend');?>/vendor/masterslider/skins/default/style.css" rel="stylesheet" type="text/css" />
    
    <!-- MasterSlider Template Style -->
	<link href="<?= base_url('/assets/frontend');?>/vendor/masterslider/style/ms-fullscreen.css" rel="stylesheet" type="text/css" />    
   
    <!-- owl Slider Style -->
    <link rel="stylesheet" href="<?= base_url('/assets/frontend');?>/vendor/owlcarousel/dist/assets/owl.carousel.min.css">
	<link rel="stylesheet" href="<?= base_url('/assets/frontend');?>/vendor/owlcarousel/dist/assets/owl.theme.default.min.css">
	
	<!-- flaticon -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('/assets/frontend');?>/vendor/flaticon/font/flaticon.css"> 
     
  </head>

  <body  id="home" >