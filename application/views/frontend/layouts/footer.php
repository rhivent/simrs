<!--footer-->  
	<footer id="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="footer-block">
						<img src="<?= base_url('/assets/img') ."/" .get_option('site_logo');?>" class="img-responsive" alt="logo" />
						<p><?= get_option('site_alamat');?><br>
							<?= get_option('site_telp');?><br>
							<?= get_option('site_email');?></p>
						<ul class="list-inline social-link">
							<li><a href="<?= get_option('site_linkfb');?>"><i class="fa fa-facebook"></i></a></li>
							<li><a href="<?= get_option('site_linktw');?>"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="<?= get_option('site_linkig');?>"><i class="fa fa-instagram"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row copy">
				<div class="col-md-6 col-sm-6">
					<p><span class="pink">© Riventus</span> 2019 | All Rights Reserved</p>
				</div>
				<div class="col-md-6 col-sm-6">
					<p class="text-right"><a href="javascript:void(0)">Terms of Use</a>    |    <a href="javascript:void(0)">Privacy Statement</a></p>
				</div>
			</div>
		</div>
	</footer>
	  
	  
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="<?= base_url('/assets/frontend');?>/js/ie10-viewport-bug-workaround.js"></script>
    
    <!-- jQuery -->
    <script src="<?= base_url('/assets/frontend');?>/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url('/assets/frontend');?>/vendor/bootstrap/js/bootstrap.min.js"></script>

	<!-- Bootstrap Datepicker- JavaScript -->
    <script src="<?= base_url('/assets/frontend');?>/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    	
    <!-- Nav JavaScript -->
    <script src="<?= base_url('/assets/frontend');?>/js/awesomenav.js"></script> 
    
	<!-- jQuery -->
    <!--<script src="vendor/masterslider/jquery.min.js"></script>-->
    <script src="<?= base_url('/assets/frontend');?>/vendor/masterslider/jquery.easing.min.js"></script>
     
    <!-- Master Slider -->
    <script src="<?= base_url('/assets/frontend');?>/vendor/masterslider/masterslider.min.js"></script>
    
    <!-- custom JavaScript -->
    <script src="<?= base_url('/assets/frontend');?>/js/custom.js"></script>     
	
	<!-- owl Slider JavaScript -->
    <script src="<?= base_url('/assets/frontend');?>/vendor/owlcarousel/dist/owl.carousel.min.js"></script> 
    
	<!-- tabstyle -->
	<script src="<?= base_url('/assets/frontend');?>/vendor/tabstyle/cbpFWTabs.js"></script>
	
    <!-- template JavaScript -->
    <script src="<?= base_url('/assets/frontend');?>/js/template.js"></script>
 
  </body>

</html>