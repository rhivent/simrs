<div class="leftmenu">
  <ul class="nav nav-tabs nav-stacked">    
    <li class="dropdown"><a href="#"><span class="icon-shopping-cart"></span>Transaksi</a>
      <ul>
        <li><a target="_blank" href="<?=base_url('apotik/penjualan/poli');?>">Penjualan Resep Poli</a></li>
        <li><a href="<?=base_url('apotik/penjualan');?>">Penjualan Apotek RS</a></li>  
        <li><a href="<?=base_url('apotik/returns');?>">Return</a></li>      
      </ul>
    </li>        
    <li class="dropdown"><a href="#"><span class="icon-file"></span> Laporan</a>
      <ul>
          <li><a href="<?=base_url('apotik/laporan/infostok');?>">Laporan Info Stok</a></li>
          <li><a href="<?=base_url('apotik/laporan/repjualperiode');?>">Laporan Penjualan</a></li>
          <li><a href="<?=base_url('apotik/laporan/repreturns');?>">Laporan Return</a></li> 
          <li><a href="<?=base_url('apotik/laporan/reprekap');?>">Laporan Rekap Faktur</a></li>
          <li><a href="<?=base_url('apotik/laporan/repMR');?>">Laporan per Pasien</a></li>
      </ul>
    </li>  
    <!--
    <li class="dropdown"><a href="#"><span class="icon-file"></span> Cetak Nota</a>
      <ul>
          <li><a href="<?=base_url('apotik/laporan/cetak_nota_apotik');?>">Pembelian Obat</a></li>
          <li><a href="<?=base_url('apotik/laporan/cetak_nota_inap');?>">Rawat Inap</a></li>
      </ul>
    </li>  
    --> 
  </ul>
</div>
<!--leftmenu-->

</div>