<?php
get_header();
?>
<h1>Edit Jenis Test</h1><br>

<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
    );
    foreach($update as $update){
echo form_open('laboratorium/jenis_lab/proses_updatejenis',$att);
?>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Kode Jenis Test</label>
        <div class="controls">
            <input type="hidden" id="inputEmail" name="id_jenis_lab" value="<?=$update->id_jenis_lab ?>"> 
            <input type="text" id="inputEmail" name="kode_jenis" value="<?=$update->kode_jenis ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Nama Jenis Test</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="nama_jenis" value="<?=$update->nama_jenis ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Nilai Standart Test</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="standart_test" value="<?=$update->standart_test ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Keterangan Test</label>
        <div class="controls">
        <textarea name="keterangan"><?=$update->keterangan;?></textarea>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-success">Proses</button>
        </div>
    </div>
</form>


<?php
    }
get_footer();
?>