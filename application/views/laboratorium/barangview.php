<?php
get_header();
?>
<h1>Manajemen Barang Laboratorium</h1>
<div class="alert alert-success">
  <h3>Selamat Datang, Silahkan tambah data barang laboratorium <a href="<?=base_url('laboratorium/barang/addbarang');?>" class="alert-link"> Di sini </a></h3>
</div>
<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>No</td>	
<td>Kode Barang</td>
<td>Nama Barang</td>
<td>Kategori Barang</td>
<td>Stok</td>
<td>Aksi</td>
</tr>
</thead>
<tbody>
<?php
	foreach($is_data['results'] as $row)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no?></td>
<td><?= $row->kode_barang_lab;?></td>
<td><?= $row->nama_barang_lab;?></td>
<td><?= $row->kategori_barang_lab;?></td>
<td><?= number_format($row->stok,5,",",".");?> <?= $row->satuan;?></td>
<td>
<a class="btn btn-small" href="<?=base_url();?>laboratorium/barang/updateview?uid=<?=$row->id_barang_lab;?>"><i class="icon-edit"></i> Edit</a>
<a class="btn btn-small" href="<?=base_url();?>laboratorium/barang/deletebarang?uid=<?=$row->id_barang_lab;?>"><i class="icon-trash"></i> Hapus</a>
</td>
</tr>
<?php }
?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error"></div>
<?php } ?>

<?= $is_data['links']; ?>
<?php
get_footer();
?>