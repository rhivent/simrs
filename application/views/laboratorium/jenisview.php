<?php
get_header();
?>
<h1>Manajemen Jenis Test Laboratorium</h1>
<div class="alert alert-success">
  <h3>Selamat Datang, Silahkan tambah data jenis test laboratorium <a href="<?=base_url('laboratorium/jenis_lab/addjenis');?>" class="alert-link"> Di sini </a></h3>
</div>
<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>No</td>	
<td>Kode Test</td>
<td>Nama Jenis Test</td>
<td>Standart Test</td>
<td>Keterangan</td>
<td>Aksi</td>
</tr>
</thead>
<tbody>
<?php
	foreach($is_data['results'] as $row)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no?></td>
<td><?= $row->kode_jenis;?></td>
<td><?= $row->nama_jenis;?></td>
<td><?= $row->standart_test;?></td>
<td><?= $row->keterangan?></td>
<td>
<a class="btn btn-small" href="<?=base_url();?>laboratorium/jenis_lab/updatejenis?uid=<?=$row->id_jenis_lab;?>"><i class="icon-edit"></i> Edit</a>
<a class="btn btn-small" href="<?=base_url();?>laboratorium/jenis_lab/deletejenis?uid=<?=$row->id_jenis_lab;?>"><i class="icon-trash"></i> Hapus</a>
</td>
</tr>
<?php }
?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error"></div>
<?php } ?>

<?= $is_data['links']; ?>
<?php
get_footer();
?>