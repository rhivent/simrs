<div class="leftmenu">
  <ul class="nav nav-tabs nav-stacked">    
    <li class="dropdown"><a href="#"><span class="icon-home"></span> Manajemen Laboratorium</a>
      <ul>
        <li><a href="<?=base_url('laboratorium/barang/databarang');?>">Data Barang</a></li>
        <li><a href="<?=base_url('laboratorium/barang/inputbarang');?>">Input Barang</a></li>
        <li><a href="<?=base_url('laboratorium/jenis_lab');?>">Jenis Test Laboratorium</a></li>
      </ul>
    </li>
    <li class="dropdown"><a href="#"><span class="icon-certificate"></span> Test Laboratorium</a>
      <ul>
        <li><a target="_blank" href="<?=base_url('laboratorium/test');?>">Test Lab Pasien</a></li>
      </ul>
    </li>  
    <li class="dropdown"><a href="#"><span class="icon-list"></span> Laporan Laboratorium</a>
      <ul>
        <li><a href="<?=base_url('laboratorium/laporan/intanggal');?>">Barang Masuk (Per Tanggal)</a></li>
        <li><a href="<?=base_url('laboratorium/laporan/inbarang');?>">Barang Masuk (Per Barang)</a></li>
        <li><a href="<?=base_url('laboratorium/laporan/outtanggal');?>">Penggunaan Barang (Per Tanggal)</a></li>
        <!--<li><a href="<?=base_url('laboratorium/laporan/outbarang');?>">Penggunaan Barang (Per Barang)</a></li>
        <li><a href="<?=base_url('laboratorium/laporan/reptest');?>">Laporan Test Laboratorium</a></li>--> 
      </ul>
    </li>         
  </ul>
</div>
<!--leftmenu-->

</div>