<?php
get_header();
?>
<h1>Riwayat Test Laboratorium</h1>
<div class="alert alert-success">
  <h3>Selamat Datang, Silahkan Proses Test Laboratorium <a href="<?=base_url('laboratorium/test/addtest');?>" class="alert-link"> Di sini </a></h3>
</div>
<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>No</td>	
<td>Nomor Rekam Medik</td>
<td>Nama Pasien</td>
<td>Nama Test</td>
<td>Tanggal Test</td>
<td>Standart Test</td>
<td>Hasil Test</td>
<td>Laboran</td>
<td>Aksi</td>
</tr>
</thead>
<tbody>
<?php
	foreach($is_data['results'] as $row)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no?></td>
<td><?= $row->id_pasien;?></td>
<td><?= $row->nama_pasien;?></td>
<td><?= $row->nama_jenis;?></td>
<td><?= $row->tanggal_test;?></td>
<td><?= $row->standart_test;?></td>
<td><?= $row->hasil_test;?></td>
<td><?= $row->laboran;?></td>
<td>
<a class="btn btn-small" target="_blank" href="<?=base_url();?>laboratorium/test/cetak?uid=<?=$row->id_test_lab;?>"><i class="icon-print"></i> Cetak</a>
<!--<a class="btn btn-small" href="<?=base_url();?>laboratorium/test/updateview?uid=<?=$row->id_test_lab;?>"><i class="icon-edit"></i> Edit</a>-->
<a class="btn btn-small" href="<?=base_url();?>laboratorium/test/delete?uid=<?=$row->id_test_lab;?>"><i class="icon-trash"></i> Hapus</a>
</td>
</tr>
<?php }
?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error"></div>
<?php } ?>

<?= $is_data['links']; ?>
<?php
get_footer();
?>