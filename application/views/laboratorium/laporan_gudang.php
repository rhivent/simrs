<?php get_header(); ?>
<?php if(!empty($is_data['results'])) : ?>
	<h1>Laporan Barang Masuk <?php echo $tanggal_awal ?> s.d. <?php echo $tanggal_akhir ?></h1>
	<div class="alert alert-success">
  		<!--<h3>Selamat Datang, Silahkan tambah data barang laboratorium <a href="<?=base_url('laboratorium/barang/addbarang');?>" class="alert-link"> Di sini </a></h3>-->
    	<a href="<?=base_url();?>laboratorium/laporan/cetakintanggal?tanggal_awal=<?=$tanggal_awal;?>&tanggal_akhir=<?=$tanggal_akhir?>" target="_blank"><i class="icon-print"></i>Cetak Laporan Barang Masuk Per Tanggal </a>
	</div>
	<table class="table table-hover">
		<thead>
			<tr>
				<td>No</td>	
				<td>Tanggal Input</td>
				<td>Kode Barang</td>
				<td>Nama Barang</td>
				<td>Kategori Barang</td>
				<td>Jumlah Barang</td>
				<td>Laboran</td>
			</tr>
		</thead>
		<tbody>
		<?php $no=0; ?>
		<?php foreach($is_data['results'] as $row) : ?>
		<?php $no=$no+1; ?>
			<tr>
				<td><?= $no?></td>
				<td><?= $row->tanggal_input;?></td>
				<td><?= $row->kode_barang_lab;?></td>
				<td><?= $row->nama_barang_lab;?></td>
				<td><?= $row->kategori_barang_lab;?></td>
				<td><?= $row->jumlah;?></td>
				<td><?= $row->laboran;?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php else : ?>
	<h1>Laporan Barang Masuk <?php echo $tanggal_awal ?> s.d. <?php echo $tanggal_akhir ?></h1>
	<div class="alert alert-error text-center">Tidak ada data untuk dimunculkan</div>
<?php endif; ?>
<?= $is_data['links']; ?>
<?php get_footer(); ?>