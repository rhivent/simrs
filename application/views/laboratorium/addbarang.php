<?php
get_header();
?>
<h1>Tambah Data Barang</h1><br>

<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('laboratorium/barang/simpanbarang',$att);
?>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Kode Barang</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="kode_barang_lab" placeholder="Masukkan Kode Barang">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Nama Barang</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="nama_barang_lab" placeholder="Masukkan Nama Barang">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Kode Barang</label>
        <div class="controls">
            <select name="kategori_barang_lab">
                <option value="0" disable="true">-- Pilih Kategori --</option>
                <option value="Peralatan">Peralatan</option>
                <option value="Obat">Obat</option>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Satuan Barang</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="satuan_barang_lab" placeholder="Masukkan Satuan Barang">
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-success">Simpan</button>
            <a href="<?php echo base_url() ?>laboratorium/barang/databarang" class="btn btn-inverse">Batal</a>
        </div>
    </div>
</form>
<?php
get_footer();
?>