<?php
get_header();
?>
<h1>Input Barang</h1><br>

<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('laboratorium/barang/prosesinputbarang',$att);
?>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Kode Barang</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="kode_barang" placeholder="Masukkan Kode Barang">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Jumlah Barang</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="jumlah_barang" placeholder="Masukkan Jumlah Barang">
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-success">Tambahkan</button>
        </div>
    </div>
</form>


<?php
get_footer();
?>