<?php
get_header();
?>
<h1>Detail Periksa Pasien</h1>
<table class="table table-hover">
    <thead>
        <tr>
            <td><strong>No Rekam Medik</strong></td>
            <td><strong>Nama Lengkap</strong></td>
            <td><strong>Alamat</strong></td>
            <td><strong>No Telepon</strong></td>
            <td><strong>Tempat Lahir</strong></td>
            <td><strong>Tanggal Lahir</strong></td>
        </tr>
    </thead>
    <tbody>
    <?php
        foreach($pasien as $pasien)
        {	
    ?>
        <tr>
            <td><?php echo $pasien->nomor;?></td>
            <td><?php echo $pasien->nama_pasien;?></td>
            <td><?php echo $pasien->alamat_pasien;?></td>
            <td><?php echo $pasien->no_telp;?></td>
            <td><?php echo $pasien->tempat_lahir;?></td>
            <td><?php echo $pasien->tanggal_lahir;?></td>
        </tr>
    <?php   
        }
    ?>
    </tbody>
</table>
<div class="alert alert-success"></div>
<table class="table table-hover">
        <thead>
            <tr>
                <td>
                    <div class="alert alert-success">
                        Proses Test Laboratorium
                    </div>
                </td>
                <td>
                    <div class="alert alert-success">
                        Riwayat Periksa Laboratorium
                    </div>
                </td>
            <tr>
        </thead>
        <tbody>
            <tr>
                <td>
                <?php
                $att=array(
                    'class'=>'form-horizontal',
                    'role'=>'form',
                    );
                echo form_open('laboratorium/test/cart',$att);
                ?>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Masukkan Kode Barang</label>
                        <div class="controls">
                            <input type="hidden" id="inputEmail" name="id_pasien" value="<?php echo $id_pasien ?>">
                            <input type="text" id="inputEmail" name="id_barang_lab" placeholder="Barang/Alat Test">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputEmail">Jumlah Barang</label>
                        <div class="controls">
                            <input type="text" id="inputEmail" name="jumlah_barang" placeholder="Jumlah Penggunaan">
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <button type="submit" class="btn btn-success">Tambahkan</button>
                        </div>
                    </div>
                </form>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td><strong>Kode Barang</strong></td>
                                <td><strong>Nama Barang</strong></td>
                                <td><strong>Jumlah</strong></td>
                            </tr>
                        <thead>
                        <tbody>
                            <?php
                                foreach($cart as $cart){
                            ?>
                                <tr>
                                    <td><?php echo $cart->kode_barang_lab ?></td>
                                    <td><?php echo $cart->nama_barang_lab?></td>
                                    <td><?php echo $cart->jumlah?></td>
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                    </br>
                    <hr>
                    <?php
                        $att=array(
                            'class'=>'form-horizontal',
                            'role'=>'form',
                            );
                        echo form_open('laboratorium/test/simpantest',$att);
                        ?>
                            <div class="control-group">
                                <label class="control-label" for="inputEmail">Jenis Test</label>
                                <div class="controls">
                                    <select name="jenis_test">
                                        <option value="0">-- Pilih Jenis Test --</option>
                                        <?php
                                            foreach($jenis_lab as $jenis_lab){
                                        ?>
                                            <option value="<?php echo $jenis_lab->id_jenis_lab ?>"><?php echo $jenis_lab->kode_jenis ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputEmail">Masukkan Hasil Test</label>
                                <div class="controls">
                                    <input type="text" id="inputEmail" name="hasil_test" placeholder="Hasil Test">
                                    <input type="hidden" id="inputEmail" name="id_pasien" value="<?php echo $id_pasien ?>">
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="controls">
                                    <button type="submit" class="btn btn-success">Simpan</button>
                                </div>
                            </div>
                        </form>
                </td>
                <td>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td><strong>Tanggal Periksa</strong></td>
                                <td><strong>Jenis Pemeriksaan</strong></td>
                                <td><strong>Hasil</strong></td>
                                <td><strong>Laboran</strong></td>
                            </tr>
                        <thead>
                        <tbody>
                            <?php
                                foreach($laboratorium as $laboratorium)
                                {
                            ?>
                                <tr>
                                    <td><?php echo $laboratorium->tanggal_test ?></td>
                                    <td><?php echo $laboratorium->kode_jenis?></td>
                                    <td><?php echo $laboratorium->hasil_test?></td>
                                    <td><?php echo $laboratorium->laboran?></td>
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
</table>
<hr>

<?php
get_footer();
?>