<?php
get_header();
?>
<h1>Konfigurasi Slider 
</h1>
<div class="alert alert-success">
  <h3>Selamat Datang, Silahkan tambah slider
    <a href="<?=base_url('pendaftaran/addregpoli');?>" class="alert-link"> Di sini 
    </a>
  </h3>
</div>
<?php
$no=0;
// echo "<pre>";
// var_dump($is_data);
// echo "</pre>";
// die;

if(!empty($is_data))
{
?>
<div style="margin:0px 0px 0px 20px;">
	<table id="example_poliklinik" class="table table-hover table-responsive" style="margin: 0px 0px 0px 0px;">
	  <thead>
	    <tr>
	      <td>No</td>	
	      <td>Title</td>
	      <td>Text</td>
	      <td>Foto</td>
	      <td>Aksi</td>
	    </tr>
	  </thead>
	  <tbody>
	    <?php
	foreach($is_data as $row)
	{
	$no=$no+1;	
	?>
	    <tr>
	      <td>
	        <?= $no?>
	      </td>
	      <td>
	        <?= $row->title_slider;?>
	      </td>
	      <td>
	        <?= $row->text_slider;?>
	      </td>
	      <td>
	        <img src="<?= base_url('/assets/slider')."/".$row->img_slider;?>" width="80px">
	      </td>
	      <td>
	        <a class="btn btn-small" onclick="window.location='<?=base_url();?>pendaftaran/updateview?uid=<?=$row->id_slider;?>'" href="javascript:void(0)"><i class="icon-edit"></i> Edit</a>
	        <a class="btn btn-small" href="<?=base_url();?>pendaftaran/poliklinikview/detail?uid=<?=$row->id_slider;?>" target="_blank">
	          <i class="icon-search">
	          </i> Detail
	        </a>
	        <a class="btn btn-small" href="<?=base_url();?>pendaftaran/poliklinikview/delete?uid=<?=$row->id_slider;?>">
	          <i class="icon-trash">
	          </i> Hapus
	        </a>
	      </td>
	    </tr>
	    <?php }
	?>
	  </tbody>
	</table>
</div>
<?php }else{ ?>
<div class="alert alert-error">
</div>
<?php } ?>
 <?php echo ''; //$is_data['links']; ?>
<?php
get_footer();
?>
