<div class="leftmenu">
  <ul class="nav nav-tabs nav-stacked">    
    <li class="dropdown"><a href="#"><span class="icon-user"></span>User Accounts</a>
      <ul>
        <li><a href="<?=base_url('admin/adduser');?>">Tambah User</a></li>        
        <li><a href="<?=base_url('admin/userview');?>">Manajemen User</a></li>
        <li><a href="<?=base_url('admin/kategoripasien');?>">Kategori Pasien</a></li>
      </ul>
    </li>
    <li class="dropdown"><a href="#"><span class="icon-edit"></span>Master Data</a>
      <ul>        
        <li><a href="<?=base_url('inap/master/ruangan');?>">Master Kamar</a></li>
        <li><a href="<?=base_url('inap/master/tindakan');?>">Master Tindakan</a></li>
        <!--<li><a href="<?=base_url('laboratorium/jenis_lab');?>">Master Tes Laboratorium</a></li>-->
      </ul>
    </li>     
    <li class="dropdown"><a href="#"><span class="icon-list"></span>Laporan</a>
        <ul>
          <li><a href="<?=base_url('pendaftaran/pasienview');?>">Laporan Pasien</a></li>
          <li><a href="<?=base_url('pendaftaran/poliklinikview');?>">Laporan Rawat Jalan</a></li>
          <li><a href="<?=base_url('inap/pasien');?>">Laporan Rawat Inap</a></li>
          <li><a href="<?=base_url('laboratorium/test');?>">Laporan Laboratorium</a></li>
          <li><a href="<?=base_url('apotik/laporan/infostok');?>">Laporan Obat</a></li>
          <li><a href="<?=base_url('apotik/laporan/repjualperiode');?>">Laporan Penjualan Obat</a></li>
        </ul>
    </li>   
    <li class="dropdown"><a href="#"><span class="icon-wrench"></span> System</a>
      <ul>
        <li><a href="<?=base_url('admin/config');?>">Konfigurasi</a></li>
        <li><a href="<?=base_url('admin/slider');?>">Slider</a></li>
        <li><a href="<?=base_url('admin/dbtool');?>">Database Tools</a></li>       
      </ul>
    </li>   
    </ul>
</div>
<!--leftmenu-->

</div>