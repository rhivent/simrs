<?php
get_header();
?>
<h1>Konfigurasi Web</h1>
<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open_multipart(base_url('admin/config/'),$att);
?>
	<div class="control-group">
	    <label class="control-label">Nama Perusahaan</label>
	    <div class="controls">
	        <input type="text" id="inputEmail" name="site_name" value="<?=get_option('site_name');?>" placeholder="Nama Perusahaan" data-validation="length" data-validation-length="min3">
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label">Nama Aplikasi</label>
	    <div class="controls">
	        <input type="text" name="site_app" value="<?=get_option('site_app');?>" placeholder="Nama Aplikasi">
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">Icon Aplikasi</label>
	    <div class="controls">
	        <input type="file" name="site_icon" value="<?=get_option('site_icon');?>" placeholder="Icon Aplikasi">
	    <img src="<?= base_url(). 'assets/img/' . get_option('site_icon');?>" width="100px" alt="icon tidak ada" />
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">Logo Aplikasi</label>
	    <div class="controls">
	        <input type="file" name="site_logo" value="<?=get_option('site_logo');?>" placeholder="Logo Aplikasi">
	    <img src="<?= base_url(). 'assets/img/' . get_option('site_logo');?>" width="100px" alt="logo tidak ada" />
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">Alamat</label>
	    <div class="controls">
	        <input type="text" name="site_alamat" value="<?=get_option('site_alamat');?>" placeholder="Alamat">
	    </div>
	</div>
	<div class="control-group">
	    <label class="control-label">Email</label>
	    <div class="controls">
	        <input type="email" name="site_email" value="<?=get_option('site_email');?>" placeholder="Email">
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">Telp</label>
	    <div class="controls">
	        <input type="text" name="site_telp" value="<?=get_option('site_telp');?>" placeholder="Nomor Telephone">
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">Facebook Link</label>
	    <div class="controls">
	        <input type="text" name="site_linkfb" value="<?=get_option('site_linkfb');?>" placeholder="Link Facebook">
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">Twitter Link</label>
	    <div class="controls">
	        <input type="text" name="site_linktw" value="<?=get_option('site_linktw');?>" placeholder="Link Twitter">
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">Instagram Link</label>
	    <div class="controls">
	        <input type="text" name="site_linkig" value="<?=get_option('site_linkig');?>" placeholder="Link Instagram">
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">Line Link</label>
	    <div class="controls">
	        <input type="text" name="site_linkline" value="<?=get_option('site_linkline');?>" placeholder="Link Line">
	    </div>
	</div>

	<div class="control-group">
	    <label class="control-label">Link</label>
	    <div class="controls">
	        <input type="text" name="site_linklain" value="<?=get_option('site_linklain');?>" placeholder="Link Lain">
	    </div>
	</div>

	<div class="control-group">
	    <div class="controls">
	        <button type="submit" class="btn btn-success">Simpan</button>
	        <button type="submit" class="btn btn-inverse" onclick="return confirm('Yakin batalkan data ini?');">Batal</button>
	    </div>
	</div>
</form>
<?php
get_footer();
?>