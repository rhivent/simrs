<?php
get_header();
?>
<h1>Hasil Pencarian Data Pasien</h1>
<table class="table table-hover">
    <thead>
        <tr>
            <td><strong>No Rekam Medik</strong></td>
            <td><strong>Nama Lengkap</strong></td>
            <td><strong>Alamat</strong></td>
            <td><strong>No Telepon</strong></td>
            <td><strong>Tempat Lahir</strong></td>
            <td><strong>Tanggal Lahir</strong></td>
            <td><strong>Aksi</strong></td>
        </tr>
    </thead>
    <tbody>
    <?php
        foreach($detail as $detail)
        {	
    ?>
        <tr>
            <td><?php echo $detail->nomor;?></td>
            <td><?php echo $detail->nama_pasien;?></td>
            <td><?php echo $detail->alamat_pasien;?></td>
            <td><?php echo $detail->no_telp;?></td>
            <td><?php echo $detail->tempat_lahir;?></td>
            <td><?php echo $detail->tanggal_lahir;?></td>
            <td>
                <a class="btn btn-small" onclick="window.location='<?=base_url();?>pendaftaran/pasienview/updateview?uid=<?=$detail->id;?>'" href="javascript:void(0)"><i class="icon-edit"></i> Edit</a>
                <a class="btn btn-small" href="<?=base_url();?>pendaftaran/pasienview/cetak?uid=<?=$detail->id;?>" target="_blank"><i class="icon-barcode"></i> Cetak</a>
                <a class="btn btn-small" onclick="window.location='<?=base_url();?>pendaftaran/pasienview/delete?uid=<?=$detail->id;?>'" href="javascript:void(0)"><i class="icon-trash"></i> Hapus</a>
            </td>
        </tr>
    <?php   
        }
    ?>
    </tbody>
</table>

<hr>

<?php
get_footer();
?>