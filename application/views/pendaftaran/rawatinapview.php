<?php
get_header();
?>
<h1>Manajemen Rawat Inap</h1>
<div class="alert alert-success">
  <h3>Selamat Datang, Silahkan tambah pendaftaran rawat inap <a href="<?=base_url('pendaftaran/rawatinapview/addreginap');?>" class="alert-link"> Di sini </a></h3>
</div>
<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>No</td>	
<td>No Rekam Medik</td>
<td>Nama Lengkap</td>
<td>Tanggal Masuk</td>
<td>Ruang</td>
<td>Paket</td>
<td>Rujukan</td>
<td>Penanggung Jawab</td>
<td>Hub Dengan Pasien</td>
<td>Aksi</td>
</tr>
</thead>
<tbody>
<?php
	foreach($is_data['results'] as $row)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no?></td>
<td><?= $row->nomor;?></td>
<td><?= $row->nama_pasien;?></td>
<td><?= $row->tanggal_masuk;?></td>
<td><?= $row->nama_ruangan;?></td>
<td><?= $row->nama_paket;?></td>
<td><?= $row->nama_perujuk;?></td>
<td><?= $row->penanggung_jawab;?></td>
<td><?= $row->hub_dengan_pasien;?></td>
<td>
<!--<a class="btn btn-small" onclick="window.location='<?=base_url();?>pendaftaran/updateview?uid=<?=$row->id;?>'" href="javascript:void(0)"><i class="icon-edit"></i> Edit</a>-->
<a class="btn btn-small" href="<?=base_url();?>pendaftaran/rawatinapview/detail?uid=<?=$row->nomor;?>" target="_blank"><i class="icon-search"></i> Detail</a>
<!--<a class="btn btn-small" href="<?=base_url();?>pendaftaran/rawatinapview/delete?uid=<?=$row->id_reg_inap;?>"><i class="icon-trash"></i> Hapus</a>-->
</td>
</tr>
<?php } ?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error"></div>
<?php } ?>

<?= $is_data['links']; ?>
<?php
get_footer();
?>