<?php
get_header();
?>
<h1>Detail Periksa Pasien</h1>
<table class="table table-hover">
    <thead>
        <tr>
            <td><strong>No Rekam Medik</strong></td>
            <td><strong>Nama Lengkap</strong></td>
            <td><strong>Alamat</strong></td>
            <td><strong>No Telepon</strong></td>
            <td><strong>Tempat Lahir</strong></td>
            <td><strong>Tanggal Lahir</strong></td>
        </tr>
    </thead>
    <tbody>
    <?php
        foreach($detail as $detail)
        {	
    ?>
        <tr>
            <td><?php echo $detail->nomor;?></td>
            <td><?php echo $detail->nama_pasien;?></td>
            <td><?php echo $detail->alamat_pasien;?></td>
            <td><?php echo $detail->no_telp;?></td>
            <td><?php echo $detail->tempat_lahir;?></td>
            <td><?php echo $detail->tanggal_lahir;?></td>
        </tr>
    <?php   
        }
    ?>
    </tbody>
</table>
<div class="alert alert-success"></div>
<table class="table table-hover">
                        <thead>
                            <tr>
                                <td><strong>Tanggal Masuk</strong></td>
                                <td><strong>Ruangan</strong></td>
                                <td><strong>Paket Rawat Inap</strong></td>
                                <td><strong>Penanggung Jawab</strong></td>
                                <td><strong>Hubungan Dengan Pasien</strong></td>
                            </tr>
                        <thead>
                        <tbody>
                            <?php
                                foreach($inap as $inap)
                                {
                            ?>
                                <tr>
                                    <td><?php echo $inap->tanggal_masuk ?></td>
                                    <td><?php echo $inap->nama_ruangan ?></td>
                                    <td><?php echo $inap->nama_paket ?></td>
                                    <td><?php echo $inap->penanggung_jawab ?></td>
                                    <td><?php echo $inap->hub_dengan_pasien ?></td>
                                    
                                </tr>
                            <?php
                                }
                            ?>
                        </tbody>
                    </table>
<hr>

<?php
get_footer();
?>