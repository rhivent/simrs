<?php
get_header();
?>
<h1>Manajemen Pasien</h1>
<div class="alert alert-success">
  <h3>Selamat Datang, Silahkan tambah data pasien <a href="<?=base_url('pendaftaran/addpasien');?>" class="alert-link"> Di sini </a></h3>
</div>
<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>No</td>	
<td>No Rekam Medik</td>
<td>Nama Lengkap</td>
<td>Alamat</td>
<td>No Telepon</td>
<td>Tempat Lahir</td>
<td>Tanggal Lahir</td>
<td>Aksi</td>
</tr>
</thead>
<tbody>
<?php
	foreach($is_data['results'] as $row)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no?></td>
<td><?= $row->nomor;?></td>
<td><?= $row->nama_pasien;?></td>
<td><?= $row->alamat_pasien;?></td>
<td><?= $row->no_telp;?></td>
<td><?= $row->tempat_lahir;?></td>
<td><?= $row->tanggal_lahir;?></td>
<td>
<a class="btn btn-small" onclick="window.location='<?=base_url();?>pendaftaran/pasienview/updateview?uid=<?=$row->id;?>'" href="javascript:void(0)"><i class="icon-edit"></i> Edit</a>
<a class="btn btn-small" href="<?=base_url();?>pendaftaran/pasienview/cetak?uid=<?=$row->id;?>" target="_blank"><i class="icon-barcode"></i> Cetak</a>
<a class="btn btn-small" onclick="window.location='<?=base_url();?>pendaftaran/pasienview/delete?uid=<?=$row->id;?>'" href="javascript:void(0)"><i class="icon-trash"></i> Hapus</a>
</td>
</tr>
<?php }
?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error"></div>
<?php } ?>

<?= $is_data['links']; ?>
<?php
get_footer();
?>