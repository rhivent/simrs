<?php
get_header();
?>
<h1>Registrasi Poliklinik</h1>
<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('',$att);
?>
<div class="control-group">
<label class="control-label" for="inputEmail">No Rekam Medik</label>
<div class="controls">
<input type="text" id="inputEmail" name="nomor" placeholder="Nomor Rekam Medik">
</div>
</div>
<div class="control-group">
<div class="controls">
<button type="submit" class="btn btn-success">CARI</button>
<!--<button type="submit" class="btn btn-inverse" onclick="return confirm('Yakin batalkan data ini?');">Batal</button>-->
<a href="<?php echo site_url() ?>pendaftaran/poliklinikview" class="btn btn-default">BATAL</a>
</div>
</div>
</form>
<?php
get_footer();
?>