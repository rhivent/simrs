<div class="leftmenu">
  <ul class="nav nav-tabs nav-stacked">    
  <li class="dropdown"><a href="#"><span class="icon-user"></span> Manajemen Pasien</a>
      <ul>
        <li><a href="<?=base_url('pendaftaran/pasienview');?>">Manajemen Pasien</a></li> 
      </ul>
    </li>
    <li class="dropdown"><a href="#"><span class="icon-file"></span> Pendaftaran</a>
      <ul>
        <li><a href="<?=base_url('pendaftaran/poliklinikview');?>">Poliklinik</a></li> 
        <li><a href="<?=base_url('pendaftaran/rawatinapview');?>">Rawat Inap</a></li> 
      
      </ul>
    </li>
    <li class="dropdown"><a href="#"><span class="icon-search"></span> Pencarian</a>
      <ul>
        <li><a href="<?=base_url('pendaftaran/caripasien/datapasien');?>">Cari Data Pasien</a></li> 
        <li><a href="<?=base_url('pendaftaran/caripasien/datapoli');?>">Cari Pasien Poliklinik</a></li>
        <li><a href="<?=base_url('pendaftaran/caripasien/datainap');?>">Cari Pasien Rawat Inap</a></li> 
      </ul>
    </li>
    <li class="dropdown"><a href="#"><span class="icon-list"></span> Laporan</a>
      <ul>
        <li><a href="<?=base_url('pendaftaran/laporan/pasien');?>">Laporan Pasien Poliklinik</a></li> 
      </ul>
    </li>    

    <li class="dropdown"><a href="#"><span class="icon-file"></span> Informasi</a>
      <ul>
        <li><a href="<?=base_url('inap/master/inforuangan');?>">Informasi Harga Kamar</a></li>
      </ul>
    </li>   
    </ul>
</div>
<!--leftmenu-->

</div>