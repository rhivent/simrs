<?php
get_header();
?>
<?php
	foreach($pasien as $pasien){
?>
<h1>Edit Pasien</h1>

<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('pendaftaran/pasienview/update?uid='.$pasien->id.'',$att);
?>

<div class="control-group">
<label class="control-label" for="inputEmail">No Rekam Medik</label>
<div class="controls">
<input type="hidden" id="inputEmail" name="id" value="<?php echo $pasien->id; ?>">
<input type="text" id="inputEmail" name="nomor" placeholder="Nomor Rekam Medik" value="<?php echo $pasien->nomor; ?>">
</div>
</div>
<div class="control-group">
<label class="control-label" for="inputEmail">Nama Pasien</label>
<div class="controls">
<input type="text" id="inputEmail" name="nama_pasien" placeholder="Nama Lengkap Pasien" value="<?php echo $pasien->nama_pasien; ?>">
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Alamat Pasien</label>
<div class="controls">
<input type="text" id="inputEmail" name="alamat_pasien" placeholder="Alamat Pasien" value="<?php echo $pasien->alamat_pasien; ?>">
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Tempat/Tanggal Lahir</label>
<div class="controls">
<input type="text" id="inputEmail" name="tempat_lahir" placeholder="Tempat Lahir Pasien" value="<?php echo $pasien->tempat_lahir; ?>">
<input type="date" id="inputEmail" name="tanggal_lahir" value="<?php echo $pasien->tanggal_lahir; ?>">
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">No Telepon / HP</label>
<div class="controls">
<input type="text" id="inputEmail" name="no_telp" placeholder="No Telepon / HP" value="<?php echo $pasien->no_telp; ?>">
</div>
</div>

<div class="control-group">
<div class="controls">
<button type="submit" class="btn btn-success">Simpan</button>
<!--<button type="submit" class="btn btn-inverse" onclick="return confirm('Yakin batalkan data ini?');">Batal</button>-->
<a href="<?php echo site_url() ?>pendaftaran/pasienview" class="btn btn-default">Batal</a>
</div>
</div>

</form>
<?php
	}
?>
<?php
get_footer();
?>