<?php
get_header();
?>
<h1>Tambah Pasien</h1>
<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('',$att);
?>
<div class="control-group">
<label class="control-label" for="inputEmail">No Rekam Medik</label>
<div class="controls">
	<input type="text" id="inputEmail" readonly name="nomor" value="<?= $nomor;?>" placeholder="Masukkan No Rekam Medik">

</div>
</div>
<div class="control-group">
<label class="control-label" for="inputEmail">Nama Pasien</label>
<div class="controls">
<input type="text" id="inputEmail" name="nama_pasien" placeholder="Nama Lengkap Pasien">
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Alamat Pasien</label>
<div class="controls">
<input type="text" id="inputEmail" name="alamat_pasien" placeholder="Alamat Pasien">
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Tempat/Tanggal Lahir</label>
<div class="controls">
<input type="text" id="inputEmail" name="tempat_lahir" placeholder="Tempat Lahir Pasien">
<input type="date" id="inputEmail" name="tanggal_lahir">
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">No Telepon / HP</label>
<div class="controls">
<input type="text" id="inputEmail" name="no_telp" placeholder="No Telepon / HP">
</div>
</div>

<div class="control-group">
<div class="controls">
<button type="submit" class="btn btn-success">Simpan</button>
<!--<button type="submit" class="btn btn-inverse" onclick="return confirm('Yakin batalkan data ini?');">Batal</button>-->
<a href="<?php echo site_url() ?>pendaftaran/pasienview" class="btn btn-default">Batal</a>
</div>
</div>
</form>
<?php
get_footer();
?>