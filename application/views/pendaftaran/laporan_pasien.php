<?php
get_header();
?>
<h1>Laporan Pasien Poliklinik</h1>

<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>No</td>	
<td>No Rekam Medik</td>
<td>Nama Lengkap</td>
<td>Tanggal Periksa</td>
<td>Poliklinik</td>
<td>No Registrasi</td>
<td>Biaya</td>
</tr>
</thead>
<tbody>
<?php
	foreach($is_data['results'] as $row)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no?></td>
<td><?= $row->nomor;?></td>
<td><?= $row->nama_pasien;?></td>
<td><?= $row->tanggal_periksa;?></td>
<td><?= $row->nama_poliklinik;?></td>
<td><?= $row->no_registrasi;?></td>
<td><?= $row->biaya;?></td>
</tr>

<?php }

?>
<tr>
        <td colspan='6'>Total Pendapatan</td>
        <td>
            <?php
            foreach($total['results'] as $row2)
            {
                echo $row2->total;
            }
            ?>
        </td>
</tr>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error"></div>
<?php } ?>

<?= $is_data['links']; ?>
<?php
get_footer();
?>