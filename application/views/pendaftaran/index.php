<?php
get_header();
?>
<div class="row-fluid">
<ul class="thumbnails">


<li class="span4">
<div class="thumbnail">                 
  <div class="caption">
    <h3>Pasien</h3>
    <p>Manajemen Pasien RSIA Puriagung</p>
    <p><a href="<?=base_url('pendaftaran/pasienview');?>" class="btn btn-primary">Masuk</a> </p>
  </div>
</div>
</li>

<li class="span4">
<div class="thumbnail">                 
  <div class="caption">
    <h3>Pasien Rawat Jalan</h3>
    <p>Manajemen Pasien Rawat Jalan RSIA Puriagung</p>
    <p><a href="<?=base_url('pendaftaran/poliklinikview');?>" class="btn btn-primary">Masuk</a> </p>
  </div>
</div>
</li>

<li class="span4">
<div class="thumbnail">                 
  <div class="caption">
    <h3>Pasien Rawat Inap</h3>
    <p>Manajemen Pasien Rawat Inap RSIA Puriagung</p>
    <p><a href="<?=base_url('pendaftaran/rawatinapview');?>" class="btn btn-primary">Masuk</a> </p>
  </div>
</div>
</li>

</ul>
</div>
<?php
get_footer();
?>