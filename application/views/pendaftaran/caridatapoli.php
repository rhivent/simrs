<?php
get_header();
?>
<h1>Pencarian Data Pasien Rawat Jalan</h1>
<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('',$att);
?>
<div class="control-group">
<label class="control-label" for="inputEmail">Pencarian</label>
<div class="controls">
<input type="text" id="inputEmail" name="cari" placeholder="Nomor Rekam Medik/Nama Pasien">
</div>
</div>
<div class="control-group">
<div class="controls">
<button type="submit" class="btn btn-success">CARI</button>
</div>
</div>
</form>
<?php
get_footer();
?>