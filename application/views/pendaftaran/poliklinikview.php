<?php
get_header();
?>
<h1>Manajemen Poliklinik
</h1>
<div class="alert alert-success">
  <h3>Selamat Datang, Silahkan tambah pendaftaran poliklinik 
    <a href="<?=base_url('pendaftaran/addregpoli');?>" class="alert-link"> Di sini 
    </a>
  </h3>
</div>
<?php
$no=0;
if(!empty($is_data))
{
?>
<div style="margin:0px 0px 0px 20px;">
	<table id="example_poliklinik" class="table table-hover table-responsive" style="margin: 0px 0px 0px 0px;">
	  <thead>
	    <tr>
	      <td>No
	      </td>	
	      <td>No Rekam Medik
	      </td>
	      <td>Nama Lengkap
	      </td>
	      <td>Tanggal Periksa
	      </td>
	      <td>Poliklinik
	      </td>
	      <td>No Registrasi
	      </td>
	      <td>Aksi
	      </td>
	    </tr>
	  </thead>
	  <tbody>
	    <?php
	foreach($is_data['results'] as $row)
	{
	$no=$no+1;	
	?>
	    <tr>
	      <td>
	        <?= $no?>
	      </td>
	      <td>
	        <?= $row->nomor;?>
	      </td>
	      <td>
	        <?= $row->nama_pasien;?>
	      </td>
	      <td>
	        <?= $row->tanggal_periksa;?>
	      </td>
	      <td>
	        <?= $row->nama_poliklinik;?>
	      </td>
	      <td>
	        <?= $row->no_registrasi;?>
	      </td>
	      <td>
	        <!--<a class="btn btn-small" onclick="window.location='<?=base_url();?>pendaftaran/updateview?uid=<?=$row->id;?>'" href="javascript:void(0)"><i class="icon-edit"></i> Edit</a>-->
	        <a class="btn btn-small" href="<?=base_url();?>pendaftaran/poliklinikview/detail?uid=<?=$row->nomor;?>" target="_blank">
	          <i class="icon-search">
	          </i> Detail
	        </a>
	        <a class="btn btn-small" href="<?=base_url();?>pendaftaran/poliklinikview/delete?uid=<?=$row->id_reg_poli;?>">
	          <i class="icon-trash">
	          </i> Hapus
	        </a>
	      </td>
	    </tr>
	    <?php }
	?>
	  </tbody>
	</table>
</div>
<?php }else{ ?>
<div class="alert alert-error">
</div>
<?php } ?>
 <?php echo ''; //$is_data['links']; ?>
<?php
get_footer();
?>
