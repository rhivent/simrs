<?php
get_header();
?>
<h1>Tambah Registrasi Poliklinik</h1>
<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('pendaftaran/addregpoli/simpan',$att);
?>
<div class="control-group">
<label class="control-label" for="inputEmail">No Rekam Medik</label>
<div class="controls">
<?php
    foreach($detail as $detail){
?>
<input type="text" id="inputEmail" name="nomor" value="<?php echo $detail->nomor ?>">
<?php
    }
?>
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">No Registrasi</label>
<div class="controls">
<input type="text" id="inputEmail" name="no_registrasi" placeholder="No Registrasi" autofocus="true">
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Poliklinik</label>
<div class="controls">
<select name="id_poli">
	<?php
		foreach($poli as $poli){
	?>
		<option value="<?php echo $poli->id_poliklinik ?>"><?php echo $poli->nama_poliklinik ?></option>
	<?php
		}
	?>
</select>
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Berat Badan</label>
<div class="controls">
<input type="text" id="inputEmail" name="bb" placeholder="Berat Badan Pasien">
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Tensi Darah</label>
<div class="controls">
<input type="text" id="inputEmail" name="tensi" placeholder="Tensi Darah Pasien">
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Keluhan</label>
<div class="controls">
<input type="text" id="inputEmail" name="keluhan" placeholder="Keluhan Pasien">
</div>
</div>

<div class="control-group">
<div class="controls">
<button type="submit" class="btn btn-success">Simpan</button>
<!--<button type="submit" class="btn btn-inverse" onclick="return confirm('Yakin batalkan data ini?');">Batal</button>-->
<a href="<?php echo site_url() ?>pendaftaran/poliklinikview" class="btn btn-default">Batal</a>
</div>
</div>
</form>
<?php
get_footer();
?>