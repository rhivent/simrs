<?php
get_header();
?>
<h1>Tambah Registrasi Rawat Inap</h1>
<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('pendaftaran/rawatinapview/simpan',$att);
?>
<div class="control-group">
<label class="control-label" for="inputEmail">No Rekam Medik</label>
<div class="controls">
<input type="text" id="inputEmail" name="nomor" placeholder="Masukkan No Rekam Medik">
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Tanggal Masuk</label>
<div class="controls">
<input type="date" id="inputEmail" name="tanggal_masuk" placeholder="Tanggal Masuk">
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Ruangan</label>
<div class="controls">
<select name="id_ruang">
    <option value="0" disable="true">-- Nama Ruangan --</option>
	<?php
		foreach($ruangan as $ruangan){
	?>
		<option value="<?php echo $ruangan->id_ruangan ?>"><?php echo $ruangan->nama_ruangan ?></option>
	<?php
		}
	?>
</select>
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Paket Rawat Inap</label>
<div class="controls">
<select name="id_paket">
    <option value="0" disable="true">-- Nama Paket --</option>
	<?php
		foreach($paket as $paket){
	?>
		<option value="<?php echo $paket->id_paket ?>"><?php echo $paket->nama_paket ?></option>
	<?php
		}
	?>
</select>
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Rujukan</label>
<div class="controls">
<select name="rujukan">
    <option value="0" disable="true">-- Nama Rujukan --</option>
	<?php
		foreach($rujukan as $rujukan){
	?>
		<option value="<?php echo $rujukan->id_rujukan ?>"><?php echo $rujukan->nama_perujuk ?></option>
	<?php
		}
	?>
</select>
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Penanggung Jawab</label>
<div class="controls">
<input type="text" id="inputEmail" name="penanggung_jawab" placeholder="Nama Penanggung Jawab">
</div>
</div>

<div class="control-group">
<label class="control-label" for="inputEmail">Hubungan Dengan Pasien</label>
<div class="controls">
<input type="text" id="inputEmail" name="hub_dengan_pasien" placeholder="Hubungan Dengan Pasien">
</div>
</div>

<div class="control-group">
<div class="controls">
<button type="submit" class="btn btn-success">Simpan</button>
<!--<button type="submit" class="btn btn-inverse" onclick="return confirm('Yakin batalkan data ini?');">Batal</button>-->
<a href="<?php echo site_url() ?>pendaftaran/rawatinapview" class="btn btn-default">Batal</a>
</div>
</div>
</form>
<?php
get_footer();
?>