<?php
get_header();
?>
<h1>Info Ruangan Rawat Inap</h1>

<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>No</td>	
<td>Nama Ruangan</td>
<td>Kelas</td>
<td>Harga</td>
<td>Status</td>
</tr>
</thead>
<tbody>
<?php
	foreach($is_data['results'] as $row)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no?></td>
<td><?= $row->nama_ruangan;?></td>
<td><?= $row->kelas;?></td>
<td><?= number_format($row->harga,2,",",".");?></td>
<td><?= $row->status;?></td>

</tr>
<?php }
?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error"></div>
<?php } ?>

<?= $is_data['links']; ?>
<?php
get_footer();
?>