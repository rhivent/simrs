<?php get_header(); ?>
<h1>Detail Pasien Rawat Inap</h1>
<div class="alert alert-success">
	<h3>BIODATA PASIEN</h3>
</div>
<?php if(!empty($is_pasien)) : ?>
	<table class="table table-hover">
		<thead>
			<tr>
				<td>No Rekam Medik</td>	
				<td>Nama Pasien</td>
				<td>Tanggal Masuk</td>
				<td>Alamat</td>
				<td>Penanggung Jawab</td>
			</tr>
		</thead>
		<tbody>
		<?php foreach($is_pasien as $row) : ?>
			<tr>
				<td><?= $row->nomor;?></td>
				<td><?= $row->nama_pasien;?></td>
				<td><?= $row->tanggal_masuk;?></td>
				<td><?= $row->alamat_pasien;?></td>
				<td><?= $row->penanggung_jawab;?> (<?= $row->hub_dengan_pasien;?>)</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php else : ?>
	<div class="alert alert-error">Detail Pasien Inap Kosong</div>
<?php endif; ?>
<?php // $is_pasien['links']; ?>

<div class="col-md-6">
	<div class="alert alert-success">
		<h3>DETAIL TINDAKAN PASIEN RAWAT INAP</h3>
	</div>
	<?php $no=0; ?>
	<?php if(!empty($is_data)) : ?>
	<table class="table table-hover">
		<thead>
			<tr>
				<td>No</td>	
				<td>Tanggal Tindakan</td>	
				<td>Nama Tindakan</td>
				<td>Petugas</td>
			</tr>
		</thead>
		<tbody>
		<?php foreach($is_data as $row2) : ?>
		<?php $no=$no+1; ?>
			<tr>
				<td><?= $no;?></td>
				<td><?= $row2->tanggal_tindakan;?></td>
				<td><?= $row2->nama_tindakan;?></td>
				<td><?= $row2->petugas;?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<?php else : ?>
		<div class="alert alert-error">Data Tindakan Masih Kosong</div>
	<?php endif; ?>
	<?php // $is_data['links']; ?>
</div>

<div class="col-md-6">
	<div class="alert alert-success">
		<h3>DETAIL OBAT PASIEN RAWAT INAP</h3>
	</div>
	<?php $no=0; ?>
	<?php if(!empty($is_obat)) : ?>
	<table class="table table-hover">
		<thead>
			<tr>
				<td>No</td>	
				<td>Tanggal Tindakan</td>	
				<td>Nama Obat</td>
				<td>Jumlah</td>
				<td>Petugas</td>
			</tr>
		</thead>
		<tbody>
		<?php foreach($is_obat as $row2) : ?>
		<?php $no=$no+1; ?>
			<tr>
				<td><?= $no;?></td>
				<td><?= $row2->tanggal_tindakan;?></td>
				<td><?= $row2->nama_obat;?></td>
				<td><?= $row2->jumlah;?> ( <?= $row2->nama_satuan;?> )</td>
				<td><?= $row2->petugas;?></td>

			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
	<?php else : ?>
		<div class="alert alert-error">Data Tindakan Masih Kosong</div>
	<?php endif; ?>
	<?php // $is_data['links']; ?>
<?php
if(!empty($is_pasien))
{
?>
<table class="table table-hover">
<thead>
<tr>

<td>No Rekam Medik</td>	
<td>Nama Pasien</td>
<td>Tanggal Masuk</td>
<td>Alamat</td>
<td>Penanggung Jawab</td>

</tr>
</thead>
<tbody>
<?php
	foreach($is_pasien as $row)
	{	
?>
<tr>

<td><?= $row->nomor;?></td>
<td><?= $row->nama_pasien;?></td>
<td><?= $row->tanggal_masuk;?></td>
<td><?= $row->alamat_pasien;?></td>
<td><?= $row->penanggung_jawab;?> (<?= $row->hub_dengan_pasien;?>)</td>

</tr>
<?php }
?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error">Detail Pasien Inap Kosong</div>
<?php } ?>
<!--<?= $is_pasien['links']; ?>-->

<div class="col-md-6">
<div class="alert alert-success">
  <h3>DETAIL TINDAKAN PASIEN RAWAT INAP</h3>
</div>
<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>No</td>	
<td>Tanggal Tindakan</td>	
<td>Nama Tindakan</td>
<td>Petugas</td>

</tr>
</thead>
<tbody>
<?php
	foreach($is_data as $row2)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no;?></td>
<td><?= $row2->tanggal_tindakan;?></td>
<td><?= $row2->nama_tindakan;?></td>
<td><?= $row2->petugas;?></td>

</tr>
<?php }
?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error">Data Tindakan Masih Kosong</div>
<?php } ?>

<!--<?= $is_pasien['links']; ?>-->

</div>

<div class="col-md-6">
<div class="alert alert-success">
  <h3>DETAIL OBAT PASIEN RAWAT INAP</h3>
</div>
<?php
$no=0;
if(!empty($is_obat))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>No</td>	
<td>Tanggal Tindakan</td>	
<td>Nama Obat</td>
<td>Jumlah</td>
<td>Petugas</td>

</tr>
</thead>
<tbody>
<?php
	foreach($is_obat as $row2)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no;?></td>
<td><?= $row2->tanggal_tindakan;?></td>
<td><?= $row2->nama_obat;?></td>
<td><?= $row2->jumlah;?> ( <?= $row2->nama_satuan;?> )</td>
<td><?= $row2->petugas;?></td>

</tr>
<?php }
?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error">Data Tindakan Masih Kosong</div>
<?php } ?>

<!--<?= $is_pasien['links']; ?>-->

</div>
<?php get_footer(); ?>