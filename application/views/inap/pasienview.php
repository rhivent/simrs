<?php
get_header();
?>
<h1>Manajemen Pasien Rawat Inap</h1>
<div class="alert alert-success">
  <h3>Selamat Datang</h3>
</div>
<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>No</td>	
<td>No Rekam Medik</td>	
<td>Nama Pasien</td>
<td>Nama Ruangan</td>
<td>Kelas</td>
<td>Tanggal Masuk</td>
<td>Alamat</td>
<td>Penanggung Jawab</td>
<td>Aksi</td>
</tr>
</thead>
<tbody>
<?php
	foreach($is_data['results'] as $row)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no?></td>
<td><?= $row->nomor;?></td>
<td><?= $row->nama_pasien;?></td>
<td><?= $row->nama_ruangan;?></td>
<td><?= $row->kelas;?></td>
<td><?= $row->tanggal_masuk;?></td>
<td><?= $row->alamat_pasien;?></td>
<td><?= $row->penanggung_jawab;?> (<?= $row->hub_dengan_pasien;?>)</td>
<td>
<a class="btn btn-small" href="<?=base_url();?>inap/pasien/detailtindakan?uid=<?=$row->id_reg_inap;?>&nomor=<?=$row->nomor?>"><i class="icon-search"></i> Detail</a>
<a class="btn btn-small" href="<?=base_url();?>inap/pasien/adddetailtindakan?uid=<?=$row->id_reg_inap;?>&nomor=<?=$row->nomor?>"><i class="icon-plus"></i> Tindakan</a>
<a class="btn btn-small" href="<?=base_url();?>inap/pasien/adddetailobat?uid=<?=$row->id_reg_inap;?>&nomor=<?=$row->nomor?>"><i class="icon-plus"></i> Obat</a>
<a class="btn btn-small" href="<?=base_url();?>inap/master/pulang?uid=<?=$row->id_reg_inap;?>&nomor=<?=$row->nomor?>"><i class="icon-home"></i> Proses</a>
</td>
</tr>
<?php }
?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error"></div>
<?php } ?>

<?= $is_data['links']; ?>
<?php
get_footer();
?>