<?php
get_header();
?>
<h1>Input Tindakan Pasien Rawat Inap</h1><br>

<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
    );
    
echo form_open('inap/pasien/simpanobat',$att);
    
?>
    <input type="hidden" id="inputEmail" name="id_reg_inap" value="<?php echo $id_reg_inap ?>">
    <div class="control-group">
        <label class="control-label" for="inputEmail">No Rekam Medik Pasien</label>
        <div class="controls">
            <?php
                foreach ($is_pasien as $row) {
            ?>
            <input type="text" id="inputEmail" name="nomor" value="<?php echo $row->nomor ?>">
            <?php
                }
            ?>
        </div>
    </div>
    <div class="control-group">
        <div class="control-group">
            <label class="control-label" for="inputEmail">Nama Obat</label>
            <div class="controls">
                <select name="id_obat">
                    <option value="0">-- Pilih Obat --</option>
                    <?php
                        foreach ($is_data as $row) {
                    ?>
                    <option value="<?php echo $row->id_obat ?>"><?php echo $row->nama_obat ?></option>
                    <?php
                        }
                    ?>
                </select>
            </div>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Dosis</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="jumlah" placeholder="Masukkan Jumlah Pemberian Obat">
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-success">Proses</button>
        </div>
    </div>
</form>


<?php
get_footer();
?>