<div class="leftmenu">
  <ul class="nav nav-tabs nav-stacked">    
  <!--
    <li class="dropdown"><a href="#"><span class="icon-home"></span> Manajemen Ruang Rawat Inap</a>
      <ul>
        <li><a href="<?=base_url('inap/master/ruangan');?>">Setting Item Ruangan</a></li>
        <li><a href="<?=base_url('inap/master/tindakan');?>">Setting Item Tindakan</a></li>
        <li><a href="<?=base_url('inap/master/obat');?>">Setting Item Obat</a></li>
        <li><a href="<?=base_url('inap/pasien');?>">Pencarian Data Pasien</a></li>
      </ul>
    </li>
    -->
    <li class="dropdown"><a href="#"><span class="icon-list"></span> Manajemen Perawatan Pasien</a>
      <ul>
        <li><a href="<?=base_url('inap/pasien');?>">Data Pasien</a></li>
      </ul>
    </li> 
    <li class="dropdown"><a href="#"><span class="icon-list"></span> Laporan</a>
      <ul>
        <li><a target="_blank" href="<?=base_url('inap/pasien/laporanpasien');?>">Laporan Pasien Rawat Inap</a></li>
        <li><a target="_blank" href="<?=base_url('inap/pasien/laporanbidanshift');?>">Laporan Jaga Rawat Inap</a></li>
        <!--<li><a target="_blank" href="<?=base_url('inap/pasien/laporanbidan');?>">Laporan Jaga Rawat Inap Per Tanggal</a></li>-->
      </ul>
    </li>         
  </ul>
</div>
<!--leftmenu-->

</div>