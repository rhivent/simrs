<link rel="stylesheet" href="<?=base_url();?>assets/themes/css/bootstrap.min.css" type="text/css" />
<div class="container">
  <div class="row-fluid">
    <div class="span12">
      <table width="100%" border="0" class="">
        <tr>
          <td width="11%" height="162" align="center"><!--<img src="<?=base_url();?>assets/img/provinsi.png" />--></td>
          <td width="77%" align="center"><h2> RSIA PURI AGUNG</h2>
            <p align="center">Alamat : Jl. Pahlawan No.119, Potrobangsan, Magelang Utara, Kota Magelang, Jawa Tengah 56116</p>
            <p align="center"><strong>===========================================================================</strong></p>
          </td>
          <td width="12%" align="center"><!--<img src="<?=base_url();?>assets/img/baktihusada.png" />--></td>
        </tr>
      </table>
    </div>
  </div>
    <center><h3>Laporan Pasien Rawat Inap</h3></center>
    <center><h4><?php echo $tanggal_awal?> s.d. <?=$tanggal_akhir?></h4></center>
    <p>&nbsp;</p>
    <a class="btn btn-medium btn-primary"href="#" onClick="window.print();"><i class="icon-print"></i> Cetak</a>
    <p>&nbsp;</p>
    <?php $no=0; ?>
    <?php if(!empty($is_data)) : ?>
      <table class="table table-hover">
        <thead>
          <tr>
            <td>No</td>	
            <td>No Rekam Medik</td>
            <td>Nama Pasien</td>
            <td>Alamat Pasien</td>
            <td>Ruangan</td>
            <td>Kelas</td>
            <td>Tanggal Masuk</td>
            <td>Tanggal Keluar</td>
          </tr>
        </thead>
        <tbody>
        <?php if (!empty($is_data['results'])) : ?>
        <?php foreach($is_data['results'] as $row) : ?>
        <?php $no=$no+1; ?>
          <tr>
            <td><?= $no?></td>
            <td><?= $row->nomor;?></td>
            <td><?= $row->nama_pasien;?></td>
            <td><?= $row->alamat_pasien;?></td>
            <td><?= $row->nama_ruangan;?></td>
            <td><?= $row->kelas;?></td>
            <td><?= $row->tanggal_masuk;?></td>
            <td><?= $row->tanggal_keluar;?></td>
          </tr>
        <?php endforeach; ?>
        <?php endif; ?>
      </tbody>
    </table>
  <?php else : ?>
    <div class="alert alert-error"></div>
  <?php endif; ?>
  <?php inc_app('themes','ttd'); //inc_app('themes','footerreport'); ?>