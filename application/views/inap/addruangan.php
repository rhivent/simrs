<?php
get_header();
?>
<h1>Input Ruangan</h1><br>

<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('',$att);
?>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Nama Ruangan</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="nama_ruangan" placeholder="Masukkan Nama Ruangan">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Kelas</label>
        <div class="controls">
            <select name="kelas">
                <option value="0">-- Pilih Kelas Kamar --</option>
                <option value="VIP">VIP</option>
                <option value="I">I</option>
                <option value="II">II</option>
                <option value="III">III</option>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Harga</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="harga" placeholder="Masukkan Harga Ruangan Per Hari">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Status</label>
        <div class="controls">
            <select name="status">
                <option value="0">-- Pilih Status Kamar --</option>
                <option value="KOSONG">KOSONG</option>
                <option value="ISI">ISI</option>
            </select>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-success">Proses</button>
        </div>
    </div>
</form>


<?php
get_footer();
?>