<?php
get_header();
?>
<h1>Edit Ruangan</h1><br>

<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('inap/master/updateruangan',$att);
foreach($update as $update){
?>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Nama Ruangan</label>
        <div class="controls">
            <input type="hidden" id="inputEmail" name="id_ruangan" value="<?php echo $update->id_ruangan ?>">
            <input type="text" id="inputEmail" name="nama_ruangan" value="<?php echo $update->nama_ruangan ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Kelas</label>
        <div class="controls">
            <select name="kelas">
                <option value="<?php echo $update->kelas ?>"><?php echo $update->kelas ?></option>
                <option value="0">-- Pilih Kelas Kamar --</option>
                <option value="VIP">VIP</option>
                <option value="I">I</option>
                <option value="II">II</option>
                <option value="III">III</option>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Harga</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="harga" value="<?php echo $update->harga ?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Status</label>
        <div class="controls">
            <select name="status">
                <option value="<?php echo $update->status ?>"><?php echo $update->status ?></option>
                <option value="0">-- Pilih Status Kamar --</option>
                <option value="KOSONG">KOSONG</option>
                <option value="ISI">ISI</option>
            </select>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-success">Proses</button>
        </div>
    </div>
</form>


<?php
}
get_footer();
?>