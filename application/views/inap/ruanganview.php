<?php
get_header();
?>
<h1>Manajemen Ruangan Rawat Inap</h1>
<div class="alert alert-success">
  <h3>Selamat Datang, Silahkan tambah data ruangan <a href="<?=base_url('inap/master/addruangan');?>" class="alert-link"> Di sini </a></h3>
</div>
<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>No</td>	
<td>Nama Ruangan</td>
<td>Kelas</td>
<td>Harga</td>
<td>Status</td>
<td>Aksi</td>
</tr>
</thead>
<tbody>
<?php
	foreach($is_data['results'] as $row)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no?></td>
<td><?= $row->nama_ruangan;?></td>
<td><?= $row->kelas;?></td>
<td><?= number_format($row->harga,2,",",".");?></td>
<td><?= $row->status;?></td>
<td>
<a class="btn btn-small" href="<?=base_url();?>inap/master/getruanganbyid?uid=<?=$row->id_ruangan;?>"><i class="icon-edit"></i> Edit</a>
<a class="btn btn-small" href="<?=base_url();?>inap/master/deleteruangan?uid=<?=$row->id_ruangan;?>"><i class="icon-trash"></i> Hapus</a>
</td>
</tr>
<?php }
?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error"></div>
<?php } ?>

<?= $is_data['links']; ?>
<?php
get_footer();
?>