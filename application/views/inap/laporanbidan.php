<?php
get_header();
?>
<h1>Laporan Tugas Bidan</h1><br>

<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('',$att);
?>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Tanggal Awal Laporan</label>
        <div class="controls">
            <input type="date" id="inputEmail" name="tanggal_awal">
            <input type="time" id="inputEmail" name="jam_awal">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Tanggal Akhir Laporan</label>
        <div class="controls">
            <input type="date" id="inputEmail" name="tanggal_akhir">
            <input type="time" id="inputEmail" name="jam_akhir">
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-success">Proses</button>
        </div>
    </div>
</form>


<?php
get_footer();
?>