<?php
get_header();
?>
<h1>Input Tindakan</h1><br>

<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
	echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('',$att);
?>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Nama Tindakan</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="nama_tindakan" placeholder="Masukkan Nama Tindakan">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Harga</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="harga" placeholder="Masukkan Harga Tindakan">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Keterangan Tindakan</label>
        <div class="controls">
        <textarea name="keterangan" placeholder="Keterangan" class="input-xxlarge"></textarea>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-success">Proses</button>
        </div>
    </div>
</form>


<?php
get_footer();
?>