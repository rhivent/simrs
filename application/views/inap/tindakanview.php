<?php
get_header();
?>
<h1>Manajemen Tindakan Rawat Inap</h1>
<div class="alert alert-success">
  <h3>Selamat Datang, Silahkan tambah data tindakan <a href="<?=base_url('inap/master/addtindakan');?>" class="alert-link"> Di sini </a></h3>
</div>
<?php
$no=0;
if(!empty($is_data))
{
?>
<table class="table table-hover">
<thead>
<tr>
<td>No</td>	
<td>Nama Tindakan</td>
<td>Harga</td>
<td>Keterangan</td>
<td>Aksi</td>
</tr>
</thead>
<tbody>
<?php
	foreach($is_data['results'] as $row)
	{
	$no=$no+1;	

?>
<tr>
<td><?= $no?></td>
<td><?= $row->nama_tindakan;?></td>
<td class="text-right"><?= number_format($row->harga,2,",",".");?></td>
<td><?= $row->keterangan;?></td>
<td>
<a class="btn btn-small" href="<?=base_url();?>inap/master/gettindakanbyid?uid=<?=$row->id_tindakan;?>"><i class="icon-edit"></i> Edit</a>
<a class="btn btn-small" href="<?=base_url();?>inap/master/deletetindakan?uid=<?=$row->id_tindakan;?>"><i class="icon-trash"></i> Hapus</a>
</td>
</tr>
<?php }
?>
</tbody>
</table>
<?php }else{ ?>
<div class="alert alert-error"></div>
<?php } ?>

<?= $is_data['links']; ?>
<?php
get_footer();
?>