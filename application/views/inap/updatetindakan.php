<?php
get_header();
?>
<h1>Update Tindakan</h1><br>

<?php echo validation_errors('<div class="alert alert-error">', '</div>'); ?>
<?php
if(!empty($isok))
{
    echo '<div class="alert alert-success">'.$isok.'</div>';
}

$att=array(
	'class'=>'form-horizontal',
	'role'=>'form',
	);
echo form_open('inap/master/updatetindakan',$att);
foreach($update as $update){
?>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Nama Tindakan</label>
        <div class="controls">
            <input type="hidden" id="inputEmail" name="id_tindakan" value="<?=$update->id_tindakan?>" class="input-mini">
            <input type="text" id="inputEmail" name="nama_tindakan" value="<?=$update->nama_tindakan?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Harga</label>
        <div class="controls">
            <input type="text" id="inputEmail" name="harga" value="<?=$update->harga?>">
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="inputEmail">Keterangan Tindakan</label>
        <div class="controls">
        <textarea name="keterangan"  class="input-xxlarge"><?=$update->keterangan?></textarea>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-success">Proses</button>
        </div>
    </div>
</form>
<?php
}
?>

<?php
get_footer();
?>